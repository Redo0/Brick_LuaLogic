
lualogic_require($LuaLogic::Path @ "bricks/gates/verticalDiode.cs");

datablock fxDTSBrickData(LogicGate_NotUp_Data : LogicGate_DiodeUp_Data)
{
	uiName = "Not Up";
	iconName = $LuaLogic::Path @ "icons/Not Up";

	logicUIName = "Not Up";
	logicUIDesc = "B is the inverse of A";
	
	logicForceColor = "RED";
	logicForcePrint = "UPARROW";

	logicUpdate = "return function(gate) Gate.setportstate(gate, 2, 1-Gate.getportstate(gate, 1)) end";
	logicCFunction = 2;
};
lualogic_registergatedefinition("LogicGate_NotUp_Data");

datablock fxDTSBrickData(LogicGate_NotDown_Data : LogicGate_DiodeDown_Data)
{
	uiName = "Not Down";
	iconName = $LuaLogic::Path @ "icons/Not Down";

	logicUIName = "Not Down";
	logicUIDesc = "B is the inverse of A";
	
	logicForceColor = "RED";
	logicForcePrint = "DOWNARROW";

	logicUpdate = "return function(gate) Gate.setportstate(gate, 2, 1-Gate.getportstate(gate, 1)) end";
	logicCFunction = 2;
};
lualogic_registergatedefinition("LogicGate_NotDown_Data");

$ND::ManualSymmetryZDB["Not Up"  ] = "Not Down";
$ND::ManualSymmetryZDB["Not Down"] = "Not Up"  ;
