datablock fxDTSBrickData(LogicGate_Diode_Data)
{
	category = "Logic Bricks";
	subCategory = "Diode";
	uiName = "1x1f Diode";
	iconName = $LuaLogic::Path @ "icons/1x1f Diode";
	brickFile = $LuaLogic::Path @ "bricks/blb/1x1f_1i_1o.blb";
	hasPrint = 1;
	printAspectRatio = "Logic";

	isLogic = 1;
	isLogicGate = 1;
	logicUIName = "Diode";
	logicUIDesc = "B is A";
	
	logicForceColor = "GREEN";
	logicForcePrint = "ARROW";

	logicUpdate = "return function(gate) Gate.setportstate(gate, 2, Gate.getportstate(gate, 1)) end";
	logicCFunction = 1;

	numLogicPorts = 2;

	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 0";
	logicPortDir[0] = "0";
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "A";
	logicPortUIDesc[0] = "";

	logicPortType[1] = 0;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = "2";
	logicPortUIName[1] = "B";
	logicPortUIDesc[1] = "";
};
lualogic_registergatedefinition("LogicGate_Diode_Data");

$ND::ManualSymmetryZ["Diode"] = true;
