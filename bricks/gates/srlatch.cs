
datablock fxDtsBrickData(LogicGate_GateSRLatch_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/AND 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/AND 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "SR Latch";
	logicUIName = "SR Latch";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if     Gate.getportstate(gate, 1)~=0 then Gate.setportstate(gate, 3, 0) " @
		"	elseif Gate.getportstate(gate, 2)~=0 then Gate.setportstate(gate, 3, 1) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 3;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "R";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "-1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "S";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 0;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out";
	
};
