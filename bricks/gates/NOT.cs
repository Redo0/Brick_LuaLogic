
lualogic_require($LuaLogic::Path @ "bricks/gates/diode.cs");

datablock fxDTSBrickData(LogicGate_NOT_Data : LogicGate_Diode_Data)
{
	uiName = "1x1f NOT";
	iconName = $LuaLogic::Path @ "icons/1x1f NOT";

	logicUIName = "NOT";
	logicUIDesc = "B is the opposite of A";
	
	logicForceColor = "RED";
	logicForcePrint = "ARROW";

	logicUpdate = "return function(gate) Gate.setportstate(gate, 2, 1-Gate.getportstate(gate, 1)) end";
	logicCFunction = 2;

	numLogicPorts = 2;
};
lualogic_registergatedefinition("LogicGate_NOT_Data");

$ND::ManualSymmetryZ["NOT"] = true;
