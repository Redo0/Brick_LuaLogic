
function lualogic_require(%fn){}

exec("./gen/execall.cs");

exec("./gates/diode.cs");
exec("./gates/NOT.cs");
exec("./gates/verticalDiode.cs");
exec("./gates/verticalNOT.cs");
exec("./gates/srlatch.cs");

exec("./inputs/switch.cs");
exec("./inputs/button.cs");
exec("./inputs/keyboard.cs");

exec("./math/8bitAdder.cs");
exec("./math/8bitSubtractor.cs");
exec("./math/8bitMultiplier.cs");
exec("./math/8bitDivider.cs");
exec("./math/FullAdder.cs");
exec("./math/HalfAdder.cs");

exec("./outputs/Pixel3.cs");
exec("./outputs/TextBrick2.cs");

exec("./special/EventGate.cs");
exec("./special/rom.cs");
exec("./special/robot.cs");
