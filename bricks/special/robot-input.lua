
local function intToPort(gate, port, len, v)
	for i = 1, len do
		local p = math.pow(2, i-1)
		Gate.setportstate(gate, port+i-1, math.floor(v/p)%2)
	end
end

return function(gate, argv)
	local arg = argv[1]:gsub("^ +", ""):gsub(" +$", "")
	for word in arg:gmatch("[^\t]+") do
		local first, rest = word:sub(1, 1), word:sub(2, #word)
		local vec = {}
		for a in rest:gmatch("[^ ]+") do table.insert(vec, tonumber(a) or error("invalid number "..a)) end
		if first=="P" then -- set position
			assert(#vec==4, "invalid position given to robot: "..word)
			gate.robotpos = {vec[1], vec[2], vec[3]}
			gate.robotdir = vec[4]
		elseif first=="B" then -- detected brick info
			assert(#vec==7, "invalid brick info given to robot: "..word)
			gate.brickexists  = vec[1]
			gate.brickcolor   = vec[2]
			gate.brickcolorfx = vec[3]
			gate.brickshapefx = vec[4]
			gate.brickray     = vec[5]
			gate.brickcol     = vec[6]
			gate.brickren     = vec[7]
			intToPort(gate, 15, 6, gate.brickcolor)
			Gate.setportstate(gate, 21, gate.brickexists)
		else error("invalid control word given to robot: \""..word.."\".. (first \""..first.."\")") end
	end
	gate.waiting = false
end
