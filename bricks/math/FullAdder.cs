datablock fxDTSBrickData(LogicGate_FullAdder_Data)
{
	brickFile = $LuaLogic::Path @ "bricks/blb/FullAdder.blb";
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Full Adder";
	iconName = $LuaLogic::Path @ "icons/Full Adder";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Full Adder";
	logicUIDesc = "Adds A and B with carry in";
	
	logicUpdate =
		"return function(gate) " @
		"	local a, b, c = Gate.getportstate(gate, 1), Gate.getportstate(gate, 2), Gate.getportstate(gate, 3) " @
		"	Gate.setportstate(gate, 4, bit.bxor(bit.bxor(a, b), c)) " @
		"	Gate.setportstate(gate, 5, bit.bor(bit.bor(bit.band(b, c), bit.band(a, c)), bit.band(a, b))) " @
		"end"
	;
	
	numLogicPorts = 5;

	logicPortType[0] = 1;
	logicPortPos[0] = "-1 0 0";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "A";

	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 3;
	logicPortCauseUpdate[1] = true;
	logicPortUIName[1] = "B";

	logicPortType[2] = 1;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 2;
	logicPortCauseUpdate[2] = true;
	logicPortUIName[2] = "Carry In";

	logicPortType[3] = 0;
	logicPortPos[3] = "-1 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Sum";

	logicPortType[4] = 0;
	logicPortPos[4] = "-1 0 0";
	logicPortDir[4] = 0;
	logicPortUIName[4] = "Carry Out";
};
lualogic_registergatedefinition("LogicGate_FullAdder_Data");
