
local function queueBit(gate, bit)
	table.insert(gate.queueBits, 1, bit)
end

return function(gate, argv)
	if argv[1]=="\\:" then argv[1] = ";" end
	
	local keycode = keyboard_keyToCode[argv[1]] or keyboard_keyToCode["invalid"]
	local status = keyboard_strToBool[argv[2]]
	
	local code = keycode+(status and 128 or 0)
	
	if #gate.queueBits > 1024 then return end
	
	queueBit(gate, 1)
	for bitidx = 1, 8 do
		local val = bit.band(code, 0x80)
		queueBit(gate, val)
		
		code = bit.lshift(code, 1)
	end
	queueBit(gate, 0)
	
	Gate.queue(gate, 0)
end
