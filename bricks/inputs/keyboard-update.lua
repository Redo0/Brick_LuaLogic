
return function(gate)
	if #gate.queueBits~=0 then
		local bit = table.remove(gate.queueBits, #gate.queueBits)
		Gate.setportstate(gate, 1, bit)
		Gate.queue(gate, 1)
	else
		Gate.setportstate(gate, 1, 0)
	end
end
