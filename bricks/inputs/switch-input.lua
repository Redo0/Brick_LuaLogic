
return function(gate, argv)
	local gatedata = Gate.getdata(gate)
	if #argv == 1 then
		gatedata.switchstate = toboolean(argv[1])
	else
		gatedata.switchstate = not gatedata.switchstate
	end
	Gate.setportstate(gate, 1, gatedata.switchstate and 1 or 0)
	Gate.setportstate(gate, 2, gatedata.switchstate and 1 or 0)
	Gate.cb(gate, bool_to_int[gatedata.switchstate])
end
