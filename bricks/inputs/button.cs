
lualogic_require($LuaLogic::Path @ "bricks/inputs/switch.cs");

datablock fxDTSBrickData(LogicGate_Button_Data : LogicGate_Switch_Data){
	category = "Logic Bricks";
	subCategory = "Inputs";
	uiName = "Logic Button";
	
	numLogicPorts = 2;
	
	logicInit  = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/switch-init.lua" );
	logicInput = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/switch-input.lua");
	
	logicPortType[0] = 0;
	logicPortPos[0] = "0 1 0";
	logicPortDir[0] = "1";
	
	logicPortType[1] = 0;
	logicPortPos[1] = "0 -1 0";
	logicPortDir[1] = "3";
};

function LogicGate_Button_Data::Logic_onInput(%data, %brick, %pos, %norm){
	lualogic_sendinput(%brick, 1, 1);
	schedule(100, 0, lualogic_sendinput, %brick, 1, 0);
}

function LogicGate_Button_Data::Logic_onAdd(%data, %brick){
	lualogic_sendinput(%brick, 1, %brick.getColorFXID() == 3);
}

function LogicGate_Button_Data::LuaLogic_Callback(%data, %brick, %args){
	%brick.setColorFX(getField(%args, 0) == 1 ? 3 : 0);
}
