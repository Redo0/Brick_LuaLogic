
datablock fxDtsBrickData(LogicWire_1x9f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x9f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x9f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x9f";
	
	logicBrickSize = "1 9 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
