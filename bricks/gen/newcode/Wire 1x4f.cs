
datablock fxDtsBrickData(LogicWire_1x4f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x4f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x4f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x4f";
	
	logicBrickSize = "1 4 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
