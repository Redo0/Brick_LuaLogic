
datablock fxDtsBrickData(LogicWire_1x39f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x39f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x39f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x39f";
	
	logicBrickSize = "1 39 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
