
datablock fxDtsBrickData(LogicWire_1x36f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x36f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x36f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x36f";
	
	logicBrickSize = "1 36 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
