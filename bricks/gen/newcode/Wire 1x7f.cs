
datablock fxDtsBrickData(LogicWire_1x7f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x7f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x7f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x7f";
	
	logicBrickSize = "1 7 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
