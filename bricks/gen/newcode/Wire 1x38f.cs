
datablock fxDtsBrickData(LogicWire_1x38f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x38f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x38f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x38f";
	
	logicBrickSize = "1 38 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
