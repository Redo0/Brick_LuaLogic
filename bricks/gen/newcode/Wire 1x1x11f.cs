
datablock fxDtsBrickData(LogicWire_1x1x11f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x11f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x11f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x11f";
	
	logicBrickSize = "1 1 11";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
