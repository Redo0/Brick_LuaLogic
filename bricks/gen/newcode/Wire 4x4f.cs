
datablock fxDtsBrickData(LogicWire_4x4f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 4x4f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 4x4f";
	
	category = "Logic Bricks";
	subCategory = "Wires Other";
	uiName = "Wire 4x4f";
	
	logicBrickSize = "4 4 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
