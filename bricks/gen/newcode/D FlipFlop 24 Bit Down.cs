
datablock fxDtsBrickData(LogicGate_DFlipFlop24BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 24 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 24 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 24 Bit Down";
	logicUIName = "D FlipFlop 24 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "24 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 49)~=0 then " @
		"		Gate.setportstate(gate, 25, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 26, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 27, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 28, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 29, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 30, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 31, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 32, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 33, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 34, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 35, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 36, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 37, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 38, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 39, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 40, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 41, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 42, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 43, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 44, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 45, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 46, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 47, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 48, Gate.getportstate(gate, 24)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 49;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "23 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "21 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "19 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "17 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "15 0 0";
	logicPortDir[4] = 4;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "13 0 0";
	logicPortDir[5] = 4;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "11 0 0";
	logicPortDir[6] = 4;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "9 0 0";
	logicPortDir[7] = 4;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "7 0 0";
	logicPortDir[8] = 4;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "5 0 0";
	logicPortDir[9] = 4;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "3 0 0";
	logicPortDir[10] = 4;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "1 0 0";
	logicPortDir[11] = 4;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "-1 0 0";
	logicPortDir[12] = 4;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "-3 0 0";
	logicPortDir[13] = 4;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "-5 0 0";
	logicPortDir[14] = 4;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "-7 0 0";
	logicPortDir[15] = 4;
	logicPortUIName[15] = "In15";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-9 0 0";
	logicPortDir[16] = 4;
	logicPortUIName[16] = "In16";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-11 0 0";
	logicPortDir[17] = 4;
	logicPortUIName[17] = "In17";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-13 0 0";
	logicPortDir[18] = 4;
	logicPortUIName[18] = "In18";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-15 0 0";
	logicPortDir[19] = 4;
	logicPortUIName[19] = "In19";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "-17 0 0";
	logicPortDir[20] = 4;
	logicPortUIName[20] = "In20";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-19 0 0";
	logicPortDir[21] = 4;
	logicPortUIName[21] = "In21";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-21 0 0";
	logicPortDir[22] = 4;
	logicPortUIName[22] = "In22";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-23 0 0";
	logicPortDir[23] = 4;
	logicPortUIName[23] = "In23";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "23 0 0";
	logicPortDir[24] = 5;
	logicPortUIName[24] = "Out0";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "21 0 0";
	logicPortDir[25] = 5;
	logicPortUIName[25] = "Out1";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "19 0 0";
	logicPortDir[26] = 5;
	logicPortUIName[26] = "Out2";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "17 0 0";
	logicPortDir[27] = 5;
	logicPortUIName[27] = "Out3";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "15 0 0";
	logicPortDir[28] = 5;
	logicPortUIName[28] = "Out4";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "13 0 0";
	logicPortDir[29] = 5;
	logicPortUIName[29] = "Out5";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "11 0 0";
	logicPortDir[30] = 5;
	logicPortUIName[30] = "Out6";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "9 0 0";
	logicPortDir[31] = 5;
	logicPortUIName[31] = "Out7";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "7 0 0";
	logicPortDir[32] = 5;
	logicPortUIName[32] = "Out8";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "5 0 0";
	logicPortDir[33] = 5;
	logicPortUIName[33] = "Out9";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "3 0 0";
	logicPortDir[34] = 5;
	logicPortUIName[34] = "Out10";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "1 0 0";
	logicPortDir[35] = 5;
	logicPortUIName[35] = "Out11";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "-1 0 0";
	logicPortDir[36] = 5;
	logicPortUIName[36] = "Out12";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "-3 0 0";
	logicPortDir[37] = 5;
	logicPortUIName[37] = "Out13";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-5 0 0";
	logicPortDir[38] = 5;
	logicPortUIName[38] = "Out14";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-7 0 0";
	logicPortDir[39] = 5;
	logicPortUIName[39] = "Out15";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "-9 0 0";
	logicPortDir[40] = 5;
	logicPortUIName[40] = "Out16";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "-11 0 0";
	logicPortDir[41] = 5;
	logicPortUIName[41] = "Out17";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "-13 0 0";
	logicPortDir[42] = 5;
	logicPortUIName[42] = "Out18";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "-15 0 0";
	logicPortDir[43] = 5;
	logicPortUIName[43] = "Out19";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-17 0 0";
	logicPortDir[44] = 5;
	logicPortUIName[44] = "Out20";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-19 0 0";
	logicPortDir[45] = 5;
	logicPortUIName[45] = "Out21";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-21 0 0";
	logicPortDir[46] = 5;
	logicPortUIName[46] = "Out22";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-23 0 0";
	logicPortDir[47] = 5;
	logicPortUIName[47] = "Out23";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "23 0 0";
	logicPortDir[48] = 2;
	logicPortUIName[48] = "Clock";
	logicPortCauseUpdate[48] = true;
	
};
