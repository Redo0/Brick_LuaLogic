
datablock fxDtsBrickData(LogicGate_DFlipFlop64BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 64 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 64 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 64 Bit Down";
	logicUIName = "D FlipFlop 64 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 129)~=0 then " @
		"		Gate.setportstate(gate, 65, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 66, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 67, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 68, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 69, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 70, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 71, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 72, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 73, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 74, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 75, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 76, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 77, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 78, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 79, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 80, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 81, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 82, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 83, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 84, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 85, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 86, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 87, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 88, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 89, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 90, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 91, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 92, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 93, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 94, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 95, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 96, Gate.getportstate(gate, 32)) " @
		"		Gate.setportstate(gate, 97, Gate.getportstate(gate, 33)) " @
		"		Gate.setportstate(gate, 98, Gate.getportstate(gate, 34)) " @
		"		Gate.setportstate(gate, 99, Gate.getportstate(gate, 35)) " @
		"		Gate.setportstate(gate, 100, Gate.getportstate(gate, 36)) " @
		"		Gate.setportstate(gate, 101, Gate.getportstate(gate, 37)) " @
		"		Gate.setportstate(gate, 102, Gate.getportstate(gate, 38)) " @
		"		Gate.setportstate(gate, 103, Gate.getportstate(gate, 39)) " @
		"		Gate.setportstate(gate, 104, Gate.getportstate(gate, 40)) " @
		"		Gate.setportstate(gate, 105, Gate.getportstate(gate, 41)) " @
		"		Gate.setportstate(gate, 106, Gate.getportstate(gate, 42)) " @
		"		Gate.setportstate(gate, 107, Gate.getportstate(gate, 43)) " @
		"		Gate.setportstate(gate, 108, Gate.getportstate(gate, 44)) " @
		"		Gate.setportstate(gate, 109, Gate.getportstate(gate, 45)) " @
		"		Gate.setportstate(gate, 110, Gate.getportstate(gate, 46)) " @
		"		Gate.setportstate(gate, 111, Gate.getportstate(gate, 47)) " @
		"		Gate.setportstate(gate, 112, Gate.getportstate(gate, 48)) " @
		"		Gate.setportstate(gate, 113, Gate.getportstate(gate, 49)) " @
		"		Gate.setportstate(gate, 114, Gate.getportstate(gate, 50)) " @
		"		Gate.setportstate(gate, 115, Gate.getportstate(gate, 51)) " @
		"		Gate.setportstate(gate, 116, Gate.getportstate(gate, 52)) " @
		"		Gate.setportstate(gate, 117, Gate.getportstate(gate, 53)) " @
		"		Gate.setportstate(gate, 118, Gate.getportstate(gate, 54)) " @
		"		Gate.setportstate(gate, 119, Gate.getportstate(gate, 55)) " @
		"		Gate.setportstate(gate, 120, Gate.getportstate(gate, 56)) " @
		"		Gate.setportstate(gate, 121, Gate.getportstate(gate, 57)) " @
		"		Gate.setportstate(gate, 122, Gate.getportstate(gate, 58)) " @
		"		Gate.setportstate(gate, 123, Gate.getportstate(gate, 59)) " @
		"		Gate.setportstate(gate, 124, Gate.getportstate(gate, 60)) " @
		"		Gate.setportstate(gate, 125, Gate.getportstate(gate, 61)) " @
		"		Gate.setportstate(gate, 126, Gate.getportstate(gate, 62)) " @
		"		Gate.setportstate(gate, 127, Gate.getportstate(gate, 63)) " @
		"		Gate.setportstate(gate, 128, Gate.getportstate(gate, 64)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 129;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 0 0";
	logicPortDir[4] = 4;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 0 0";
	logicPortDir[5] = 4;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 0 0";
	logicPortDir[6] = 4;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 0 0";
	logicPortDir[7] = 4;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 0 0";
	logicPortDir[8] = 4;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 0 0";
	logicPortDir[9] = 4;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 0 0";
	logicPortDir[10] = 4;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "41 0 0";
	logicPortDir[11] = 4;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "39 0 0";
	logicPortDir[12] = 4;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "37 0 0";
	logicPortDir[13] = 4;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "35 0 0";
	logicPortDir[14] = 4;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "33 0 0";
	logicPortDir[15] = 4;
	logicPortUIName[15] = "In15";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "31 0 0";
	logicPortDir[16] = 4;
	logicPortUIName[16] = "In16";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "29 0 0";
	logicPortDir[17] = 4;
	logicPortUIName[17] = "In17";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "27 0 0";
	logicPortDir[18] = 4;
	logicPortUIName[18] = "In18";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "25 0 0";
	logicPortDir[19] = 4;
	logicPortUIName[19] = "In19";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "23 0 0";
	logicPortDir[20] = 4;
	logicPortUIName[20] = "In20";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "21 0 0";
	logicPortDir[21] = 4;
	logicPortUIName[21] = "In21";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "19 0 0";
	logicPortDir[22] = 4;
	logicPortUIName[22] = "In22";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "17 0 0";
	logicPortDir[23] = 4;
	logicPortUIName[23] = "In23";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "15 0 0";
	logicPortDir[24] = 4;
	logicPortUIName[24] = "In24";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "13 0 0";
	logicPortDir[25] = 4;
	logicPortUIName[25] = "In25";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "11 0 0";
	logicPortDir[26] = 4;
	logicPortUIName[26] = "In26";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "9 0 0";
	logicPortDir[27] = 4;
	logicPortUIName[27] = "In27";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "7 0 0";
	logicPortDir[28] = 4;
	logicPortUIName[28] = "In28";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "5 0 0";
	logicPortDir[29] = 4;
	logicPortUIName[29] = "In29";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "3 0 0";
	logicPortDir[30] = 4;
	logicPortUIName[30] = "In30";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "1 0 0";
	logicPortDir[31] = 4;
	logicPortUIName[31] = "In31";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-1 0 0";
	logicPortDir[32] = 4;
	logicPortUIName[32] = "In32";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-3 0 0";
	logicPortDir[33] = 4;
	logicPortUIName[33] = "In33";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-5 0 0";
	logicPortDir[34] = 4;
	logicPortUIName[34] = "In34";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-7 0 0";
	logicPortDir[35] = 4;
	logicPortUIName[35] = "In35";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-9 0 0";
	logicPortDir[36] = 4;
	logicPortUIName[36] = "In36";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "-11 0 0";
	logicPortDir[37] = 4;
	logicPortUIName[37] = "In37";
	
	logicPortType[38] = 1;
	logicPortPos[38] = "-13 0 0";
	logicPortDir[38] = 4;
	logicPortUIName[38] = "In38";
	
	logicPortType[39] = 1;
	logicPortPos[39] = "-15 0 0";
	logicPortDir[39] = 4;
	logicPortUIName[39] = "In39";
	
	logicPortType[40] = 1;
	logicPortPos[40] = "-17 0 0";
	logicPortDir[40] = 4;
	logicPortUIName[40] = "In40";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "-19 0 0";
	logicPortDir[41] = 4;
	logicPortUIName[41] = "In41";
	
	logicPortType[42] = 1;
	logicPortPos[42] = "-21 0 0";
	logicPortDir[42] = 4;
	logicPortUIName[42] = "In42";
	
	logicPortType[43] = 1;
	logicPortPos[43] = "-23 0 0";
	logicPortDir[43] = 4;
	logicPortUIName[43] = "In43";
	
	logicPortType[44] = 1;
	logicPortPos[44] = "-25 0 0";
	logicPortDir[44] = 4;
	logicPortUIName[44] = "In44";
	
	logicPortType[45] = 1;
	logicPortPos[45] = "-27 0 0";
	logicPortDir[45] = 4;
	logicPortUIName[45] = "In45";
	
	logicPortType[46] = 1;
	logicPortPos[46] = "-29 0 0";
	logicPortDir[46] = 4;
	logicPortUIName[46] = "In46";
	
	logicPortType[47] = 1;
	logicPortPos[47] = "-31 0 0";
	logicPortDir[47] = 4;
	logicPortUIName[47] = "In47";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "-33 0 0";
	logicPortDir[48] = 4;
	logicPortUIName[48] = "In48";
	
	logicPortType[49] = 1;
	logicPortPos[49] = "-35 0 0";
	logicPortDir[49] = 4;
	logicPortUIName[49] = "In49";
	
	logicPortType[50] = 1;
	logicPortPos[50] = "-37 0 0";
	logicPortDir[50] = 4;
	logicPortUIName[50] = "In50";
	
	logicPortType[51] = 1;
	logicPortPos[51] = "-39 0 0";
	logicPortDir[51] = 4;
	logicPortUIName[51] = "In51";
	
	logicPortType[52] = 1;
	logicPortPos[52] = "-41 0 0";
	logicPortDir[52] = 4;
	logicPortUIName[52] = "In52";
	
	logicPortType[53] = 1;
	logicPortPos[53] = "-43 0 0";
	logicPortDir[53] = 4;
	logicPortUIName[53] = "In53";
	
	logicPortType[54] = 1;
	logicPortPos[54] = "-45 0 0";
	logicPortDir[54] = 4;
	logicPortUIName[54] = "In54";
	
	logicPortType[55] = 1;
	logicPortPos[55] = "-47 0 0";
	logicPortDir[55] = 4;
	logicPortUIName[55] = "In55";
	
	logicPortType[56] = 1;
	logicPortPos[56] = "-49 0 0";
	logicPortDir[56] = 4;
	logicPortUIName[56] = "In56";
	
	logicPortType[57] = 1;
	logicPortPos[57] = "-51 0 0";
	logicPortDir[57] = 4;
	logicPortUIName[57] = "In57";
	
	logicPortType[58] = 1;
	logicPortPos[58] = "-53 0 0";
	logicPortDir[58] = 4;
	logicPortUIName[58] = "In58";
	
	logicPortType[59] = 1;
	logicPortPos[59] = "-55 0 0";
	logicPortDir[59] = 4;
	logicPortUIName[59] = "In59";
	
	logicPortType[60] = 1;
	logicPortPos[60] = "-57 0 0";
	logicPortDir[60] = 4;
	logicPortUIName[60] = "In60";
	
	logicPortType[61] = 1;
	logicPortPos[61] = "-59 0 0";
	logicPortDir[61] = 4;
	logicPortUIName[61] = "In61";
	
	logicPortType[62] = 1;
	logicPortPos[62] = "-61 0 0";
	logicPortDir[62] = 4;
	logicPortUIName[62] = "In62";
	
	logicPortType[63] = 1;
	logicPortPos[63] = "-63 0 0";
	logicPortDir[63] = 4;
	logicPortUIName[63] = "In63";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "63 0 0";
	logicPortDir[64] = 5;
	logicPortUIName[64] = "Out0";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "61 0 0";
	logicPortDir[65] = 5;
	logicPortUIName[65] = "Out1";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "59 0 0";
	logicPortDir[66] = 5;
	logicPortUIName[66] = "Out2";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "57 0 0";
	logicPortDir[67] = 5;
	logicPortUIName[67] = "Out3";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "55 0 0";
	logicPortDir[68] = 5;
	logicPortUIName[68] = "Out4";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "53 0 0";
	logicPortDir[69] = 5;
	logicPortUIName[69] = "Out5";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "51 0 0";
	logicPortDir[70] = 5;
	logicPortUIName[70] = "Out6";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "49 0 0";
	logicPortDir[71] = 5;
	logicPortUIName[71] = "Out7";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "47 0 0";
	logicPortDir[72] = 5;
	logicPortUIName[72] = "Out8";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "45 0 0";
	logicPortDir[73] = 5;
	logicPortUIName[73] = "Out9";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "43 0 0";
	logicPortDir[74] = 5;
	logicPortUIName[74] = "Out10";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "41 0 0";
	logicPortDir[75] = 5;
	logicPortUIName[75] = "Out11";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "39 0 0";
	logicPortDir[76] = 5;
	logicPortUIName[76] = "Out12";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "37 0 0";
	logicPortDir[77] = 5;
	logicPortUIName[77] = "Out13";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "35 0 0";
	logicPortDir[78] = 5;
	logicPortUIName[78] = "Out14";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "33 0 0";
	logicPortDir[79] = 5;
	logicPortUIName[79] = "Out15";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "31 0 0";
	logicPortDir[80] = 5;
	logicPortUIName[80] = "Out16";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "29 0 0";
	logicPortDir[81] = 5;
	logicPortUIName[81] = "Out17";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "27 0 0";
	logicPortDir[82] = 5;
	logicPortUIName[82] = "Out18";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "25 0 0";
	logicPortDir[83] = 5;
	logicPortUIName[83] = "Out19";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "23 0 0";
	logicPortDir[84] = 5;
	logicPortUIName[84] = "Out20";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "21 0 0";
	logicPortDir[85] = 5;
	logicPortUIName[85] = "Out21";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "19 0 0";
	logicPortDir[86] = 5;
	logicPortUIName[86] = "Out22";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "17 0 0";
	logicPortDir[87] = 5;
	logicPortUIName[87] = "Out23";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "15 0 0";
	logicPortDir[88] = 5;
	logicPortUIName[88] = "Out24";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "13 0 0";
	logicPortDir[89] = 5;
	logicPortUIName[89] = "Out25";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "11 0 0";
	logicPortDir[90] = 5;
	logicPortUIName[90] = "Out26";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "9 0 0";
	logicPortDir[91] = 5;
	logicPortUIName[91] = "Out27";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "7 0 0";
	logicPortDir[92] = 5;
	logicPortUIName[92] = "Out28";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "5 0 0";
	logicPortDir[93] = 5;
	logicPortUIName[93] = "Out29";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "3 0 0";
	logicPortDir[94] = 5;
	logicPortUIName[94] = "Out30";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "1 0 0";
	logicPortDir[95] = 5;
	logicPortUIName[95] = "Out31";
	
	logicPortType[96] = 0;
	logicPortPos[96] = "-1 0 0";
	logicPortDir[96] = 5;
	logicPortUIName[96] = "Out32";
	
	logicPortType[97] = 0;
	logicPortPos[97] = "-3 0 0";
	logicPortDir[97] = 5;
	logicPortUIName[97] = "Out33";
	
	logicPortType[98] = 0;
	logicPortPos[98] = "-5 0 0";
	logicPortDir[98] = 5;
	logicPortUIName[98] = "Out34";
	
	logicPortType[99] = 0;
	logicPortPos[99] = "-7 0 0";
	logicPortDir[99] = 5;
	logicPortUIName[99] = "Out35";
	
	logicPortType[100] = 0;
	logicPortPos[100] = "-9 0 0";
	logicPortDir[100] = 5;
	logicPortUIName[100] = "Out36";
	
	logicPortType[101] = 0;
	logicPortPos[101] = "-11 0 0";
	logicPortDir[101] = 5;
	logicPortUIName[101] = "Out37";
	
	logicPortType[102] = 0;
	logicPortPos[102] = "-13 0 0";
	logicPortDir[102] = 5;
	logicPortUIName[102] = "Out38";
	
	logicPortType[103] = 0;
	logicPortPos[103] = "-15 0 0";
	logicPortDir[103] = 5;
	logicPortUIName[103] = "Out39";
	
	logicPortType[104] = 0;
	logicPortPos[104] = "-17 0 0";
	logicPortDir[104] = 5;
	logicPortUIName[104] = "Out40";
	
	logicPortType[105] = 0;
	logicPortPos[105] = "-19 0 0";
	logicPortDir[105] = 5;
	logicPortUIName[105] = "Out41";
	
	logicPortType[106] = 0;
	logicPortPos[106] = "-21 0 0";
	logicPortDir[106] = 5;
	logicPortUIName[106] = "Out42";
	
	logicPortType[107] = 0;
	logicPortPos[107] = "-23 0 0";
	logicPortDir[107] = 5;
	logicPortUIName[107] = "Out43";
	
	logicPortType[108] = 0;
	logicPortPos[108] = "-25 0 0";
	logicPortDir[108] = 5;
	logicPortUIName[108] = "Out44";
	
	logicPortType[109] = 0;
	logicPortPos[109] = "-27 0 0";
	logicPortDir[109] = 5;
	logicPortUIName[109] = "Out45";
	
	logicPortType[110] = 0;
	logicPortPos[110] = "-29 0 0";
	logicPortDir[110] = 5;
	logicPortUIName[110] = "Out46";
	
	logicPortType[111] = 0;
	logicPortPos[111] = "-31 0 0";
	logicPortDir[111] = 5;
	logicPortUIName[111] = "Out47";
	
	logicPortType[112] = 0;
	logicPortPos[112] = "-33 0 0";
	logicPortDir[112] = 5;
	logicPortUIName[112] = "Out48";
	
	logicPortType[113] = 0;
	logicPortPos[113] = "-35 0 0";
	logicPortDir[113] = 5;
	logicPortUIName[113] = "Out49";
	
	logicPortType[114] = 0;
	logicPortPos[114] = "-37 0 0";
	logicPortDir[114] = 5;
	logicPortUIName[114] = "Out50";
	
	logicPortType[115] = 0;
	logicPortPos[115] = "-39 0 0";
	logicPortDir[115] = 5;
	logicPortUIName[115] = "Out51";
	
	logicPortType[116] = 0;
	logicPortPos[116] = "-41 0 0";
	logicPortDir[116] = 5;
	logicPortUIName[116] = "Out52";
	
	logicPortType[117] = 0;
	logicPortPos[117] = "-43 0 0";
	logicPortDir[117] = 5;
	logicPortUIName[117] = "Out53";
	
	logicPortType[118] = 0;
	logicPortPos[118] = "-45 0 0";
	logicPortDir[118] = 5;
	logicPortUIName[118] = "Out54";
	
	logicPortType[119] = 0;
	logicPortPos[119] = "-47 0 0";
	logicPortDir[119] = 5;
	logicPortUIName[119] = "Out55";
	
	logicPortType[120] = 0;
	logicPortPos[120] = "-49 0 0";
	logicPortDir[120] = 5;
	logicPortUIName[120] = "Out56";
	
	logicPortType[121] = 0;
	logicPortPos[121] = "-51 0 0";
	logicPortDir[121] = 5;
	logicPortUIName[121] = "Out57";
	
	logicPortType[122] = 0;
	logicPortPos[122] = "-53 0 0";
	logicPortDir[122] = 5;
	logicPortUIName[122] = "Out58";
	
	logicPortType[123] = 0;
	logicPortPos[123] = "-55 0 0";
	logicPortDir[123] = 5;
	logicPortUIName[123] = "Out59";
	
	logicPortType[124] = 0;
	logicPortPos[124] = "-57 0 0";
	logicPortDir[124] = 5;
	logicPortUIName[124] = "Out60";
	
	logicPortType[125] = 0;
	logicPortPos[125] = "-59 0 0";
	logicPortDir[125] = 5;
	logicPortUIName[125] = "Out61";
	
	logicPortType[126] = 0;
	logicPortPos[126] = "-61 0 0";
	logicPortDir[126] = 5;
	logicPortUIName[126] = "Out62";
	
	logicPortType[127] = 0;
	logicPortPos[127] = "-63 0 0";
	logicPortDir[127] = 5;
	logicPortUIName[127] = "Out63";
	
	logicPortType[128] = 1;
	logicPortPos[128] = "63 0 0";
	logicPortDir[128] = 2;
	logicPortUIName[128] = "Clock";
	logicPortCauseUpdate[128] = true;
	
};
