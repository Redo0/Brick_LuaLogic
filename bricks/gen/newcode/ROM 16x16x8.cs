
datablock fxDtsBrickData(LogicGate_Rom16x16x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 16x16x8.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 16x16x8";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 16x16x8";
	logicUIName = "ROM 16x16x8";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "16 16 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 2047 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 17;
	
	isLogicRom = true;
	logicRomY = 16;
	logicRomZ = 8;
	logicRomX = 16;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "15 -15 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "13 -15 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "11 -15 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "9 -15 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "7 -15 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "5 -15 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 -15 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "1 -15 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "15 15 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "13 15 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "11 15 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "9 15 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O3";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "7 15 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O4";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "5 15 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O5";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "3 15 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O6";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "1 15 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O7";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "15 -15 0";
	logicPortDir[16] = 2;
	logicPortUIName[16] = "Clock";
	logicPortCauseUpdate[16] = true;
	
};
