
datablock fxDtsBrickData(LogicWire_1x1f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x1f";
	
	logicBrickSize = "1 1 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
