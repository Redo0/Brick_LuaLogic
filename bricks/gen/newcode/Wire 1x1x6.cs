
datablock fxDtsBrickData(LogicWire_1x1x6_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x6.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x6";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x6";
	
	logicBrickSize = "1 1 18";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
