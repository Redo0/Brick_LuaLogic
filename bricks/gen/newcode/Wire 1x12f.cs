
datablock fxDtsBrickData(LogicWire_1x12f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x12f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x12f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x12f";
	
	logicBrickSize = "1 12 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
