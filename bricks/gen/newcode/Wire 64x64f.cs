
datablock fxDtsBrickData(LogicWire_64x64f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 64x64f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 64x64f";
	
	category = "Logic Bricks";
	subCategory = "Wires Other";
	uiName = "Wire 64x64f";
	
	logicBrickSize = "64 64 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
