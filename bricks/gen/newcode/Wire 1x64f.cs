
datablock fxDtsBrickData(LogicWire_1x64f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x64f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x64f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x64f";
	
	logicBrickSize = "1 64 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
