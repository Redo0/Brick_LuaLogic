
datablock fxDtsBrickData(LogicWire_1x18f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x18f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x18f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x18f";
	
	logicBrickSize = "1 18 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
