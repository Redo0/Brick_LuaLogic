
datablock fxDtsBrickData(LogicGate_Buffer4Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 4 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 4 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 4 Bit";
	logicUIName = "Buffer 4 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 9)~=0 then " @
		"		Gate.setportstate(gate, 5, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 6, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 7, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, 4)) " @
		"	else " @
		"		Gate.setportstate(gate, 5, 0) " @
		"		Gate.setportstate(gate, 6, 0) " @
		"		Gate.setportstate(gate, 7, 0) " @
		"		Gate.setportstate(gate, 8, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 9;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-1 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-3 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "3 0 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "Out0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "1 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out1";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "-1 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out2";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "-3 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out3";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "3 0 0";
	logicPortDir[8] = 2;
	logicPortUIName[8] = "Clock";
	logicPortCauseUpdate[8] = true;
	
};
