
datablock fxDtsBrickData(LogicWire_1x60f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x60f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x60f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x60f";
	
	logicBrickSize = "1 60 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
