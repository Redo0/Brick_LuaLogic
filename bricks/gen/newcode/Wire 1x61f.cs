
datablock fxDtsBrickData(LogicWire_1x61f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x61f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x61f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x61f";
	
	logicBrickSize = "1 61 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
