
datablock fxDtsBrickData(LogicGate_Enabler64Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 64 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 64 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 64 Bit";
	logicUIName = "Enabler 64 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 129)~=0 then " @
		"		Gate.setportstate(gate, 65, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 66, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 67, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 68, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 69, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 70, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 71, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 72, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 73, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 74, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 75, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 76, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 77, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 78, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 79, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 80, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 81, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 82, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 83, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 84, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 85, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 86, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 87, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 88, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 89, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 90, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 91, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 92, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 93, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 94, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 95, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 96, Gate.getportstate(gate, 32)) " @
		"		Gate.setportstate(gate, 97, Gate.getportstate(gate, 33)) " @
		"		Gate.setportstate(gate, 98, Gate.getportstate(gate, 34)) " @
		"		Gate.setportstate(gate, 99, Gate.getportstate(gate, 35)) " @
		"		Gate.setportstate(gate, 100, Gate.getportstate(gate, 36)) " @
		"		Gate.setportstate(gate, 101, Gate.getportstate(gate, 37)) " @
		"		Gate.setportstate(gate, 102, Gate.getportstate(gate, 38)) " @
		"		Gate.setportstate(gate, 103, Gate.getportstate(gate, 39)) " @
		"		Gate.setportstate(gate, 104, Gate.getportstate(gate, 40)) " @
		"		Gate.setportstate(gate, 105, Gate.getportstate(gate, 41)) " @
		"		Gate.setportstate(gate, 106, Gate.getportstate(gate, 42)) " @
		"		Gate.setportstate(gate, 107, Gate.getportstate(gate, 43)) " @
		"		Gate.setportstate(gate, 108, Gate.getportstate(gate, 44)) " @
		"		Gate.setportstate(gate, 109, Gate.getportstate(gate, 45)) " @
		"		Gate.setportstate(gate, 110, Gate.getportstate(gate, 46)) " @
		"		Gate.setportstate(gate, 111, Gate.getportstate(gate, 47)) " @
		"		Gate.setportstate(gate, 112, Gate.getportstate(gate, 48)) " @
		"		Gate.setportstate(gate, 113, Gate.getportstate(gate, 49)) " @
		"		Gate.setportstate(gate, 114, Gate.getportstate(gate, 50)) " @
		"		Gate.setportstate(gate, 115, Gate.getportstate(gate, 51)) " @
		"		Gate.setportstate(gate, 116, Gate.getportstate(gate, 52)) " @
		"		Gate.setportstate(gate, 117, Gate.getportstate(gate, 53)) " @
		"		Gate.setportstate(gate, 118, Gate.getportstate(gate, 54)) " @
		"		Gate.setportstate(gate, 119, Gate.getportstate(gate, 55)) " @
		"		Gate.setportstate(gate, 120, Gate.getportstate(gate, 56)) " @
		"		Gate.setportstate(gate, 121, Gate.getportstate(gate, 57)) " @
		"		Gate.setportstate(gate, 122, Gate.getportstate(gate, 58)) " @
		"		Gate.setportstate(gate, 123, Gate.getportstate(gate, 59)) " @
		"		Gate.setportstate(gate, 124, Gate.getportstate(gate, 60)) " @
		"		Gate.setportstate(gate, 125, Gate.getportstate(gate, 61)) " @
		"		Gate.setportstate(gate, 126, Gate.getportstate(gate, 62)) " @
		"		Gate.setportstate(gate, 127, Gate.getportstate(gate, 63)) " @
		"		Gate.setportstate(gate, 128, Gate.getportstate(gate, 64)) " @
		"	else " @
		"		Gate.setportstate(gate, 65, 0) " @
		"		Gate.setportstate(gate, 66, 0) " @
		"		Gate.setportstate(gate, 67, 0) " @
		"		Gate.setportstate(gate, 68, 0) " @
		"		Gate.setportstate(gate, 69, 0) " @
		"		Gate.setportstate(gate, 70, 0) " @
		"		Gate.setportstate(gate, 71, 0) " @
		"		Gate.setportstate(gate, 72, 0) " @
		"		Gate.setportstate(gate, 73, 0) " @
		"		Gate.setportstate(gate, 74, 0) " @
		"		Gate.setportstate(gate, 75, 0) " @
		"		Gate.setportstate(gate, 76, 0) " @
		"		Gate.setportstate(gate, 77, 0) " @
		"		Gate.setportstate(gate, 78, 0) " @
		"		Gate.setportstate(gate, 79, 0) " @
		"		Gate.setportstate(gate, 80, 0) " @
		"		Gate.setportstate(gate, 81, 0) " @
		"		Gate.setportstate(gate, 82, 0) " @
		"		Gate.setportstate(gate, 83, 0) " @
		"		Gate.setportstate(gate, 84, 0) " @
		"		Gate.setportstate(gate, 85, 0) " @
		"		Gate.setportstate(gate, 86, 0) " @
		"		Gate.setportstate(gate, 87, 0) " @
		"		Gate.setportstate(gate, 88, 0) " @
		"		Gate.setportstate(gate, 89, 0) " @
		"		Gate.setportstate(gate, 90, 0) " @
		"		Gate.setportstate(gate, 91, 0) " @
		"		Gate.setportstate(gate, 92, 0) " @
		"		Gate.setportstate(gate, 93, 0) " @
		"		Gate.setportstate(gate, 94, 0) " @
		"		Gate.setportstate(gate, 95, 0) " @
		"		Gate.setportstate(gate, 96, 0) " @
		"		Gate.setportstate(gate, 97, 0) " @
		"		Gate.setportstate(gate, 98, 0) " @
		"		Gate.setportstate(gate, 99, 0) " @
		"		Gate.setportstate(gate, 100, 0) " @
		"		Gate.setportstate(gate, 101, 0) " @
		"		Gate.setportstate(gate, 102, 0) " @
		"		Gate.setportstate(gate, 103, 0) " @
		"		Gate.setportstate(gate, 104, 0) " @
		"		Gate.setportstate(gate, 105, 0) " @
		"		Gate.setportstate(gate, 106, 0) " @
		"		Gate.setportstate(gate, 107, 0) " @
		"		Gate.setportstate(gate, 108, 0) " @
		"		Gate.setportstate(gate, 109, 0) " @
		"		Gate.setportstate(gate, 110, 0) " @
		"		Gate.setportstate(gate, 111, 0) " @
		"		Gate.setportstate(gate, 112, 0) " @
		"		Gate.setportstate(gate, 113, 0) " @
		"		Gate.setportstate(gate, 114, 0) " @
		"		Gate.setportstate(gate, 115, 0) " @
		"		Gate.setportstate(gate, 116, 0) " @
		"		Gate.setportstate(gate, 117, 0) " @
		"		Gate.setportstate(gate, 118, 0) " @
		"		Gate.setportstate(gate, 119, 0) " @
		"		Gate.setportstate(gate, 120, 0) " @
		"		Gate.setportstate(gate, 121, 0) " @
		"		Gate.setportstate(gate, 122, 0) " @
		"		Gate.setportstate(gate, 123, 0) " @
		"		Gate.setportstate(gate, 124, 0) " @
		"		Gate.setportstate(gate, 125, 0) " @
		"		Gate.setportstate(gate, 126, 0) " @
		"		Gate.setportstate(gate, 127, 0) " @
		"		Gate.setportstate(gate, 128, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 129;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "41 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "39 0 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "In12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "37 0 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "In13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "35 0 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "In14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "33 0 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "In15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 1;
	logicPortPos[16] = "31 0 0";
	logicPortDir[16] = 3;
	logicPortUIName[16] = "In16";
	logicPortCauseUpdate[16] = true;
	
	logicPortType[17] = 1;
	logicPortPos[17] = "29 0 0";
	logicPortDir[17] = 3;
	logicPortUIName[17] = "In17";
	logicPortCauseUpdate[17] = true;
	
	logicPortType[18] = 1;
	logicPortPos[18] = "27 0 0";
	logicPortDir[18] = 3;
	logicPortUIName[18] = "In18";
	logicPortCauseUpdate[18] = true;
	
	logicPortType[19] = 1;
	logicPortPos[19] = "25 0 0";
	logicPortDir[19] = 3;
	logicPortUIName[19] = "In19";
	logicPortCauseUpdate[19] = true;
	
	logicPortType[20] = 1;
	logicPortPos[20] = "23 0 0";
	logicPortDir[20] = 3;
	logicPortUIName[20] = "In20";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 1;
	logicPortPos[21] = "21 0 0";
	logicPortDir[21] = 3;
	logicPortUIName[21] = "In21";
	logicPortCauseUpdate[21] = true;
	
	logicPortType[22] = 1;
	logicPortPos[22] = "19 0 0";
	logicPortDir[22] = 3;
	logicPortUIName[22] = "In22";
	logicPortCauseUpdate[22] = true;
	
	logicPortType[23] = 1;
	logicPortPos[23] = "17 0 0";
	logicPortDir[23] = 3;
	logicPortUIName[23] = "In23";
	logicPortCauseUpdate[23] = true;
	
	logicPortType[24] = 1;
	logicPortPos[24] = "15 0 0";
	logicPortDir[24] = 3;
	logicPortUIName[24] = "In24";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "13 0 0";
	logicPortDir[25] = 3;
	logicPortUIName[25] = "In25";
	logicPortCauseUpdate[25] = true;
	
	logicPortType[26] = 1;
	logicPortPos[26] = "11 0 0";
	logicPortDir[26] = 3;
	logicPortUIName[26] = "In26";
	logicPortCauseUpdate[26] = true;
	
	logicPortType[27] = 1;
	logicPortPos[27] = "9 0 0";
	logicPortDir[27] = 3;
	logicPortUIName[27] = "In27";
	logicPortCauseUpdate[27] = true;
	
	logicPortType[28] = 1;
	logicPortPos[28] = "7 0 0";
	logicPortDir[28] = 3;
	logicPortUIName[28] = "In28";
	logicPortCauseUpdate[28] = true;
	
	logicPortType[29] = 1;
	logicPortPos[29] = "5 0 0";
	logicPortDir[29] = 3;
	logicPortUIName[29] = "In29";
	logicPortCauseUpdate[29] = true;
	
	logicPortType[30] = 1;
	logicPortPos[30] = "3 0 0";
	logicPortDir[30] = 3;
	logicPortUIName[30] = "In30";
	logicPortCauseUpdate[30] = true;
	
	logicPortType[31] = 1;
	logicPortPos[31] = "1 0 0";
	logicPortDir[31] = 3;
	logicPortUIName[31] = "In31";
	logicPortCauseUpdate[31] = true;
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-1 0 0";
	logicPortDir[32] = 3;
	logicPortUIName[32] = "In32";
	logicPortCauseUpdate[32] = true;
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-3 0 0";
	logicPortDir[33] = 3;
	logicPortUIName[33] = "In33";
	logicPortCauseUpdate[33] = true;
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-5 0 0";
	logicPortDir[34] = 3;
	logicPortUIName[34] = "In34";
	logicPortCauseUpdate[34] = true;
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-7 0 0";
	logicPortDir[35] = 3;
	logicPortUIName[35] = "In35";
	logicPortCauseUpdate[35] = true;
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-9 0 0";
	logicPortDir[36] = 3;
	logicPortUIName[36] = "In36";
	logicPortCauseUpdate[36] = true;
	
	logicPortType[37] = 1;
	logicPortPos[37] = "-11 0 0";
	logicPortDir[37] = 3;
	logicPortUIName[37] = "In37";
	logicPortCauseUpdate[37] = true;
	
	logicPortType[38] = 1;
	logicPortPos[38] = "-13 0 0";
	logicPortDir[38] = 3;
	logicPortUIName[38] = "In38";
	logicPortCauseUpdate[38] = true;
	
	logicPortType[39] = 1;
	logicPortPos[39] = "-15 0 0";
	logicPortDir[39] = 3;
	logicPortUIName[39] = "In39";
	logicPortCauseUpdate[39] = true;
	
	logicPortType[40] = 1;
	logicPortPos[40] = "-17 0 0";
	logicPortDir[40] = 3;
	logicPortUIName[40] = "In40";
	logicPortCauseUpdate[40] = true;
	
	logicPortType[41] = 1;
	logicPortPos[41] = "-19 0 0";
	logicPortDir[41] = 3;
	logicPortUIName[41] = "In41";
	logicPortCauseUpdate[41] = true;
	
	logicPortType[42] = 1;
	logicPortPos[42] = "-21 0 0";
	logicPortDir[42] = 3;
	logicPortUIName[42] = "In42";
	logicPortCauseUpdate[42] = true;
	
	logicPortType[43] = 1;
	logicPortPos[43] = "-23 0 0";
	logicPortDir[43] = 3;
	logicPortUIName[43] = "In43";
	logicPortCauseUpdate[43] = true;
	
	logicPortType[44] = 1;
	logicPortPos[44] = "-25 0 0";
	logicPortDir[44] = 3;
	logicPortUIName[44] = "In44";
	logicPortCauseUpdate[44] = true;
	
	logicPortType[45] = 1;
	logicPortPos[45] = "-27 0 0";
	logicPortDir[45] = 3;
	logicPortUIName[45] = "In45";
	logicPortCauseUpdate[45] = true;
	
	logicPortType[46] = 1;
	logicPortPos[46] = "-29 0 0";
	logicPortDir[46] = 3;
	logicPortUIName[46] = "In46";
	logicPortCauseUpdate[46] = true;
	
	logicPortType[47] = 1;
	logicPortPos[47] = "-31 0 0";
	logicPortDir[47] = 3;
	logicPortUIName[47] = "In47";
	logicPortCauseUpdate[47] = true;
	
	logicPortType[48] = 1;
	logicPortPos[48] = "-33 0 0";
	logicPortDir[48] = 3;
	logicPortUIName[48] = "In48";
	logicPortCauseUpdate[48] = true;
	
	logicPortType[49] = 1;
	logicPortPos[49] = "-35 0 0";
	logicPortDir[49] = 3;
	logicPortUIName[49] = "In49";
	logicPortCauseUpdate[49] = true;
	
	logicPortType[50] = 1;
	logicPortPos[50] = "-37 0 0";
	logicPortDir[50] = 3;
	logicPortUIName[50] = "In50";
	logicPortCauseUpdate[50] = true;
	
	logicPortType[51] = 1;
	logicPortPos[51] = "-39 0 0";
	logicPortDir[51] = 3;
	logicPortUIName[51] = "In51";
	logicPortCauseUpdate[51] = true;
	
	logicPortType[52] = 1;
	logicPortPos[52] = "-41 0 0";
	logicPortDir[52] = 3;
	logicPortUIName[52] = "In52";
	logicPortCauseUpdate[52] = true;
	
	logicPortType[53] = 1;
	logicPortPos[53] = "-43 0 0";
	logicPortDir[53] = 3;
	logicPortUIName[53] = "In53";
	logicPortCauseUpdate[53] = true;
	
	logicPortType[54] = 1;
	logicPortPos[54] = "-45 0 0";
	logicPortDir[54] = 3;
	logicPortUIName[54] = "In54";
	logicPortCauseUpdate[54] = true;
	
	logicPortType[55] = 1;
	logicPortPos[55] = "-47 0 0";
	logicPortDir[55] = 3;
	logicPortUIName[55] = "In55";
	logicPortCauseUpdate[55] = true;
	
	logicPortType[56] = 1;
	logicPortPos[56] = "-49 0 0";
	logicPortDir[56] = 3;
	logicPortUIName[56] = "In56";
	logicPortCauseUpdate[56] = true;
	
	logicPortType[57] = 1;
	logicPortPos[57] = "-51 0 0";
	logicPortDir[57] = 3;
	logicPortUIName[57] = "In57";
	logicPortCauseUpdate[57] = true;
	
	logicPortType[58] = 1;
	logicPortPos[58] = "-53 0 0";
	logicPortDir[58] = 3;
	logicPortUIName[58] = "In58";
	logicPortCauseUpdate[58] = true;
	
	logicPortType[59] = 1;
	logicPortPos[59] = "-55 0 0";
	logicPortDir[59] = 3;
	logicPortUIName[59] = "In59";
	logicPortCauseUpdate[59] = true;
	
	logicPortType[60] = 1;
	logicPortPos[60] = "-57 0 0";
	logicPortDir[60] = 3;
	logicPortUIName[60] = "In60";
	logicPortCauseUpdate[60] = true;
	
	logicPortType[61] = 1;
	logicPortPos[61] = "-59 0 0";
	logicPortDir[61] = 3;
	logicPortUIName[61] = "In61";
	logicPortCauseUpdate[61] = true;
	
	logicPortType[62] = 1;
	logicPortPos[62] = "-61 0 0";
	logicPortDir[62] = 3;
	logicPortUIName[62] = "In62";
	logicPortCauseUpdate[62] = true;
	
	logicPortType[63] = 1;
	logicPortPos[63] = "-63 0 0";
	logicPortDir[63] = 3;
	logicPortUIName[63] = "In63";
	logicPortCauseUpdate[63] = true;
	
	logicPortType[64] = 0;
	logicPortPos[64] = "63 0 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "Out0";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "61 0 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "Out1";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "59 0 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "Out2";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "57 0 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "Out3";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "55 0 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "Out4";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "53 0 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "Out5";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "51 0 0";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "Out6";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "49 0 0";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "Out7";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "47 0 0";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "Out8";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "45 0 0";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "Out9";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "43 0 0";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "Out10";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "41 0 0";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "Out11";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "39 0 0";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "Out12";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "37 0 0";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "Out13";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "35 0 0";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "Out14";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "33 0 0";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "Out15";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "31 0 0";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "Out16";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "29 0 0";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "Out17";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "27 0 0";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "Out18";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "25 0 0";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "Out19";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "23 0 0";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "Out20";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "21 0 0";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "Out21";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "19 0 0";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "Out22";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "17 0 0";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "Out23";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "15 0 0";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "Out24";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "13 0 0";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "Out25";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "11 0 0";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "Out26";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "9 0 0";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "Out27";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "7 0 0";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "Out28";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "5 0 0";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "Out29";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "3 0 0";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "Out30";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "1 0 0";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "Out31";
	
	logicPortType[96] = 0;
	logicPortPos[96] = "-1 0 0";
	logicPortDir[96] = 1;
	logicPortUIName[96] = "Out32";
	
	logicPortType[97] = 0;
	logicPortPos[97] = "-3 0 0";
	logicPortDir[97] = 1;
	logicPortUIName[97] = "Out33";
	
	logicPortType[98] = 0;
	logicPortPos[98] = "-5 0 0";
	logicPortDir[98] = 1;
	logicPortUIName[98] = "Out34";
	
	logicPortType[99] = 0;
	logicPortPos[99] = "-7 0 0";
	logicPortDir[99] = 1;
	logicPortUIName[99] = "Out35";
	
	logicPortType[100] = 0;
	logicPortPos[100] = "-9 0 0";
	logicPortDir[100] = 1;
	logicPortUIName[100] = "Out36";
	
	logicPortType[101] = 0;
	logicPortPos[101] = "-11 0 0";
	logicPortDir[101] = 1;
	logicPortUIName[101] = "Out37";
	
	logicPortType[102] = 0;
	logicPortPos[102] = "-13 0 0";
	logicPortDir[102] = 1;
	logicPortUIName[102] = "Out38";
	
	logicPortType[103] = 0;
	logicPortPos[103] = "-15 0 0";
	logicPortDir[103] = 1;
	logicPortUIName[103] = "Out39";
	
	logicPortType[104] = 0;
	logicPortPos[104] = "-17 0 0";
	logicPortDir[104] = 1;
	logicPortUIName[104] = "Out40";
	
	logicPortType[105] = 0;
	logicPortPos[105] = "-19 0 0";
	logicPortDir[105] = 1;
	logicPortUIName[105] = "Out41";
	
	logicPortType[106] = 0;
	logicPortPos[106] = "-21 0 0";
	logicPortDir[106] = 1;
	logicPortUIName[106] = "Out42";
	
	logicPortType[107] = 0;
	logicPortPos[107] = "-23 0 0";
	logicPortDir[107] = 1;
	logicPortUIName[107] = "Out43";
	
	logicPortType[108] = 0;
	logicPortPos[108] = "-25 0 0";
	logicPortDir[108] = 1;
	logicPortUIName[108] = "Out44";
	
	logicPortType[109] = 0;
	logicPortPos[109] = "-27 0 0";
	logicPortDir[109] = 1;
	logicPortUIName[109] = "Out45";
	
	logicPortType[110] = 0;
	logicPortPos[110] = "-29 0 0";
	logicPortDir[110] = 1;
	logicPortUIName[110] = "Out46";
	
	logicPortType[111] = 0;
	logicPortPos[111] = "-31 0 0";
	logicPortDir[111] = 1;
	logicPortUIName[111] = "Out47";
	
	logicPortType[112] = 0;
	logicPortPos[112] = "-33 0 0";
	logicPortDir[112] = 1;
	logicPortUIName[112] = "Out48";
	
	logicPortType[113] = 0;
	logicPortPos[113] = "-35 0 0";
	logicPortDir[113] = 1;
	logicPortUIName[113] = "Out49";
	
	logicPortType[114] = 0;
	logicPortPos[114] = "-37 0 0";
	logicPortDir[114] = 1;
	logicPortUIName[114] = "Out50";
	
	logicPortType[115] = 0;
	logicPortPos[115] = "-39 0 0";
	logicPortDir[115] = 1;
	logicPortUIName[115] = "Out51";
	
	logicPortType[116] = 0;
	logicPortPos[116] = "-41 0 0";
	logicPortDir[116] = 1;
	logicPortUIName[116] = "Out52";
	
	logicPortType[117] = 0;
	logicPortPos[117] = "-43 0 0";
	logicPortDir[117] = 1;
	logicPortUIName[117] = "Out53";
	
	logicPortType[118] = 0;
	logicPortPos[118] = "-45 0 0";
	logicPortDir[118] = 1;
	logicPortUIName[118] = "Out54";
	
	logicPortType[119] = 0;
	logicPortPos[119] = "-47 0 0";
	logicPortDir[119] = 1;
	logicPortUIName[119] = "Out55";
	
	logicPortType[120] = 0;
	logicPortPos[120] = "-49 0 0";
	logicPortDir[120] = 1;
	logicPortUIName[120] = "Out56";
	
	logicPortType[121] = 0;
	logicPortPos[121] = "-51 0 0";
	logicPortDir[121] = 1;
	logicPortUIName[121] = "Out57";
	
	logicPortType[122] = 0;
	logicPortPos[122] = "-53 0 0";
	logicPortDir[122] = 1;
	logicPortUIName[122] = "Out58";
	
	logicPortType[123] = 0;
	logicPortPos[123] = "-55 0 0";
	logicPortDir[123] = 1;
	logicPortUIName[123] = "Out59";
	
	logicPortType[124] = 0;
	logicPortPos[124] = "-57 0 0";
	logicPortDir[124] = 1;
	logicPortUIName[124] = "Out60";
	
	logicPortType[125] = 0;
	logicPortPos[125] = "-59 0 0";
	logicPortDir[125] = 1;
	logicPortUIName[125] = "Out61";
	
	logicPortType[126] = 0;
	logicPortPos[126] = "-61 0 0";
	logicPortDir[126] = 1;
	logicPortUIName[126] = "Out62";
	
	logicPortType[127] = 0;
	logicPortPos[127] = "-63 0 0";
	logicPortDir[127] = 1;
	logicPortUIName[127] = "Out63";
	
	logicPortType[128] = 1;
	logicPortPos[128] = "63 0 0";
	logicPortDir[128] = 2;
	logicPortUIName[128] = "Clock";
	logicPortCauseUpdate[128] = true;
	
};
