
datablock fxDtsBrickData(LogicWire_1x49f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x49f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x49f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x49f";
	
	logicBrickSize = "1 49 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
