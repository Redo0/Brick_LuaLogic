
datablock fxDtsBrickData(LogicWire_1x33f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x33f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x33f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x33f";
	
	logicBrickSize = "1 33 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
