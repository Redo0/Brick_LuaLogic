
datablock fxDtsBrickData(LogicGate_Demux5_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 5 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 5 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 5 Bit";
	logicUIName = "Demux 5 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 6 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 38)~=0 then " @
		"		local idx = 6 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 38;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "31 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out0";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "29 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out1";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "27 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out2";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "25 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out3";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "23 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out4";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "21 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out5";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "19 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out6";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "17 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out7";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "15 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out8";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "13 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out9";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "11 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out10";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "9 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out11";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "7 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out12";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "5 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out13";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "3 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out14";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "1 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out15";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "-1 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out16";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "-3 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out17";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "-5 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out18";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "-7 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out19";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-9 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out20";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-11 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out21";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-13 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out22";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-15 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out23";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-17 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out24";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "-19 0 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out25";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "-21 0 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out26";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "-23 0 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out27";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "-25 0 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out28";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "-27 0 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out29";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "-29 0 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out30";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "-31 0 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out31";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "31 0 0";
	logicPortDir[37] = 2;
	logicPortUIName[37] = "Enable";
	logicPortCauseUpdate[37] = true;
	
};
