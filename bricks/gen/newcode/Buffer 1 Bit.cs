
datablock fxDtsBrickData(LogicGate_Buffer1Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 1 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 1 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 1 Bit";
	logicUIName = "Buffer 1 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 3)~=0 then " @
		"		Gate.setportstate(gate, 2, Gate.getportstate(gate, 1)) " @
		"	else " @
		"		Gate.setportstate(gate, 2, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 3;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 0;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = 1;
	logicPortUIName[1] = "Out0";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 0";
	logicPortDir[2] = 2;
	logicPortUIName[2] = "Clock";
	logicPortCauseUpdate[2] = true;
	
};
