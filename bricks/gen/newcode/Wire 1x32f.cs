
datablock fxDtsBrickData(LogicWire_1x32f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x32f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x32f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x32f";
	
	logicBrickSize = "1 32 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
