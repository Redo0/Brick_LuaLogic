
datablock fxDtsBrickData(LogicGate_DFlipFlop4BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 4 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 4 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 4 Bit Down";
	logicUIName = "D FlipFlop 4 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 9)~=0 then " @
		"		Gate.setportstate(gate, 5, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 6, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 7, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, 4)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 9;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-1 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-3 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "3 0 0";
	logicPortDir[4] = 5;
	logicPortUIName[4] = "Out0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "1 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "Out1";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "-1 0 0";
	logicPortDir[6] = 5;
	logicPortUIName[6] = "Out2";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "-3 0 0";
	logicPortDir[7] = 5;
	logicPortUIName[7] = "Out3";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "3 0 0";
	logicPortDir[8] = 2;
	logicPortUIName[8] = "Clock";
	logicPortCauseUpdate[8] = true;
	
};
