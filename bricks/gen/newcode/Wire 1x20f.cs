
datablock fxDtsBrickData(LogicWire_1x20f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x20f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x20f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x20f";
	
	logicBrickSize = "1 20 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
