
datablock fxDtsBrickData(LogicWire_1x23f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x23f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x23f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x23f";
	
	logicBrickSize = "1 23 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
