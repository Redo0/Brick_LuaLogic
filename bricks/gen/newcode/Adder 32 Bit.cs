
datablock fxDtsBrickData(LogicGate_Adder32Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 32 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 32 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 32 Bit";
	logicUIName = "Adder 32 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 33) + Gate.getportstate(gate, 97)) " @
		" + ((Gate.getportstate(gate, 2) + Gate.getportstate(gate, 34)) * 2) " @
		" + ((Gate.getportstate(gate, 3) + Gate.getportstate(gate, 35)) * 4) " @
		" + ((Gate.getportstate(gate, 4) + Gate.getportstate(gate, 36)) * 8) " @
		" + ((Gate.getportstate(gate, 5) + Gate.getportstate(gate, 37)) * 16) " @
		" + ((Gate.getportstate(gate, 6) + Gate.getportstate(gate, 38)) * 32) " @
		" + ((Gate.getportstate(gate, 7) + Gate.getportstate(gate, 39)) * 64) " @
		" + ((Gate.getportstate(gate, 8) + Gate.getportstate(gate, 40)) * 128) " @
		" + ((Gate.getportstate(gate, 9) + Gate.getportstate(gate, 41)) * 256) " @
		" + ((Gate.getportstate(gate, 10) + Gate.getportstate(gate, 42)) * 512) " @
		" + ((Gate.getportstate(gate, 11) + Gate.getportstate(gate, 43)) * 1024) " @
		" + ((Gate.getportstate(gate, 12) + Gate.getportstate(gate, 44)) * 2048) " @
		" + ((Gate.getportstate(gate, 13) + Gate.getportstate(gate, 45)) * 4096) " @
		" + ((Gate.getportstate(gate, 14) + Gate.getportstate(gate, 46)) * 8192) " @
		" + ((Gate.getportstate(gate, 15) + Gate.getportstate(gate, 47)) * 16384) " @
		" + ((Gate.getportstate(gate, 16) + Gate.getportstate(gate, 48)) * 32768) " @
		" + ((Gate.getportstate(gate, 17) + Gate.getportstate(gate, 49)) * 65536) " @
		" + ((Gate.getportstate(gate, 18) + Gate.getportstate(gate, 50)) * 131072) " @
		" + ((Gate.getportstate(gate, 19) + Gate.getportstate(gate, 51)) * 262144) " @
		" + ((Gate.getportstate(gate, 20) + Gate.getportstate(gate, 52)) * 524288) " @
		" + ((Gate.getportstate(gate, 21) + Gate.getportstate(gate, 53)) * 1048576) " @
		" + ((Gate.getportstate(gate, 22) + Gate.getportstate(gate, 54)) * 2097152) " @
		" + ((Gate.getportstate(gate, 23) + Gate.getportstate(gate, 55)) * 4194304) " @
		" + ((Gate.getportstate(gate, 24) + Gate.getportstate(gate, 56)) * 8388608) " @
		" + ((Gate.getportstate(gate, 25) + Gate.getportstate(gate, 57)) * 16777216) " @
		" + ((Gate.getportstate(gate, 26) + Gate.getportstate(gate, 58)) * 33554432) " @
		" + ((Gate.getportstate(gate, 27) + Gate.getportstate(gate, 59)) * 67108864) " @
		" + ((Gate.getportstate(gate, 28) + Gate.getportstate(gate, 60)) * 134217728) " @
		" + ((Gate.getportstate(gate, 29) + Gate.getportstate(gate, 61)) * 268435456) " @
		" + ((Gate.getportstate(gate, 30) + Gate.getportstate(gate, 62)) * 536870912) " @
		" + ((Gate.getportstate(gate, 31) + Gate.getportstate(gate, 63)) * 1073741824) " @
		" + ((Gate.getportstate(gate, 32) + Gate.getportstate(gate, 64)) * 2147483648) " @
		") " @
		"if val >= 4294967296 then val = val-4294967296; Gate.setportstate(gate, 98, 1); else Gate.setportstate(gate, 98, 0) end " @
		"if val >= 2147483648 then val = val-2147483648; Gate.setportstate(gate, 96, 1); else Gate.setportstate(gate, 96, 0) end " @
		"if val >= 1073741824 then val = val-1073741824; Gate.setportstate(gate, 95, 1); else Gate.setportstate(gate, 95, 0) end " @
		"if val >= 536870912 then val = val-536870912; Gate.setportstate(gate, 94, 1); else Gate.setportstate(gate, 94, 0) end " @
		"if val >= 268435456 then val = val-268435456; Gate.setportstate(gate, 93, 1); else Gate.setportstate(gate, 93, 0) end " @
		"if val >= 134217728 then val = val-134217728; Gate.setportstate(gate, 92, 1); else Gate.setportstate(gate, 92, 0) end " @
		"if val >= 67108864 then val = val-67108864; Gate.setportstate(gate, 91, 1); else Gate.setportstate(gate, 91, 0) end " @
		"if val >= 33554432 then val = val-33554432; Gate.setportstate(gate, 90, 1); else Gate.setportstate(gate, 90, 0) end " @
		"if val >= 16777216 then val = val-16777216; Gate.setportstate(gate, 89, 1); else Gate.setportstate(gate, 89, 0) end " @
		"if val >= 8388608 then val = val-8388608; Gate.setportstate(gate, 88, 1); else Gate.setportstate(gate, 88, 0) end " @
		"if val >= 4194304 then val = val-4194304; Gate.setportstate(gate, 87, 1); else Gate.setportstate(gate, 87, 0) end " @
		"if val >= 2097152 then val = val-2097152; Gate.setportstate(gate, 86, 1); else Gate.setportstate(gate, 86, 0) end " @
		"if val >= 1048576 then val = val-1048576; Gate.setportstate(gate, 85, 1); else Gate.setportstate(gate, 85, 0) end " @
		"if val >= 524288 then val = val-524288; Gate.setportstate(gate, 84, 1); else Gate.setportstate(gate, 84, 0) end " @
		"if val >= 262144 then val = val-262144; Gate.setportstate(gate, 83, 1); else Gate.setportstate(gate, 83, 0) end " @
		"if val >= 131072 then val = val-131072; Gate.setportstate(gate, 82, 1); else Gate.setportstate(gate, 82, 0) end " @
		"if val >= 65536 then val = val-65536; Gate.setportstate(gate, 81, 1); else Gate.setportstate(gate, 81, 0) end " @
		"if val >= 32768 then val = val-32768; Gate.setportstate(gate, 80, 1); else Gate.setportstate(gate, 80, 0) end " @
		"if val >= 16384 then val = val-16384; Gate.setportstate(gate, 79, 1); else Gate.setportstate(gate, 79, 0) end " @
		"if val >= 8192 then val = val-8192; Gate.setportstate(gate, 78, 1); else Gate.setportstate(gate, 78, 0) end " @
		"if val >= 4096 then val = val-4096; Gate.setportstate(gate, 77, 1); else Gate.setportstate(gate, 77, 0) end " @
		"if val >= 2048 then val = val-2048; Gate.setportstate(gate, 76, 1); else Gate.setportstate(gate, 76, 0) end " @
		"if val >= 1024 then val = val-1024; Gate.setportstate(gate, 75, 1); else Gate.setportstate(gate, 75, 0) end " @
		"if val >= 512 then val = val-512; Gate.setportstate(gate, 74, 1); else Gate.setportstate(gate, 74, 0) end " @
		"if val >= 256 then val = val-256; Gate.setportstate(gate, 73, 1); else Gate.setportstate(gate, 73, 0) end " @
		"if val >= 128 then val = val-128; Gate.setportstate(gate, 72, 1); else Gate.setportstate(gate, 72, 0) end " @
		"if val >= 64 then val = val-64; Gate.setportstate(gate, 71, 1); else Gate.setportstate(gate, 71, 0) end " @
		"if val >= 32 then val = val-32; Gate.setportstate(gate, 70, 1); else Gate.setportstate(gate, 70, 0) end " @
		"if val >= 16 then val = val-16; Gate.setportstate(gate, 69, 1); else Gate.setportstate(gate, 69, 0) end " @
		"if val >= 8 then val = val-8; Gate.setportstate(gate, 68, 1); else Gate.setportstate(gate, 68, 0) end " @
		"if val >= 4 then val = val-4; Gate.setportstate(gate, 67, 1); else Gate.setportstate(gate, 67, 0) end " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 66, 1); else Gate.setportstate(gate, 66, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 65, 1); else Gate.setportstate(gate, 65, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 98;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -1 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -1 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -1 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -1 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -1 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 -1 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 -1 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "41 -1 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "A11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "39 -1 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "A12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "37 -1 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "A13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "35 -1 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "A14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "33 -1 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "A15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 1;
	logicPortPos[16] = "31 -1 0";
	logicPortDir[16] = 3;
	logicPortUIName[16] = "A16";
	logicPortCauseUpdate[16] = true;
	
	logicPortType[17] = 1;
	logicPortPos[17] = "29 -1 0";
	logicPortDir[17] = 3;
	logicPortUIName[17] = "A17";
	logicPortCauseUpdate[17] = true;
	
	logicPortType[18] = 1;
	logicPortPos[18] = "27 -1 0";
	logicPortDir[18] = 3;
	logicPortUIName[18] = "A18";
	logicPortCauseUpdate[18] = true;
	
	logicPortType[19] = 1;
	logicPortPos[19] = "25 -1 0";
	logicPortDir[19] = 3;
	logicPortUIName[19] = "A19";
	logicPortCauseUpdate[19] = true;
	
	logicPortType[20] = 1;
	logicPortPos[20] = "23 -1 0";
	logicPortDir[20] = 3;
	logicPortUIName[20] = "A20";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 1;
	logicPortPos[21] = "21 -1 0";
	logicPortDir[21] = 3;
	logicPortUIName[21] = "A21";
	logicPortCauseUpdate[21] = true;
	
	logicPortType[22] = 1;
	logicPortPos[22] = "19 -1 0";
	logicPortDir[22] = 3;
	logicPortUIName[22] = "A22";
	logicPortCauseUpdate[22] = true;
	
	logicPortType[23] = 1;
	logicPortPos[23] = "17 -1 0";
	logicPortDir[23] = 3;
	logicPortUIName[23] = "A23";
	logicPortCauseUpdate[23] = true;
	
	logicPortType[24] = 1;
	logicPortPos[24] = "15 -1 0";
	logicPortDir[24] = 3;
	logicPortUIName[24] = "A24";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "13 -1 0";
	logicPortDir[25] = 3;
	logicPortUIName[25] = "A25";
	logicPortCauseUpdate[25] = true;
	
	logicPortType[26] = 1;
	logicPortPos[26] = "11 -1 0";
	logicPortDir[26] = 3;
	logicPortUIName[26] = "A26";
	logicPortCauseUpdate[26] = true;
	
	logicPortType[27] = 1;
	logicPortPos[27] = "9 -1 0";
	logicPortDir[27] = 3;
	logicPortUIName[27] = "A27";
	logicPortCauseUpdate[27] = true;
	
	logicPortType[28] = 1;
	logicPortPos[28] = "7 -1 0";
	logicPortDir[28] = 3;
	logicPortUIName[28] = "A28";
	logicPortCauseUpdate[28] = true;
	
	logicPortType[29] = 1;
	logicPortPos[29] = "5 -1 0";
	logicPortDir[29] = 3;
	logicPortUIName[29] = "A29";
	logicPortCauseUpdate[29] = true;
	
	logicPortType[30] = 1;
	logicPortPos[30] = "3 -1 0";
	logicPortDir[30] = 3;
	logicPortUIName[30] = "A30";
	logicPortCauseUpdate[30] = true;
	
	logicPortType[31] = 1;
	logicPortPos[31] = "1 -1 0";
	logicPortDir[31] = 3;
	logicPortUIName[31] = "A31";
	logicPortCauseUpdate[31] = true;
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-1 -1 0";
	logicPortDir[32] = 3;
	logicPortUIName[32] = "B0";
	logicPortCauseUpdate[32] = true;
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-3 -1 0";
	logicPortDir[33] = 3;
	logicPortUIName[33] = "B1";
	logicPortCauseUpdate[33] = true;
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-5 -1 0";
	logicPortDir[34] = 3;
	logicPortUIName[34] = "B2";
	logicPortCauseUpdate[34] = true;
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-7 -1 0";
	logicPortDir[35] = 3;
	logicPortUIName[35] = "B3";
	logicPortCauseUpdate[35] = true;
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-9 -1 0";
	logicPortDir[36] = 3;
	logicPortUIName[36] = "B4";
	logicPortCauseUpdate[36] = true;
	
	logicPortType[37] = 1;
	logicPortPos[37] = "-11 -1 0";
	logicPortDir[37] = 3;
	logicPortUIName[37] = "B5";
	logicPortCauseUpdate[37] = true;
	
	logicPortType[38] = 1;
	logicPortPos[38] = "-13 -1 0";
	logicPortDir[38] = 3;
	logicPortUIName[38] = "B6";
	logicPortCauseUpdate[38] = true;
	
	logicPortType[39] = 1;
	logicPortPos[39] = "-15 -1 0";
	logicPortDir[39] = 3;
	logicPortUIName[39] = "B7";
	logicPortCauseUpdate[39] = true;
	
	logicPortType[40] = 1;
	logicPortPos[40] = "-17 -1 0";
	logicPortDir[40] = 3;
	logicPortUIName[40] = "B8";
	logicPortCauseUpdate[40] = true;
	
	logicPortType[41] = 1;
	logicPortPos[41] = "-19 -1 0";
	logicPortDir[41] = 3;
	logicPortUIName[41] = "B9";
	logicPortCauseUpdate[41] = true;
	
	logicPortType[42] = 1;
	logicPortPos[42] = "-21 -1 0";
	logicPortDir[42] = 3;
	logicPortUIName[42] = "B10";
	logicPortCauseUpdate[42] = true;
	
	logicPortType[43] = 1;
	logicPortPos[43] = "-23 -1 0";
	logicPortDir[43] = 3;
	logicPortUIName[43] = "B11";
	logicPortCauseUpdate[43] = true;
	
	logicPortType[44] = 1;
	logicPortPos[44] = "-25 -1 0";
	logicPortDir[44] = 3;
	logicPortUIName[44] = "B12";
	logicPortCauseUpdate[44] = true;
	
	logicPortType[45] = 1;
	logicPortPos[45] = "-27 -1 0";
	logicPortDir[45] = 3;
	logicPortUIName[45] = "B13";
	logicPortCauseUpdate[45] = true;
	
	logicPortType[46] = 1;
	logicPortPos[46] = "-29 -1 0";
	logicPortDir[46] = 3;
	logicPortUIName[46] = "B14";
	logicPortCauseUpdate[46] = true;
	
	logicPortType[47] = 1;
	logicPortPos[47] = "-31 -1 0";
	logicPortDir[47] = 3;
	logicPortUIName[47] = "B15";
	logicPortCauseUpdate[47] = true;
	
	logicPortType[48] = 1;
	logicPortPos[48] = "-33 -1 0";
	logicPortDir[48] = 3;
	logicPortUIName[48] = "B16";
	logicPortCauseUpdate[48] = true;
	
	logicPortType[49] = 1;
	logicPortPos[49] = "-35 -1 0";
	logicPortDir[49] = 3;
	logicPortUIName[49] = "B17";
	logicPortCauseUpdate[49] = true;
	
	logicPortType[50] = 1;
	logicPortPos[50] = "-37 -1 0";
	logicPortDir[50] = 3;
	logicPortUIName[50] = "B18";
	logicPortCauseUpdate[50] = true;
	
	logicPortType[51] = 1;
	logicPortPos[51] = "-39 -1 0";
	logicPortDir[51] = 3;
	logicPortUIName[51] = "B19";
	logicPortCauseUpdate[51] = true;
	
	logicPortType[52] = 1;
	logicPortPos[52] = "-41 -1 0";
	logicPortDir[52] = 3;
	logicPortUIName[52] = "B20";
	logicPortCauseUpdate[52] = true;
	
	logicPortType[53] = 1;
	logicPortPos[53] = "-43 -1 0";
	logicPortDir[53] = 3;
	logicPortUIName[53] = "B21";
	logicPortCauseUpdate[53] = true;
	
	logicPortType[54] = 1;
	logicPortPos[54] = "-45 -1 0";
	logicPortDir[54] = 3;
	logicPortUIName[54] = "B22";
	logicPortCauseUpdate[54] = true;
	
	logicPortType[55] = 1;
	logicPortPos[55] = "-47 -1 0";
	logicPortDir[55] = 3;
	logicPortUIName[55] = "B23";
	logicPortCauseUpdate[55] = true;
	
	logicPortType[56] = 1;
	logicPortPos[56] = "-49 -1 0";
	logicPortDir[56] = 3;
	logicPortUIName[56] = "B24";
	logicPortCauseUpdate[56] = true;
	
	logicPortType[57] = 1;
	logicPortPos[57] = "-51 -1 0";
	logicPortDir[57] = 3;
	logicPortUIName[57] = "B25";
	logicPortCauseUpdate[57] = true;
	
	logicPortType[58] = 1;
	logicPortPos[58] = "-53 -1 0";
	logicPortDir[58] = 3;
	logicPortUIName[58] = "B26";
	logicPortCauseUpdate[58] = true;
	
	logicPortType[59] = 1;
	logicPortPos[59] = "-55 -1 0";
	logicPortDir[59] = 3;
	logicPortUIName[59] = "B27";
	logicPortCauseUpdate[59] = true;
	
	logicPortType[60] = 1;
	logicPortPos[60] = "-57 -1 0";
	logicPortDir[60] = 3;
	logicPortUIName[60] = "B28";
	logicPortCauseUpdate[60] = true;
	
	logicPortType[61] = 1;
	logicPortPos[61] = "-59 -1 0";
	logicPortDir[61] = 3;
	logicPortUIName[61] = "B29";
	logicPortCauseUpdate[61] = true;
	
	logicPortType[62] = 1;
	logicPortPos[62] = "-61 -1 0";
	logicPortDir[62] = 3;
	logicPortUIName[62] = "B30";
	logicPortCauseUpdate[62] = true;
	
	logicPortType[63] = 1;
	logicPortPos[63] = "-63 -1 0";
	logicPortDir[63] = 3;
	logicPortUIName[63] = "B31";
	logicPortCauseUpdate[63] = true;
	
	logicPortType[64] = 0;
	logicPortPos[64] = "63 1 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "O0";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "61 1 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "O1";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "59 1 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "O2";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "57 1 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "O3";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "55 1 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "O4";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "53 1 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "O5";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "51 1 0";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "O6";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "49 1 0";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "O7";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "47 1 0";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "O8";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "45 1 0";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "O9";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "43 1 0";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "O10";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "41 1 0";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "O11";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "39 1 0";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "O12";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "37 1 0";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "O13";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "35 1 0";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "O14";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "33 1 0";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "O15";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "31 1 0";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "O16";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "29 1 0";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "O17";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "27 1 0";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "O18";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "25 1 0";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "O19";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "23 1 0";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "O20";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "21 1 0";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "O21";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "19 1 0";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "O22";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "17 1 0";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "O23";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "15 1 0";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "O24";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "13 1 0";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "O25";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "11 1 0";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "O26";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "9 1 0";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "O27";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "7 1 0";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "O28";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "5 1 0";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "O29";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "3 1 0";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "O30";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "1 1 0";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "O31";
	
	logicPortType[96] = 1;
	logicPortPos[96] = "63 -1 0";
	logicPortDir[96] = 2;
	logicPortUIName[96] = "CIn";
	logicPortCauseUpdate[96] = true;
	
	logicPortType[97] = 0;
	logicPortPos[97] = "-63 -1 0";
	logicPortDir[97] = 0;
	logicPortUIName[97] = "COut";
	logicPortCauseUpdate[97] = true;
	
};
