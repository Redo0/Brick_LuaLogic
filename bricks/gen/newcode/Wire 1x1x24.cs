
datablock fxDtsBrickData(LogicWire_1x1x24_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x24.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x24";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x24";
	
	logicBrickSize = "1 1 72";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
