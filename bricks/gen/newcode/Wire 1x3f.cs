
datablock fxDtsBrickData(LogicWire_1x3f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x3f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x3f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x3f";
	
	logicBrickSize = "1 3 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
