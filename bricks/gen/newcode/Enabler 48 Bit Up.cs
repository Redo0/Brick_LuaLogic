
datablock fxDtsBrickData(LogicGate_Enabler48BitUp_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Enabler 48 Bit Up.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Enabler 48 Bit Up";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Enabler 48 Bit Up";
	logicUIName = "Enabler 48 Bit Up";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "48 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 97)~=0 then " @
		"		Gate.setportstate(gate, 49, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 50, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 51, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 52, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 53, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 54, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 55, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 56, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 57, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 58, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 59, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 60, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 61, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 62, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 63, Gate.getportstate(gate, 15)) " @
		"		Gate.setportstate(gate, 64, Gate.getportstate(gate, 16)) " @
		"		Gate.setportstate(gate, 65, Gate.getportstate(gate, 17)) " @
		"		Gate.setportstate(gate, 66, Gate.getportstate(gate, 18)) " @
		"		Gate.setportstate(gate, 67, Gate.getportstate(gate, 19)) " @
		"		Gate.setportstate(gate, 68, Gate.getportstate(gate, 20)) " @
		"		Gate.setportstate(gate, 69, Gate.getportstate(gate, 21)) " @
		"		Gate.setportstate(gate, 70, Gate.getportstate(gate, 22)) " @
		"		Gate.setportstate(gate, 71, Gate.getportstate(gate, 23)) " @
		"		Gate.setportstate(gate, 72, Gate.getportstate(gate, 24)) " @
		"		Gate.setportstate(gate, 73, Gate.getportstate(gate, 25)) " @
		"		Gate.setportstate(gate, 74, Gate.getportstate(gate, 26)) " @
		"		Gate.setportstate(gate, 75, Gate.getportstate(gate, 27)) " @
		"		Gate.setportstate(gate, 76, Gate.getportstate(gate, 28)) " @
		"		Gate.setportstate(gate, 77, Gate.getportstate(gate, 29)) " @
		"		Gate.setportstate(gate, 78, Gate.getportstate(gate, 30)) " @
		"		Gate.setportstate(gate, 79, Gate.getportstate(gate, 31)) " @
		"		Gate.setportstate(gate, 80, Gate.getportstate(gate, 32)) " @
		"		Gate.setportstate(gate, 81, Gate.getportstate(gate, 33)) " @
		"		Gate.setportstate(gate, 82, Gate.getportstate(gate, 34)) " @
		"		Gate.setportstate(gate, 83, Gate.getportstate(gate, 35)) " @
		"		Gate.setportstate(gate, 84, Gate.getportstate(gate, 36)) " @
		"		Gate.setportstate(gate, 85, Gate.getportstate(gate, 37)) " @
		"		Gate.setportstate(gate, 86, Gate.getportstate(gate, 38)) " @
		"		Gate.setportstate(gate, 87, Gate.getportstate(gate, 39)) " @
		"		Gate.setportstate(gate, 88, Gate.getportstate(gate, 40)) " @
		"		Gate.setportstate(gate, 89, Gate.getportstate(gate, 41)) " @
		"		Gate.setportstate(gate, 90, Gate.getportstate(gate, 42)) " @
		"		Gate.setportstate(gate, 91, Gate.getportstate(gate, 43)) " @
		"		Gate.setportstate(gate, 92, Gate.getportstate(gate, 44)) " @
		"		Gate.setportstate(gate, 93, Gate.getportstate(gate, 45)) " @
		"		Gate.setportstate(gate, 94, Gate.getportstate(gate, 46)) " @
		"		Gate.setportstate(gate, 95, Gate.getportstate(gate, 47)) " @
		"		Gate.setportstate(gate, 96, Gate.getportstate(gate, 48)) " @
		"	else " @
		"		Gate.setportstate(gate, 49, 0) " @
		"		Gate.setportstate(gate, 50, 0) " @
		"		Gate.setportstate(gate, 51, 0) " @
		"		Gate.setportstate(gate, 52, 0) " @
		"		Gate.setportstate(gate, 53, 0) " @
		"		Gate.setportstate(gate, 54, 0) " @
		"		Gate.setportstate(gate, 55, 0) " @
		"		Gate.setportstate(gate, 56, 0) " @
		"		Gate.setportstate(gate, 57, 0) " @
		"		Gate.setportstate(gate, 58, 0) " @
		"		Gate.setportstate(gate, 59, 0) " @
		"		Gate.setportstate(gate, 60, 0) " @
		"		Gate.setportstate(gate, 61, 0) " @
		"		Gate.setportstate(gate, 62, 0) " @
		"		Gate.setportstate(gate, 63, 0) " @
		"		Gate.setportstate(gate, 64, 0) " @
		"		Gate.setportstate(gate, 65, 0) " @
		"		Gate.setportstate(gate, 66, 0) " @
		"		Gate.setportstate(gate, 67, 0) " @
		"		Gate.setportstate(gate, 68, 0) " @
		"		Gate.setportstate(gate, 69, 0) " @
		"		Gate.setportstate(gate, 70, 0) " @
		"		Gate.setportstate(gate, 71, 0) " @
		"		Gate.setportstate(gate, 72, 0) " @
		"		Gate.setportstate(gate, 73, 0) " @
		"		Gate.setportstate(gate, 74, 0) " @
		"		Gate.setportstate(gate, 75, 0) " @
		"		Gate.setportstate(gate, 76, 0) " @
		"		Gate.setportstate(gate, 77, 0) " @
		"		Gate.setportstate(gate, 78, 0) " @
		"		Gate.setportstate(gate, 79, 0) " @
		"		Gate.setportstate(gate, 80, 0) " @
		"		Gate.setportstate(gate, 81, 0) " @
		"		Gate.setportstate(gate, 82, 0) " @
		"		Gate.setportstate(gate, 83, 0) " @
		"		Gate.setportstate(gate, 84, 0) " @
		"		Gate.setportstate(gate, 85, 0) " @
		"		Gate.setportstate(gate, 86, 0) " @
		"		Gate.setportstate(gate, 87, 0) " @
		"		Gate.setportstate(gate, 88, 0) " @
		"		Gate.setportstate(gate, 89, 0) " @
		"		Gate.setportstate(gate, 90, 0) " @
		"		Gate.setportstate(gate, 91, 0) " @
		"		Gate.setportstate(gate, 92, 0) " @
		"		Gate.setportstate(gate, 93, 0) " @
		"		Gate.setportstate(gate, 94, 0) " @
		"		Gate.setportstate(gate, 95, 0) " @
		"		Gate.setportstate(gate, 96, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 97;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "47 0 0";
	logicPortDir[0] = 5;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "45 0 0";
	logicPortDir[1] = 5;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "43 0 0";
	logicPortDir[2] = 5;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "41 0 0";
	logicPortDir[3] = 5;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "39 0 0";
	logicPortDir[4] = 5;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "37 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "In5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "35 0 0";
	logicPortDir[6] = 5;
	logicPortUIName[6] = "In6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "33 0 0";
	logicPortDir[7] = 5;
	logicPortUIName[7] = "In7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "31 0 0";
	logicPortDir[8] = 5;
	logicPortUIName[8] = "In8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "29 0 0";
	logicPortDir[9] = 5;
	logicPortUIName[9] = "In9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "27 0 0";
	logicPortDir[10] = 5;
	logicPortUIName[10] = "In10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "25 0 0";
	logicPortDir[11] = 5;
	logicPortUIName[11] = "In11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "23 0 0";
	logicPortDir[12] = 5;
	logicPortUIName[12] = "In12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "21 0 0";
	logicPortDir[13] = 5;
	logicPortUIName[13] = "In13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "19 0 0";
	logicPortDir[14] = 5;
	logicPortUIName[14] = "In14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "17 0 0";
	logicPortDir[15] = 5;
	logicPortUIName[15] = "In15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 1;
	logicPortPos[16] = "15 0 0";
	logicPortDir[16] = 5;
	logicPortUIName[16] = "In16";
	logicPortCauseUpdate[16] = true;
	
	logicPortType[17] = 1;
	logicPortPos[17] = "13 0 0";
	logicPortDir[17] = 5;
	logicPortUIName[17] = "In17";
	logicPortCauseUpdate[17] = true;
	
	logicPortType[18] = 1;
	logicPortPos[18] = "11 0 0";
	logicPortDir[18] = 5;
	logicPortUIName[18] = "In18";
	logicPortCauseUpdate[18] = true;
	
	logicPortType[19] = 1;
	logicPortPos[19] = "9 0 0";
	logicPortDir[19] = 5;
	logicPortUIName[19] = "In19";
	logicPortCauseUpdate[19] = true;
	
	logicPortType[20] = 1;
	logicPortPos[20] = "7 0 0";
	logicPortDir[20] = 5;
	logicPortUIName[20] = "In20";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 1;
	logicPortPos[21] = "5 0 0";
	logicPortDir[21] = 5;
	logicPortUIName[21] = "In21";
	logicPortCauseUpdate[21] = true;
	
	logicPortType[22] = 1;
	logicPortPos[22] = "3 0 0";
	logicPortDir[22] = 5;
	logicPortUIName[22] = "In22";
	logicPortCauseUpdate[22] = true;
	
	logicPortType[23] = 1;
	logicPortPos[23] = "1 0 0";
	logicPortDir[23] = 5;
	logicPortUIName[23] = "In23";
	logicPortCauseUpdate[23] = true;
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-1 0 0";
	logicPortDir[24] = 5;
	logicPortUIName[24] = "In24";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-3 0 0";
	logicPortDir[25] = 5;
	logicPortUIName[25] = "In25";
	logicPortCauseUpdate[25] = true;
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-5 0 0";
	logicPortDir[26] = 5;
	logicPortUIName[26] = "In26";
	logicPortCauseUpdate[26] = true;
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-7 0 0";
	logicPortDir[27] = 5;
	logicPortUIName[27] = "In27";
	logicPortCauseUpdate[27] = true;
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-9 0 0";
	logicPortDir[28] = 5;
	logicPortUIName[28] = "In28";
	logicPortCauseUpdate[28] = true;
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-11 0 0";
	logicPortDir[29] = 5;
	logicPortUIName[29] = "In29";
	logicPortCauseUpdate[29] = true;
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-13 0 0";
	logicPortDir[30] = 5;
	logicPortUIName[30] = "In30";
	logicPortCauseUpdate[30] = true;
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-15 0 0";
	logicPortDir[31] = 5;
	logicPortUIName[31] = "In31";
	logicPortCauseUpdate[31] = true;
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-17 0 0";
	logicPortDir[32] = 5;
	logicPortUIName[32] = "In32";
	logicPortCauseUpdate[32] = true;
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-19 0 0";
	logicPortDir[33] = 5;
	logicPortUIName[33] = "In33";
	logicPortCauseUpdate[33] = true;
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-21 0 0";
	logicPortDir[34] = 5;
	logicPortUIName[34] = "In34";
	logicPortCauseUpdate[34] = true;
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-23 0 0";
	logicPortDir[35] = 5;
	logicPortUIName[35] = "In35";
	logicPortCauseUpdate[35] = true;
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-25 0 0";
	logicPortDir[36] = 5;
	logicPortUIName[36] = "In36";
	logicPortCauseUpdate[36] = true;
	
	logicPortType[37] = 1;
	logicPortPos[37] = "-27 0 0";
	logicPortDir[37] = 5;
	logicPortUIName[37] = "In37";
	logicPortCauseUpdate[37] = true;
	
	logicPortType[38] = 1;
	logicPortPos[38] = "-29 0 0";
	logicPortDir[38] = 5;
	logicPortUIName[38] = "In38";
	logicPortCauseUpdate[38] = true;
	
	logicPortType[39] = 1;
	logicPortPos[39] = "-31 0 0";
	logicPortDir[39] = 5;
	logicPortUIName[39] = "In39";
	logicPortCauseUpdate[39] = true;
	
	logicPortType[40] = 1;
	logicPortPos[40] = "-33 0 0";
	logicPortDir[40] = 5;
	logicPortUIName[40] = "In40";
	logicPortCauseUpdate[40] = true;
	
	logicPortType[41] = 1;
	logicPortPos[41] = "-35 0 0";
	logicPortDir[41] = 5;
	logicPortUIName[41] = "In41";
	logicPortCauseUpdate[41] = true;
	
	logicPortType[42] = 1;
	logicPortPos[42] = "-37 0 0";
	logicPortDir[42] = 5;
	logicPortUIName[42] = "In42";
	logicPortCauseUpdate[42] = true;
	
	logicPortType[43] = 1;
	logicPortPos[43] = "-39 0 0";
	logicPortDir[43] = 5;
	logicPortUIName[43] = "In43";
	logicPortCauseUpdate[43] = true;
	
	logicPortType[44] = 1;
	logicPortPos[44] = "-41 0 0";
	logicPortDir[44] = 5;
	logicPortUIName[44] = "In44";
	logicPortCauseUpdate[44] = true;
	
	logicPortType[45] = 1;
	logicPortPos[45] = "-43 0 0";
	logicPortDir[45] = 5;
	logicPortUIName[45] = "In45";
	logicPortCauseUpdate[45] = true;
	
	logicPortType[46] = 1;
	logicPortPos[46] = "-45 0 0";
	logicPortDir[46] = 5;
	logicPortUIName[46] = "In46";
	logicPortCauseUpdate[46] = true;
	
	logicPortType[47] = 1;
	logicPortPos[47] = "-47 0 0";
	logicPortDir[47] = 5;
	logicPortUIName[47] = "In47";
	logicPortCauseUpdate[47] = true;
	
	logicPortType[48] = 0;
	logicPortPos[48] = "47 0 0";
	logicPortDir[48] = 4;
	logicPortUIName[48] = "Out0";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "45 0 0";
	logicPortDir[49] = 4;
	logicPortUIName[49] = "Out1";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "43 0 0";
	logicPortDir[50] = 4;
	logicPortUIName[50] = "Out2";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "41 0 0";
	logicPortDir[51] = 4;
	logicPortUIName[51] = "Out3";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "39 0 0";
	logicPortDir[52] = 4;
	logicPortUIName[52] = "Out4";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "37 0 0";
	logicPortDir[53] = 4;
	logicPortUIName[53] = "Out5";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "35 0 0";
	logicPortDir[54] = 4;
	logicPortUIName[54] = "Out6";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "33 0 0";
	logicPortDir[55] = 4;
	logicPortUIName[55] = "Out7";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "31 0 0";
	logicPortDir[56] = 4;
	logicPortUIName[56] = "Out8";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "29 0 0";
	logicPortDir[57] = 4;
	logicPortUIName[57] = "Out9";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "27 0 0";
	logicPortDir[58] = 4;
	logicPortUIName[58] = "Out10";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "25 0 0";
	logicPortDir[59] = 4;
	logicPortUIName[59] = "Out11";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "23 0 0";
	logicPortDir[60] = 4;
	logicPortUIName[60] = "Out12";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "21 0 0";
	logicPortDir[61] = 4;
	logicPortUIName[61] = "Out13";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "19 0 0";
	logicPortDir[62] = 4;
	logicPortUIName[62] = "Out14";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "17 0 0";
	logicPortDir[63] = 4;
	logicPortUIName[63] = "Out15";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "15 0 0";
	logicPortDir[64] = 4;
	logicPortUIName[64] = "Out16";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "13 0 0";
	logicPortDir[65] = 4;
	logicPortUIName[65] = "Out17";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "11 0 0";
	logicPortDir[66] = 4;
	logicPortUIName[66] = "Out18";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "9 0 0";
	logicPortDir[67] = 4;
	logicPortUIName[67] = "Out19";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "7 0 0";
	logicPortDir[68] = 4;
	logicPortUIName[68] = "Out20";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "5 0 0";
	logicPortDir[69] = 4;
	logicPortUIName[69] = "Out21";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "3 0 0";
	logicPortDir[70] = 4;
	logicPortUIName[70] = "Out22";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "1 0 0";
	logicPortDir[71] = 4;
	logicPortUIName[71] = "Out23";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "-1 0 0";
	logicPortDir[72] = 4;
	logicPortUIName[72] = "Out24";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "-3 0 0";
	logicPortDir[73] = 4;
	logicPortUIName[73] = "Out25";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "-5 0 0";
	logicPortDir[74] = 4;
	logicPortUIName[74] = "Out26";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "-7 0 0";
	logicPortDir[75] = 4;
	logicPortUIName[75] = "Out27";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "-9 0 0";
	logicPortDir[76] = 4;
	logicPortUIName[76] = "Out28";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "-11 0 0";
	logicPortDir[77] = 4;
	logicPortUIName[77] = "Out29";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "-13 0 0";
	logicPortDir[78] = 4;
	logicPortUIName[78] = "Out30";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "-15 0 0";
	logicPortDir[79] = 4;
	logicPortUIName[79] = "Out31";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "-17 0 0";
	logicPortDir[80] = 4;
	logicPortUIName[80] = "Out32";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "-19 0 0";
	logicPortDir[81] = 4;
	logicPortUIName[81] = "Out33";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "-21 0 0";
	logicPortDir[82] = 4;
	logicPortUIName[82] = "Out34";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "-23 0 0";
	logicPortDir[83] = 4;
	logicPortUIName[83] = "Out35";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "-25 0 0";
	logicPortDir[84] = 4;
	logicPortUIName[84] = "Out36";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "-27 0 0";
	logicPortDir[85] = 4;
	logicPortUIName[85] = "Out37";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "-29 0 0";
	logicPortDir[86] = 4;
	logicPortUIName[86] = "Out38";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "-31 0 0";
	logicPortDir[87] = 4;
	logicPortUIName[87] = "Out39";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "-33 0 0";
	logicPortDir[88] = 4;
	logicPortUIName[88] = "Out40";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "-35 0 0";
	logicPortDir[89] = 4;
	logicPortUIName[89] = "Out41";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "-37 0 0";
	logicPortDir[90] = 4;
	logicPortUIName[90] = "Out42";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "-39 0 0";
	logicPortDir[91] = 4;
	logicPortUIName[91] = "Out43";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "-41 0 0";
	logicPortDir[92] = 4;
	logicPortUIName[92] = "Out44";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "-43 0 0";
	logicPortDir[93] = 4;
	logicPortUIName[93] = "Out45";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "-45 0 0";
	logicPortDir[94] = 4;
	logicPortUIName[94] = "Out46";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "-47 0 0";
	logicPortDir[95] = 4;
	logicPortUIName[95] = "Out47";
	
	logicPortType[96] = 1;
	logicPortPos[96] = "47 0 0";
	logicPortDir[96] = 2;
	logicPortUIName[96] = "Clock";
	logicPortCauseUpdate[96] = true;
	
};
