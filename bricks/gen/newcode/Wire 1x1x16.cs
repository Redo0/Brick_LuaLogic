
datablock fxDtsBrickData(LogicWire_1x1x16_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x16.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x16";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x16";
	
	logicBrickSize = "1 1 48";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
