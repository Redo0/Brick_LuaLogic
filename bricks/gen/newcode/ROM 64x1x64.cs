
datablock fxDtsBrickData(LogicGate_Rom64x1x64_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x1x64.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x1x64";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x1x64";
	logicUIName = "ROM 64x1x64";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 4095 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 71;
	
	isLogicRom = true;
	logicRomY = 1;
	logicRomZ = 64;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "63 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "O0";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "61 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "O1";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "59 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O2";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "57 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O3";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "55 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O4";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "53 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O5";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "51 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O6";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "49 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O7";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "47 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O8";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "45 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O9";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "43 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O10";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "41 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O11";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "39 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O12";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "37 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O13";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "35 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O14";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "33 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O15";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "31 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O16";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "29 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O17";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "27 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O18";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "25 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O19";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "23 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O20";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "21 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O21";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "19 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O22";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "17 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O23";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "15 0 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O24";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "13 0 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O25";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "11 0 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O26";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "9 0 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O27";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "7 0 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O28";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "5 0 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O29";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "3 0 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O30";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "1 0 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O31";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-1 0 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O32";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-3 0 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O33";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "-5 0 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O34";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "-7 0 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O35";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "-9 0 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "O36";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "-11 0 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "O37";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-13 0 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "O38";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-15 0 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "O39";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-17 0 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "O40";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-19 0 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "O41";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-21 0 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "O42";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-23 0 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "O43";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-25 0 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "O44";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-27 0 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "O45";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-29 0 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "O46";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-31 0 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "O47";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-33 0 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "O48";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-35 0 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "O49";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-37 0 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "O50";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-39 0 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "O51";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-41 0 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "O52";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-43 0 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "O53";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-45 0 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "O54";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-47 0 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "O55";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-49 0 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "O56";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-51 0 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "O57";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "-53 0 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "O58";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "-55 0 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "O59";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "-57 0 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "O60";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "-59 0 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "O61";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "-61 0 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "O62";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "-63 0 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "O63";
	
	logicPortType[70] = 1;
	logicPortPos[70] = "63 -0 0";
	logicPortDir[70] = 2;
	logicPortUIName[70] = "Clock";
	logicPortCauseUpdate[70] = true;
	
};
