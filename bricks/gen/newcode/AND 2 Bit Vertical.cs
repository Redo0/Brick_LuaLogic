
datablock fxDtsBrickData(LogicGate_GateAnd2Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/AND 2 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/AND 2 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "AND 2 Bit Vertical";
	logicUIName = "AND 2 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 2";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 3, (( " @
		"		(Gate.getportstate(gate, 1)~=0) and " @
		"		(Gate.getportstate(gate, 2)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 3;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 1";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -1";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 0;
	logicPortPos[2] = "0 0 -1";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out";
	
};
