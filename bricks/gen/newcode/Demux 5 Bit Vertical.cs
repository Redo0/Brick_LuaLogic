
datablock fxDtsBrickData(LogicGate_Demux5Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 5 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 5 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 5 Bit Vertical";
	logicUIName = "Demux 5 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 32";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 6 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 38)~=0 then " @
		"		local idx = 6 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 38;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -31";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -29";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -27";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -25";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -23";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "0 0 -31";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out0";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "0 0 -29";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out1";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "0 0 -27";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out2";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 -25";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out3";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "0 0 -23";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out4";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 -21";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out5";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "0 0 -19";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out6";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "0 0 -17";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out7";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "0 0 -15";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out8";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "0 0 -13";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out9";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "0 0 -11";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out10";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 0 -9";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out11";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "0 0 -7";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out12";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "0 0 -5";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out13";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "0 0 -3";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out14";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "0 0 -1";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out15";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "0 0 1";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out16";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "0 0 3";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out17";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "0 0 5";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out18";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "0 0 7";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out19";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "0 0 9";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out20";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "0 0 11";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out21";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "0 0 13";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out22";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "0 0 15";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out23";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "0 0 17";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out24";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "0 0 19";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out25";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "0 0 21";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out26";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "0 0 23";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out27";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "0 0 25";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out28";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "0 0 27";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out29";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "0 0 29";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out30";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "0 0 31";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out31";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "0 0 -31";
	logicPortDir[37] = 5;
	logicPortUIName[37] = "Enable";
	logicPortCauseUpdate[37] = true;
	
};
