
datablock fxDtsBrickData(LogicGate_Demux7Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 7 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 7 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 7 Bit Vertical";
	logicUIName = "Demux 7 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 128";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 8 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 136)~=0 then " @
		"		local idx = 8 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) + " @
		"			(Gate.getportstate(gate, 7) * 64) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 136;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -127";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -125";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -123";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -121";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 -119";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 0 -117";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 0 -115";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "Sel6";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "0 0 -127";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out0";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 -125";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out1";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "0 0 -123";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out2";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 -121";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out3";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "0 0 -119";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out4";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "0 0 -117";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out5";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "0 0 -115";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out6";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "0 0 -113";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out7";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "0 0 -111";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out8";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 0 -109";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out9";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "0 0 -107";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out10";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "0 0 -105";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out11";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "0 0 -103";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out12";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "0 0 -101";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out13";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "0 0 -99";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out14";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "0 0 -97";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out15";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "0 0 -95";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out16";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "0 0 -93";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out17";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "0 0 -91";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out18";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "0 0 -89";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out19";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "0 0 -87";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out20";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "0 0 -85";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out21";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "0 0 -83";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out22";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "0 0 -81";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out23";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "0 0 -79";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out24";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "0 0 -77";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out25";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "0 0 -75";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out26";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "0 0 -73";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out27";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "0 0 -71";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out28";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "0 0 -69";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out29";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "0 0 -67";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "Out30";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "0 0 -65";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "Out31";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "0 0 -63";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "Out32";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "0 0 -61";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "Out33";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "0 0 -59";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "Out34";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "0 0 -57";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "Out35";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "0 0 -55";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "Out36";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "0 0 -53";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "Out37";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "0 0 -51";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "Out38";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "0 0 -49";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "Out39";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "0 0 -47";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "Out40";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "0 0 -45";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "Out41";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "0 0 -43";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "Out42";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "0 0 -41";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "Out43";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "0 0 -39";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "Out44";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "0 0 -37";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "Out45";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "0 0 -35";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "Out46";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "0 0 -33";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "Out47";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "0 0 -31";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "Out48";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "0 0 -29";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "Out49";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "0 0 -27";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "Out50";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "0 0 -25";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "Out51";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "0 0 -23";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "Out52";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "0 0 -21";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "Out53";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "0 0 -19";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "Out54";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "0 0 -17";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "Out55";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "0 0 -15";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "Out56";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "0 0 -13";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "Out57";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "0 0 -11";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "Out58";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "0 0 -9";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "Out59";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "0 0 -7";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "Out60";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "0 0 -5";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "Out61";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "0 0 -3";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "Out62";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "0 0 -1";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "Out63";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "0 0 1";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "Out64";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "0 0 3";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "Out65";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "0 0 5";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "Out66";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "0 0 7";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "Out67";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "0 0 9";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "Out68";
	
	logicPortType[76] = 0;
	logicPortPos[76] = "0 0 11";
	logicPortDir[76] = 1;
	logicPortUIName[76] = "Out69";
	
	logicPortType[77] = 0;
	logicPortPos[77] = "0 0 13";
	logicPortDir[77] = 1;
	logicPortUIName[77] = "Out70";
	
	logicPortType[78] = 0;
	logicPortPos[78] = "0 0 15";
	logicPortDir[78] = 1;
	logicPortUIName[78] = "Out71";
	
	logicPortType[79] = 0;
	logicPortPos[79] = "0 0 17";
	logicPortDir[79] = 1;
	logicPortUIName[79] = "Out72";
	
	logicPortType[80] = 0;
	logicPortPos[80] = "0 0 19";
	logicPortDir[80] = 1;
	logicPortUIName[80] = "Out73";
	
	logicPortType[81] = 0;
	logicPortPos[81] = "0 0 21";
	logicPortDir[81] = 1;
	logicPortUIName[81] = "Out74";
	
	logicPortType[82] = 0;
	logicPortPos[82] = "0 0 23";
	logicPortDir[82] = 1;
	logicPortUIName[82] = "Out75";
	
	logicPortType[83] = 0;
	logicPortPos[83] = "0 0 25";
	logicPortDir[83] = 1;
	logicPortUIName[83] = "Out76";
	
	logicPortType[84] = 0;
	logicPortPos[84] = "0 0 27";
	logicPortDir[84] = 1;
	logicPortUIName[84] = "Out77";
	
	logicPortType[85] = 0;
	logicPortPos[85] = "0 0 29";
	logicPortDir[85] = 1;
	logicPortUIName[85] = "Out78";
	
	logicPortType[86] = 0;
	logicPortPos[86] = "0 0 31";
	logicPortDir[86] = 1;
	logicPortUIName[86] = "Out79";
	
	logicPortType[87] = 0;
	logicPortPos[87] = "0 0 33";
	logicPortDir[87] = 1;
	logicPortUIName[87] = "Out80";
	
	logicPortType[88] = 0;
	logicPortPos[88] = "0 0 35";
	logicPortDir[88] = 1;
	logicPortUIName[88] = "Out81";
	
	logicPortType[89] = 0;
	logicPortPos[89] = "0 0 37";
	logicPortDir[89] = 1;
	logicPortUIName[89] = "Out82";
	
	logicPortType[90] = 0;
	logicPortPos[90] = "0 0 39";
	logicPortDir[90] = 1;
	logicPortUIName[90] = "Out83";
	
	logicPortType[91] = 0;
	logicPortPos[91] = "0 0 41";
	logicPortDir[91] = 1;
	logicPortUIName[91] = "Out84";
	
	logicPortType[92] = 0;
	logicPortPos[92] = "0 0 43";
	logicPortDir[92] = 1;
	logicPortUIName[92] = "Out85";
	
	logicPortType[93] = 0;
	logicPortPos[93] = "0 0 45";
	logicPortDir[93] = 1;
	logicPortUIName[93] = "Out86";
	
	logicPortType[94] = 0;
	logicPortPos[94] = "0 0 47";
	logicPortDir[94] = 1;
	logicPortUIName[94] = "Out87";
	
	logicPortType[95] = 0;
	logicPortPos[95] = "0 0 49";
	logicPortDir[95] = 1;
	logicPortUIName[95] = "Out88";
	
	logicPortType[96] = 0;
	logicPortPos[96] = "0 0 51";
	logicPortDir[96] = 1;
	logicPortUIName[96] = "Out89";
	
	logicPortType[97] = 0;
	logicPortPos[97] = "0 0 53";
	logicPortDir[97] = 1;
	logicPortUIName[97] = "Out90";
	
	logicPortType[98] = 0;
	logicPortPos[98] = "0 0 55";
	logicPortDir[98] = 1;
	logicPortUIName[98] = "Out91";
	
	logicPortType[99] = 0;
	logicPortPos[99] = "0 0 57";
	logicPortDir[99] = 1;
	logicPortUIName[99] = "Out92";
	
	logicPortType[100] = 0;
	logicPortPos[100] = "0 0 59";
	logicPortDir[100] = 1;
	logicPortUIName[100] = "Out93";
	
	logicPortType[101] = 0;
	logicPortPos[101] = "0 0 61";
	logicPortDir[101] = 1;
	logicPortUIName[101] = "Out94";
	
	logicPortType[102] = 0;
	logicPortPos[102] = "0 0 63";
	logicPortDir[102] = 1;
	logicPortUIName[102] = "Out95";
	
	logicPortType[103] = 0;
	logicPortPos[103] = "0 0 65";
	logicPortDir[103] = 1;
	logicPortUIName[103] = "Out96";
	
	logicPortType[104] = 0;
	logicPortPos[104] = "0 0 67";
	logicPortDir[104] = 1;
	logicPortUIName[104] = "Out97";
	
	logicPortType[105] = 0;
	logicPortPos[105] = "0 0 69";
	logicPortDir[105] = 1;
	logicPortUIName[105] = "Out98";
	
	logicPortType[106] = 0;
	logicPortPos[106] = "0 0 71";
	logicPortDir[106] = 1;
	logicPortUIName[106] = "Out99";
	
	logicPortType[107] = 0;
	logicPortPos[107] = "0 0 73";
	logicPortDir[107] = 1;
	logicPortUIName[107] = "Out100";
	
	logicPortType[108] = 0;
	logicPortPos[108] = "0 0 75";
	logicPortDir[108] = 1;
	logicPortUIName[108] = "Out101";
	
	logicPortType[109] = 0;
	logicPortPos[109] = "0 0 77";
	logicPortDir[109] = 1;
	logicPortUIName[109] = "Out102";
	
	logicPortType[110] = 0;
	logicPortPos[110] = "0 0 79";
	logicPortDir[110] = 1;
	logicPortUIName[110] = "Out103";
	
	logicPortType[111] = 0;
	logicPortPos[111] = "0 0 81";
	logicPortDir[111] = 1;
	logicPortUIName[111] = "Out104";
	
	logicPortType[112] = 0;
	logicPortPos[112] = "0 0 83";
	logicPortDir[112] = 1;
	logicPortUIName[112] = "Out105";
	
	logicPortType[113] = 0;
	logicPortPos[113] = "0 0 85";
	logicPortDir[113] = 1;
	logicPortUIName[113] = "Out106";
	
	logicPortType[114] = 0;
	logicPortPos[114] = "0 0 87";
	logicPortDir[114] = 1;
	logicPortUIName[114] = "Out107";
	
	logicPortType[115] = 0;
	logicPortPos[115] = "0 0 89";
	logicPortDir[115] = 1;
	logicPortUIName[115] = "Out108";
	
	logicPortType[116] = 0;
	logicPortPos[116] = "0 0 91";
	logicPortDir[116] = 1;
	logicPortUIName[116] = "Out109";
	
	logicPortType[117] = 0;
	logicPortPos[117] = "0 0 93";
	logicPortDir[117] = 1;
	logicPortUIName[117] = "Out110";
	
	logicPortType[118] = 0;
	logicPortPos[118] = "0 0 95";
	logicPortDir[118] = 1;
	logicPortUIName[118] = "Out111";
	
	logicPortType[119] = 0;
	logicPortPos[119] = "0 0 97";
	logicPortDir[119] = 1;
	logicPortUIName[119] = "Out112";
	
	logicPortType[120] = 0;
	logicPortPos[120] = "0 0 99";
	logicPortDir[120] = 1;
	logicPortUIName[120] = "Out113";
	
	logicPortType[121] = 0;
	logicPortPos[121] = "0 0 101";
	logicPortDir[121] = 1;
	logicPortUIName[121] = "Out114";
	
	logicPortType[122] = 0;
	logicPortPos[122] = "0 0 103";
	logicPortDir[122] = 1;
	logicPortUIName[122] = "Out115";
	
	logicPortType[123] = 0;
	logicPortPos[123] = "0 0 105";
	logicPortDir[123] = 1;
	logicPortUIName[123] = "Out116";
	
	logicPortType[124] = 0;
	logicPortPos[124] = "0 0 107";
	logicPortDir[124] = 1;
	logicPortUIName[124] = "Out117";
	
	logicPortType[125] = 0;
	logicPortPos[125] = "0 0 109";
	logicPortDir[125] = 1;
	logicPortUIName[125] = "Out118";
	
	logicPortType[126] = 0;
	logicPortPos[126] = "0 0 111";
	logicPortDir[126] = 1;
	logicPortUIName[126] = "Out119";
	
	logicPortType[127] = 0;
	logicPortPos[127] = "0 0 113";
	logicPortDir[127] = 1;
	logicPortUIName[127] = "Out120";
	
	logicPortType[128] = 0;
	logicPortPos[128] = "0 0 115";
	logicPortDir[128] = 1;
	logicPortUIName[128] = "Out121";
	
	logicPortType[129] = 0;
	logicPortPos[129] = "0 0 117";
	logicPortDir[129] = 1;
	logicPortUIName[129] = "Out122";
	
	logicPortType[130] = 0;
	logicPortPos[130] = "0 0 119";
	logicPortDir[130] = 1;
	logicPortUIName[130] = "Out123";
	
	logicPortType[131] = 0;
	logicPortPos[131] = "0 0 121";
	logicPortDir[131] = 1;
	logicPortUIName[131] = "Out124";
	
	logicPortType[132] = 0;
	logicPortPos[132] = "0 0 123";
	logicPortDir[132] = 1;
	logicPortUIName[132] = "Out125";
	
	logicPortType[133] = 0;
	logicPortPos[133] = "0 0 125";
	logicPortDir[133] = 1;
	logicPortUIName[133] = "Out126";
	
	logicPortType[134] = 0;
	logicPortPos[134] = "0 0 127";
	logicPortDir[134] = 1;
	logicPortUIName[134] = "Out127";
	
	logicPortType[135] = 1;
	logicPortPos[135] = "0 0 -127";
	logicPortDir[135] = 5;
	logicPortUIName[135] = "Enable";
	logicPortCauseUpdate[135] = true;
	
};
