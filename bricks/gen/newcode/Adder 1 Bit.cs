
datablock fxDtsBrickData(LogicGate_Adder1Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 1 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 1 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 1 Bit";
	logicUIName = "Adder 1 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 2) + Gate.getportstate(gate, 4)) " @
		") " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 5, 1); else Gate.setportstate(gate, 5, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 3, 1); else Gate.setportstate(gate, 3, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 5;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "-1 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "B0";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 0;
	logicPortPos[2] = "1 1 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "O0";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 -1 0";
	logicPortDir[3] = 2;
	logicPortUIName[3] = "CIn";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 0;
	logicPortPos[4] = "-1 -1 0";
	logicPortDir[4] = 0;
	logicPortUIName[4] = "COut";
	logicPortCauseUpdate[4] = true;
	
};
