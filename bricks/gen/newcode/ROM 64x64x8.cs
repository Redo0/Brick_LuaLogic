
datablock fxDtsBrickData(LogicGate_Rom64x64x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x64x8.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x64x8";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x64x8";
	logicUIName = "ROM 64x64x8";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 64 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 32767 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 21;
	
	isLogicRom = true;
	logicRomY = 64;
	logicRomZ = 8;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -63 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -63 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -63 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -63 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -63 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -63 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -63 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -63 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -63 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 -63 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 -63 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "41 -63 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "A11";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "63 63 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O0";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "61 63 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O1";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "59 63 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O2";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "57 63 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O3";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "55 63 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O4";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "53 63 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O5";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "51 63 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O6";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "49 63 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O7";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "63 -63 0";
	logicPortDir[20] = 2;
	logicPortUIName[20] = "Clock";
	logicPortCauseUpdate[20] = true;
	
};
