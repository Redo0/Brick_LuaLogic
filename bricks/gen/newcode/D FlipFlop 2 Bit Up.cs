
datablock fxDtsBrickData(LogicGate_DFlipFlop2BitUp_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 2 Bit Up.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 2 Bit Up";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 2 Bit Up";
	logicUIName = "D FlipFlop 2 Bit Up";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 5)~=0 then " @
		"		Gate.setportstate(gate, 3, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 4, Gate.getportstate(gate, 2)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 5;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 5;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "-1 0 0";
	logicPortDir[1] = 5;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 0;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "Out0";
	
	logicPortType[3] = 0;
	logicPortPos[3] = "-1 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "Out1";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "1 0 0";
	logicPortDir[4] = 2;
	logicPortUIName[4] = "Clock";
	logicPortCauseUpdate[4] = true;
	
};
