
datablock fxDtsBrickData(LogicGate_Rom32x16x32_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 32x16x32.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 32x16x32";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 32x16x32";
	logicUIName = "ROM 32x16x32";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 16 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 16383 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 42;
	
	isLogicRom = true;
	logicRomY = 16;
	logicRomZ = 32;
	logicRomX = 32;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 -15 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 -15 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 -15 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 -15 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 -15 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 -15 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 -15 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 -15 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "15 -15 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "31 15 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O0";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "29 15 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O1";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "27 15 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O2";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "25 15 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O3";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "23 15 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O4";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "21 15 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O5";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "19 15 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O6";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "17 15 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O7";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "15 15 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O8";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "13 15 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O9";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "11 15 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O10";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "9 15 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O11";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "7 15 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O12";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "5 15 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O13";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "3 15 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O14";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "1 15 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O15";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-1 15 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O16";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-3 15 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O17";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-5 15 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O18";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-7 15 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O19";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-9 15 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O20";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "-11 15 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O21";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "-13 15 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O22";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "-15 15 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O23";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "-17 15 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O24";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "-19 15 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O25";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "-21 15 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O26";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "-23 15 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O27";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "-25 15 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O28";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-27 15 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O29";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-29 15 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O30";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "-31 15 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O31";
	
	logicPortType[41] = 1;
	logicPortPos[41] = "31 -15 0";
	logicPortDir[41] = 2;
	logicPortUIName[41] = "Clock";
	logicPortCauseUpdate[41] = true;
	
};
