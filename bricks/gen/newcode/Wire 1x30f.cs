
datablock fxDtsBrickData(LogicWire_1x30f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x30f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x30f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x30f";
	
	logicBrickSize = "1 30 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
