
datablock fxDtsBrickData(LogicWire_2x2f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 2x2f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 2x2f";
	
	category = "Logic Bricks";
	subCategory = "Wires Other";
	uiName = "Wire 2x2f";
	
	logicBrickSize = "2 2 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
