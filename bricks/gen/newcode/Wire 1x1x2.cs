
datablock fxDtsBrickData(LogicWire_1x1x2_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x2.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x2";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x2";
	
	logicBrickSize = "1 1 6";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
