
datablock fxDtsBrickData(LogicGate_Demux6_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 6 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 6 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 6 Bit";
	logicUIName = "Demux 6 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 7 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 71)~=0 then " @
		"		local idx = 7 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) + " @
		"			(Gate.getportstate(gate, 6) * 32) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 71;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "Sel5";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "63 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out0";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "61 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out1";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "59 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out2";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "57 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out3";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "55 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out4";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "53 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out5";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "51 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out6";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "49 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out7";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "47 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out8";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "45 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out9";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "43 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out10";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "41 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out11";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "39 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out12";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "37 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out13";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "35 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out14";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "33 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out15";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "31 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out16";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "29 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out17";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "27 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out18";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "25 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out19";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "23 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out20";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "21 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out21";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "19 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out22";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "17 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out23";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "15 0 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "Out24";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "13 0 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "Out25";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "11 0 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "Out26";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "9 0 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "Out27";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "7 0 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "Out28";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "5 0 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "Out29";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "3 0 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "Out30";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "1 0 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "Out31";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-1 0 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "Out32";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "-3 0 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "Out33";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "-5 0 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "Out34";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "-7 0 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "Out35";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "-9 0 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "Out36";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "-11 0 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "Out37";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-13 0 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "Out38";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-15 0 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "Out39";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-17 0 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "Out40";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-19 0 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "Out41";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-21 0 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "Out42";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-23 0 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "Out43";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-25 0 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "Out44";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-27 0 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "Out45";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-29 0 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "Out46";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-31 0 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "Out47";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-33 0 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "Out48";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-35 0 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "Out49";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-37 0 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "Out50";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-39 0 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "Out51";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-41 0 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "Out52";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-43 0 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "Out53";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-45 0 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "Out54";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-47 0 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "Out55";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-49 0 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "Out56";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-51 0 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "Out57";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "-53 0 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "Out58";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "-55 0 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "Out59";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "-57 0 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "Out60";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "-59 0 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "Out61";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "-61 0 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "Out62";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "-63 0 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "Out63";
	
	logicPortType[70] = 1;
	logicPortPos[70] = "63 0 0";
	logicPortDir[70] = 2;
	logicPortUIName[70] = "Enable";
	logicPortCauseUpdate[70] = true;
	
};
