
datablock fxDtsBrickData(LogicGate_Rom4x4x4_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 4x4x4.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 4x4x4";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 4x4x4";
	logicUIName = "ROM 4x4x4";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 4 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 63 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 9;
	
	isLogicRom = true;
	logicRomY = 4;
	logicRomZ = 4;
	logicRomX = 4;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 -3 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 -3 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-1 -3 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-3 -3 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "3 3 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "O0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "1 3 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "O1";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "-1 3 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "O2";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "-3 3 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "O3";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "3 -3 0";
	logicPortDir[8] = 2;
	logicPortUIName[8] = "Clock";
	logicPortCauseUpdate[8] = true;
	
};
