
datablock fxDtsBrickData(LogicGate_Mux2_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 2 Bit";
	logicUIName = "Mux 2 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 7) then " @
		"		local idx = 3 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 8, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 8;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "In0";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "In1";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-1 0 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "In2";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-3 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "In3";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 0 0";
	logicPortDir[6] = 2;
	logicPortUIName[6] = "Enable";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 0;
	logicPortPos[7] = "-3 0 0";
	logicPortDir[7] = 0;
	logicPortUIName[7] = "Out";
	
};
