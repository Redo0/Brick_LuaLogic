
datablock fxDtsBrickData(LogicGate_Buffer5BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 5 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 5 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 5 Bit Down";
	logicUIName = "Buffer 5 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "5 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 11)~=0 then " @
		"		Gate.setportstate(gate, 6, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 7, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 9, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 10, Gate.getportstate(gate, 5)) " @
		"	else " @
		"		Gate.setportstate(gate, 6, 0) " @
		"		Gate.setportstate(gate, 7, 0) " @
		"		Gate.setportstate(gate, 8, 0) " @
		"		Gate.setportstate(gate, 9, 0) " @
		"		Gate.setportstate(gate, 10, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 11;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "4 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "2 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-2 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-4 0 0";
	logicPortDir[4] = 4;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "4 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "Out0";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "2 0 0";
	logicPortDir[6] = 5;
	logicPortUIName[6] = "Out1";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "0 0 0";
	logicPortDir[7] = 5;
	logicPortUIName[7] = "Out2";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "-2 0 0";
	logicPortDir[8] = 5;
	logicPortUIName[8] = "Out3";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "-4 0 0";
	logicPortDir[9] = 5;
	logicPortUIName[9] = "Out4";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "4 0 0";
	logicPortDir[10] = 2;
	logicPortUIName[10] = "Clock";
	logicPortCauseUpdate[10] = true;
	
};
