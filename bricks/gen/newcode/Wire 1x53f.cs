
datablock fxDtsBrickData(LogicWire_1x53f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x53f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x53f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x53f";
	
	logicBrickSize = "1 53 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
