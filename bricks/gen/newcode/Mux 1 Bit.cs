
datablock fxDtsBrickData(LogicGate_Mux1_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 1 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 1 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 1 Bit";
	logicUIName = "Mux 1 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "2 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 4) then " @
		"		local idx = 2 + " @
		"			(Gate.getportstate(gate, 1) * 1) " @
		"		Gate.setportstate(gate, 5, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 5, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 5;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "1 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 1;
	logicPortUIName[1] = "In0";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-1 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "In1";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 0 0";
	logicPortDir[3] = 2;
	logicPortUIName[3] = "Enable";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 0;
	logicPortPos[4] = "-1 0 0";
	logicPortDir[4] = 0;
	logicPortUIName[4] = "Out";
	
};
