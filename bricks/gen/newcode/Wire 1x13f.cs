
datablock fxDtsBrickData(LogicWire_1x13f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x13f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x13f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x13f";
	
	logicBrickSize = "1 13 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
