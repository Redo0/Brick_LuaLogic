
datablock fxDtsBrickData(LogicWire_1x57f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x57f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x57f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x57f";
	
	logicBrickSize = "1 57 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
