
datablock fxDtsBrickData(LogicGate_Demux3Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 3 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 3 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 3 Bit Vertical";
	logicUIName = "Demux 3 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 8";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 4 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 12)~=0 then " @
		"		local idx = 4 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 12;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -7";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -5";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -3";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 0;
	logicPortPos[3] = "0 0 -7";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Out0";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "0 0 -5";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "Out1";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "0 0 -3";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out2";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "0 0 -1";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out3";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "0 0 1";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out4";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 3";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out5";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "0 0 5";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out6";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 7";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out7";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "0 0 -7";
	logicPortDir[11] = 5;
	logicPortUIName[11] = "Enable";
	logicPortCauseUpdate[11] = true;
	
};
