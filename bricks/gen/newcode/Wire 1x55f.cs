
datablock fxDtsBrickData(LogicWire_1x55f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x55f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x55f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x55f";
	
	logicBrickSize = "1 55 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
