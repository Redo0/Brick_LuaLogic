
datablock fxDtsBrickData(LogicWire_1x44f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x44f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x44f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x44f";
	
	logicBrickSize = "1 44 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
