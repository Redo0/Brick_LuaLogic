
datablock fxDtsBrickData(LogicWire_1x16f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x16f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x16f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x16f";
	
	logicBrickSize = "1 16 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
