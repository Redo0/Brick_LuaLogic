
datablock fxDtsBrickData(LogicWire_1x8f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x8f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x8f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x8f";
	
	logicBrickSize = "1 8 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
