
datablock fxDtsBrickData(LogicGate_Adder2Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 2 Bit";
	logicUIName = "Adder 2 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 3) + Gate.getportstate(gate, 7)) " @
		" + ((Gate.getportstate(gate, 2) + Gate.getportstate(gate, 4)) * 2) " @
		") " @
		"if val >= 4 then val = val-4; Gate.setportstate(gate, 8, 1); else Gate.setportstate(gate, 8, 0) end " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 6, 1); else Gate.setportstate(gate, 6, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 5, 1); else Gate.setportstate(gate, 5, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 8;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-1 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "B0";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-3 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "B1";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 0;
	logicPortPos[4] = "3 1 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "O0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "1 1 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "O1";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 -1 0";
	logicPortDir[6] = 2;
	logicPortUIName[6] = "CIn";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 0;
	logicPortPos[7] = "-3 -1 0";
	logicPortDir[7] = 0;
	logicPortUIName[7] = "COut";
	logicPortCauseUpdate[7] = true;
	
};
