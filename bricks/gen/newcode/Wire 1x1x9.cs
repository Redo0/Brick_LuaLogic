
datablock fxDtsBrickData(LogicWire_1x1x9_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x9.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x9";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x9";
	
	logicBrickSize = "1 1 27";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
