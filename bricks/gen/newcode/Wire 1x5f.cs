
datablock fxDtsBrickData(LogicWire_1x5f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x5f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x5f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x5f";
	
	logicBrickSize = "1 5 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
