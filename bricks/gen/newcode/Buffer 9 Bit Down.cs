
datablock fxDtsBrickData(LogicGate_Buffer9BitDown_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 9 Bit Down.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 9 Bit Down";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 9 Bit Down";
	logicUIName = "Buffer 9 Bit Down";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "9 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 19)~=0 then " @
		"		Gate.setportstate(gate, 10, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 11, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 12, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 14, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 15, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 16, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 17, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 18, Gate.getportstate(gate, 9)) " @
		"	else " @
		"		Gate.setportstate(gate, 10, 0) " @
		"		Gate.setportstate(gate, 11, 0) " @
		"		Gate.setportstate(gate, 12, 0) " @
		"		Gate.setportstate(gate, 13, 0) " @
		"		Gate.setportstate(gate, 14, 0) " @
		"		Gate.setportstate(gate, 15, 0) " @
		"		Gate.setportstate(gate, 16, 0) " @
		"		Gate.setportstate(gate, 17, 0) " @
		"		Gate.setportstate(gate, 18, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 19;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "8 0 0";
	logicPortDir[0] = 4;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "6 0 0";
	logicPortDir[1] = 4;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "4 0 0";
	logicPortDir[2] = 4;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "2 0 0";
	logicPortDir[3] = 4;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 0 0";
	logicPortDir[4] = 4;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-2 0 0";
	logicPortDir[5] = 4;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-4 0 0";
	logicPortDir[6] = 4;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-6 0 0";
	logicPortDir[7] = 4;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-8 0 0";
	logicPortDir[8] = 4;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "8 0 0";
	logicPortDir[9] = 5;
	logicPortUIName[9] = "Out0";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "6 0 0";
	logicPortDir[10] = 5;
	logicPortUIName[10] = "Out1";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "4 0 0";
	logicPortDir[11] = 5;
	logicPortUIName[11] = "Out2";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "2 0 0";
	logicPortDir[12] = 5;
	logicPortUIName[12] = "Out3";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "0 0 0";
	logicPortDir[13] = 5;
	logicPortUIName[13] = "Out4";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "-2 0 0";
	logicPortDir[14] = 5;
	logicPortUIName[14] = "Out5";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "-4 0 0";
	logicPortDir[15] = 5;
	logicPortUIName[15] = "Out6";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "-6 0 0";
	logicPortDir[16] = 5;
	logicPortUIName[16] = "Out7";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "-8 0 0";
	logicPortDir[17] = 5;
	logicPortUIName[17] = "Out8";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "8 0 0";
	logicPortDir[18] = 2;
	logicPortUIName[18] = "Clock";
	logicPortCauseUpdate[18] = true;
	
};
