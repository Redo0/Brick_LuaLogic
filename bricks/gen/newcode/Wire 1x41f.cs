
datablock fxDtsBrickData(LogicWire_1x41f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x41f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x41f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x41f";
	
	logicBrickSize = "1 41 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
