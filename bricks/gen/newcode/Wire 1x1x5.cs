
datablock fxDtsBrickData(LogicWire_1x1x5_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x5.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x5";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x5";
	
	logicBrickSize = "1 1 15";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
