
datablock fxDtsBrickData(LogicGate_Mux5_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Mux 5 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Mux 5 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Mux 5 Bit";
	logicUIName = "Mux 5 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 38) then " @
		"		local idx = 6 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) + " @
		"			(Gate.getportstate(gate, 5) * 16) " @
		"		Gate.setportstate(gate, 39, Gate.getportstate(gate, idx)) " @
		"	else " @
		"		Gate.setportstate(gate, 39, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 39;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "Sel4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "31 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "In0";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "29 0 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "In1";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "27 0 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "In2";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "25 0 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "In3";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "23 0 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "In4";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "21 0 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "In5";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "19 0 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "In6";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "17 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "In7";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "15 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "In8";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "13 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "In9";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "11 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "In10";
	
	logicPortType[16] = 1;
	logicPortPos[16] = "9 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "In11";
	
	logicPortType[17] = 1;
	logicPortPos[17] = "7 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "In12";
	
	logicPortType[18] = 1;
	logicPortPos[18] = "5 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "In13";
	
	logicPortType[19] = 1;
	logicPortPos[19] = "3 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "In14";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "1 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "In15";
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-1 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "In16";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-3 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "In17";
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-5 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "In18";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-7 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "In19";
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-9 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "In20";
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-11 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "In21";
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-13 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "In22";
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-15 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "In23";
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-17 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "In24";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-19 0 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "In25";
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-21 0 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "In26";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "-23 0 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "In27";
	
	logicPortType[33] = 1;
	logicPortPos[33] = "-25 0 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "In28";
	
	logicPortType[34] = 1;
	logicPortPos[34] = "-27 0 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "In29";
	
	logicPortType[35] = 1;
	logicPortPos[35] = "-29 0 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "In30";
	
	logicPortType[36] = 1;
	logicPortPos[36] = "-31 0 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "In31";
	
	logicPortType[37] = 1;
	logicPortPos[37] = "31 0 0";
	logicPortDir[37] = 2;
	logicPortUIName[37] = "Enable";
	logicPortCauseUpdate[37] = true;
	
	logicPortType[38] = 0;
	logicPortPos[38] = "-31 0 0";
	logicPortDir[38] = 0;
	logicPortUIName[38] = "Out";
	
};
