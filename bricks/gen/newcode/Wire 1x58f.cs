
datablock fxDtsBrickData(LogicWire_1x58f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x58f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x58f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x58f";
	
	logicBrickSize = "1 58 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
