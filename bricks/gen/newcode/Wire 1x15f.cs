
datablock fxDtsBrickData(LogicWire_1x15f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x15f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x15f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x15f";
	
	logicBrickSize = "1 15 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
