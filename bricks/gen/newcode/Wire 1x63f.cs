
datablock fxDtsBrickData(LogicWire_1x63f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x63f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x63f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x63f";
	
	logicBrickSize = "1 63 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
