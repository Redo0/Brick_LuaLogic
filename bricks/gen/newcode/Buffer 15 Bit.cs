
datablock fxDtsBrickData(LogicGate_Buffer15Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 15 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 15 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 15 Bit";
	logicUIName = "Buffer 15 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "15 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 31)~=0 then " @
		"		Gate.setportstate(gate, 16, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 17, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 18, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 19, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 20, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 21, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 22, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 23, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 24, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 25, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 26, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 27, Gate.getportstate(gate, 12)) " @
		"		Gate.setportstate(gate, 28, Gate.getportstate(gate, 13)) " @
		"		Gate.setportstate(gate, 29, Gate.getportstate(gate, 14)) " @
		"		Gate.setportstate(gate, 30, Gate.getportstate(gate, 15)) " @
		"	else " @
		"		Gate.setportstate(gate, 16, 0) " @
		"		Gate.setportstate(gate, 17, 0) " @
		"		Gate.setportstate(gate, 18, 0) " @
		"		Gate.setportstate(gate, 19, 0) " @
		"		Gate.setportstate(gate, 20, 0) " @
		"		Gate.setportstate(gate, 21, 0) " @
		"		Gate.setportstate(gate, 22, 0) " @
		"		Gate.setportstate(gate, 23, 0) " @
		"		Gate.setportstate(gate, 24, 0) " @
		"		Gate.setportstate(gate, 25, 0) " @
		"		Gate.setportstate(gate, 26, 0) " @
		"		Gate.setportstate(gate, 27, 0) " @
		"		Gate.setportstate(gate, 28, 0) " @
		"		Gate.setportstate(gate, 29, 0) " @
		"		Gate.setportstate(gate, 30, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 31;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "14 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "12 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "10 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "8 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "6 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "4 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "2 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-2 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-4 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-6 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "-8 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "-10 0 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "-12 0 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "-14 0 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "14 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out0";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "12 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out1";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "10 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out2";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "8 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out3";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "6 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out4";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "4 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out5";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "2 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out6";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "0 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out7";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "-2 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out8";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "-4 0 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "Out9";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "-6 0 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "Out10";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "-8 0 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "Out11";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "-10 0 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "Out12";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "-12 0 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "Out13";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "-14 0 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "Out14";
	
	logicPortType[30] = 1;
	logicPortPos[30] = "14 0 0";
	logicPortDir[30] = 2;
	logicPortUIName[30] = "Clock";
	logicPortCauseUpdate[30] = true;
	
};
