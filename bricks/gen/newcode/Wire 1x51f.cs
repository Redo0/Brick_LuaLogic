
datablock fxDtsBrickData(LogicWire_1x51f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x51f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x51f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x51f";
	
	logicBrickSize = "1 51 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
