
datablock fxDtsBrickData(LogicGate_Adder4Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 4 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 4 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 4 Bit";
	logicUIName = "Adder 4 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 5) + Gate.getportstate(gate, 13)) " @
		" + ((Gate.getportstate(gate, 2) + Gate.getportstate(gate, 6)) * 2) " @
		" + ((Gate.getportstate(gate, 3) + Gate.getportstate(gate, 7)) * 4) " @
		" + ((Gate.getportstate(gate, 4) + Gate.getportstate(gate, 8)) * 8) " @
		") " @
		"if val >= 16 then val = val-16; Gate.setportstate(gate, 14, 1); else Gate.setportstate(gate, 14, 0) end " @
		"if val >= 8 then val = val-8; Gate.setportstate(gate, 12, 1); else Gate.setportstate(gate, 12, 0) end " @
		"if val >= 4 then val = val-4; Gate.setportstate(gate, 11, 1); else Gate.setportstate(gate, 11, 0) end " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 10, 1); else Gate.setportstate(gate, 10, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 9, 1); else Gate.setportstate(gate, 9, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 14;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-1 -1 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "B0";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-3 -1 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "B1";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-5 -1 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "B2";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-7 -1 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "B3";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 0;
	logicPortPos[8] = "7 1 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O0";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "5 1 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O1";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "3 1 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O2";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "1 1 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O3";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "7 -1 0";
	logicPortDir[12] = 2;
	logicPortUIName[12] = "CIn";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 0;
	logicPortPos[13] = "-7 -1 0";
	logicPortDir[13] = 0;
	logicPortUIName[13] = "COut";
	logicPortCauseUpdate[13] = true;
	
};
