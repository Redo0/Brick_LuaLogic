
datablock fxDtsBrickData(LogicGate_Adder16Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Adder 16 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Adder 16 Bit";
	
	category = "Logic Bricks";
	subCategory = "Math";
	uiName = "Adder 16 Bit";
	logicUIName = "Adder 16 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "32 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"local val = ( " @
		"   ( Gate.getportstate(gate, 1) + Gate.getportstate(gate, 17) + Gate.getportstate(gate, 49)) " @
		" + ((Gate.getportstate(gate, 2) + Gate.getportstate(gate, 18)) * 2) " @
		" + ((Gate.getportstate(gate, 3) + Gate.getportstate(gate, 19)) * 4) " @
		" + ((Gate.getportstate(gate, 4) + Gate.getportstate(gate, 20)) * 8) " @
		" + ((Gate.getportstate(gate, 5) + Gate.getportstate(gate, 21)) * 16) " @
		" + ((Gate.getportstate(gate, 6) + Gate.getportstate(gate, 22)) * 32) " @
		" + ((Gate.getportstate(gate, 7) + Gate.getportstate(gate, 23)) * 64) " @
		" + ((Gate.getportstate(gate, 8) + Gate.getportstate(gate, 24)) * 128) " @
		" + ((Gate.getportstate(gate, 9) + Gate.getportstate(gate, 25)) * 256) " @
		" + ((Gate.getportstate(gate, 10) + Gate.getportstate(gate, 26)) * 512) " @
		" + ((Gate.getportstate(gate, 11) + Gate.getportstate(gate, 27)) * 1024) " @
		" + ((Gate.getportstate(gate, 12) + Gate.getportstate(gate, 28)) * 2048) " @
		" + ((Gate.getportstate(gate, 13) + Gate.getportstate(gate, 29)) * 4096) " @
		" + ((Gate.getportstate(gate, 14) + Gate.getportstate(gate, 30)) * 8192) " @
		" + ((Gate.getportstate(gate, 15) + Gate.getportstate(gate, 31)) * 16384) " @
		" + ((Gate.getportstate(gate, 16) + Gate.getportstate(gate, 32)) * 32768) " @
		") " @
		"if val >= 65536 then val = val-65536; Gate.setportstate(gate, 50, 1); else Gate.setportstate(gate, 50, 0) end " @
		"if val >= 32768 then val = val-32768; Gate.setportstate(gate, 48, 1); else Gate.setportstate(gate, 48, 0) end " @
		"if val >= 16384 then val = val-16384; Gate.setportstate(gate, 47, 1); else Gate.setportstate(gate, 47, 0) end " @
		"if val >= 8192 then val = val-8192; Gate.setportstate(gate, 46, 1); else Gate.setportstate(gate, 46, 0) end " @
		"if val >= 4096 then val = val-4096; Gate.setportstate(gate, 45, 1); else Gate.setportstate(gate, 45, 0) end " @
		"if val >= 2048 then val = val-2048; Gate.setportstate(gate, 44, 1); else Gate.setportstate(gate, 44, 0) end " @
		"if val >= 1024 then val = val-1024; Gate.setportstate(gate, 43, 1); else Gate.setportstate(gate, 43, 0) end " @
		"if val >= 512 then val = val-512; Gate.setportstate(gate, 42, 1); else Gate.setportstate(gate, 42, 0) end " @
		"if val >= 256 then val = val-256; Gate.setportstate(gate, 41, 1); else Gate.setportstate(gate, 41, 0) end " @
		"if val >= 128 then val = val-128; Gate.setportstate(gate, 40, 1); else Gate.setportstate(gate, 40, 0) end " @
		"if val >= 64 then val = val-64; Gate.setportstate(gate, 39, 1); else Gate.setportstate(gate, 39, 0) end " @
		"if val >= 32 then val = val-32; Gate.setportstate(gate, 38, 1); else Gate.setportstate(gate, 38, 0) end " @
		"if val >= 16 then val = val-16; Gate.setportstate(gate, 37, 1); else Gate.setportstate(gate, 37, 0) end " @
		"if val >= 8 then val = val-8; Gate.setportstate(gate, 36, 1); else Gate.setportstate(gate, 36, 0) end " @
		"if val >= 4 then val = val-4; Gate.setportstate(gate, 35, 1); else Gate.setportstate(gate, 35, 0) end " @
		"if val >= 2 then val = val-2; Gate.setportstate(gate, 34, 1); else Gate.setportstate(gate, 34, 0) end " @
		"if val >= 1 then val = val-1; Gate.setportstate(gate, 33, 1); else Gate.setportstate(gate, 33, 0) end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 50;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "31 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "29 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "27 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "25 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "23 -1 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 1;
	logicPortPos[5] = "21 -1 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	logicPortCauseUpdate[5] = true;
	
	logicPortType[6] = 1;
	logicPortPos[6] = "19 -1 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	logicPortCauseUpdate[6] = true;
	
	logicPortType[7] = 1;
	logicPortPos[7] = "17 -1 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	logicPortCauseUpdate[7] = true;
	
	logicPortType[8] = 1;
	logicPortPos[8] = "15 -1 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	logicPortCauseUpdate[8] = true;
	
	logicPortType[9] = 1;
	logicPortPos[9] = "13 -1 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	logicPortCauseUpdate[9] = true;
	
	logicPortType[10] = 1;
	logicPortPos[10] = "11 -1 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	logicPortCauseUpdate[10] = true;
	
	logicPortType[11] = 1;
	logicPortPos[11] = "9 -1 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "A11";
	logicPortCauseUpdate[11] = true;
	
	logicPortType[12] = 1;
	logicPortPos[12] = "7 -1 0";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "A12";
	logicPortCauseUpdate[12] = true;
	
	logicPortType[13] = 1;
	logicPortPos[13] = "5 -1 0";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "A13";
	logicPortCauseUpdate[13] = true;
	
	logicPortType[14] = 1;
	logicPortPos[14] = "3 -1 0";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "A14";
	logicPortCauseUpdate[14] = true;
	
	logicPortType[15] = 1;
	logicPortPos[15] = "1 -1 0";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "A15";
	logicPortCauseUpdate[15] = true;
	
	logicPortType[16] = 1;
	logicPortPos[16] = "-1 -1 0";
	logicPortDir[16] = 3;
	logicPortUIName[16] = "B0";
	logicPortCauseUpdate[16] = true;
	
	logicPortType[17] = 1;
	logicPortPos[17] = "-3 -1 0";
	logicPortDir[17] = 3;
	logicPortUIName[17] = "B1";
	logicPortCauseUpdate[17] = true;
	
	logicPortType[18] = 1;
	logicPortPos[18] = "-5 -1 0";
	logicPortDir[18] = 3;
	logicPortUIName[18] = "B2";
	logicPortCauseUpdate[18] = true;
	
	logicPortType[19] = 1;
	logicPortPos[19] = "-7 -1 0";
	logicPortDir[19] = 3;
	logicPortUIName[19] = "B3";
	logicPortCauseUpdate[19] = true;
	
	logicPortType[20] = 1;
	logicPortPos[20] = "-9 -1 0";
	logicPortDir[20] = 3;
	logicPortUIName[20] = "B4";
	logicPortCauseUpdate[20] = true;
	
	logicPortType[21] = 1;
	logicPortPos[21] = "-11 -1 0";
	logicPortDir[21] = 3;
	logicPortUIName[21] = "B5";
	logicPortCauseUpdate[21] = true;
	
	logicPortType[22] = 1;
	logicPortPos[22] = "-13 -1 0";
	logicPortDir[22] = 3;
	logicPortUIName[22] = "B6";
	logicPortCauseUpdate[22] = true;
	
	logicPortType[23] = 1;
	logicPortPos[23] = "-15 -1 0";
	logicPortDir[23] = 3;
	logicPortUIName[23] = "B7";
	logicPortCauseUpdate[23] = true;
	
	logicPortType[24] = 1;
	logicPortPos[24] = "-17 -1 0";
	logicPortDir[24] = 3;
	logicPortUIName[24] = "B8";
	logicPortCauseUpdate[24] = true;
	
	logicPortType[25] = 1;
	logicPortPos[25] = "-19 -1 0";
	logicPortDir[25] = 3;
	logicPortUIName[25] = "B9";
	logicPortCauseUpdate[25] = true;
	
	logicPortType[26] = 1;
	logicPortPos[26] = "-21 -1 0";
	logicPortDir[26] = 3;
	logicPortUIName[26] = "B10";
	logicPortCauseUpdate[26] = true;
	
	logicPortType[27] = 1;
	logicPortPos[27] = "-23 -1 0";
	logicPortDir[27] = 3;
	logicPortUIName[27] = "B11";
	logicPortCauseUpdate[27] = true;
	
	logicPortType[28] = 1;
	logicPortPos[28] = "-25 -1 0";
	logicPortDir[28] = 3;
	logicPortUIName[28] = "B12";
	logicPortCauseUpdate[28] = true;
	
	logicPortType[29] = 1;
	logicPortPos[29] = "-27 -1 0";
	logicPortDir[29] = 3;
	logicPortUIName[29] = "B13";
	logicPortCauseUpdate[29] = true;
	
	logicPortType[30] = 1;
	logicPortPos[30] = "-29 -1 0";
	logicPortDir[30] = 3;
	logicPortUIName[30] = "B14";
	logicPortCauseUpdate[30] = true;
	
	logicPortType[31] = 1;
	logicPortPos[31] = "-31 -1 0";
	logicPortDir[31] = 3;
	logicPortUIName[31] = "B15";
	logicPortCauseUpdate[31] = true;
	
	logicPortType[32] = 0;
	logicPortPos[32] = "31 1 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O0";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "29 1 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O1";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "27 1 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O2";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "25 1 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O3";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "23 1 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O4";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "21 1 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O5";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "19 1 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O6";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "17 1 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O7";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "15 1 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O8";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "13 1 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O9";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "11 1 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "O10";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "9 1 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "O11";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "7 1 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "O12";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "5 1 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "O13";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "3 1 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "O14";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "1 1 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "O15";
	
	logicPortType[48] = 1;
	logicPortPos[48] = "31 -1 0";
	logicPortDir[48] = 2;
	logicPortUIName[48] = "CIn";
	logicPortCauseUpdate[48] = true;
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-31 -1 0";
	logicPortDir[49] = 0;
	logicPortUIName[49] = "COut";
	logicPortCauseUpdate[49] = true;
	
};
