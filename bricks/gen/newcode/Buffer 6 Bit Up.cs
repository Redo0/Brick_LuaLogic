
datablock fxDtsBrickData(LogicGate_Buffer6BitUp_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Buffer 6 Bit Up.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Buffer 6 Bit Up";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "Buffer 6 Bit Up";
	logicUIName = "Buffer 6 Bit Up";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "6 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 13)~=0 then " @
		"		Gate.setportstate(gate, 7, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 8, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 9, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 10, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 11, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 12, Gate.getportstate(gate, 6)) " @
		"	else " @
		"		Gate.setportstate(gate, 7, 0) " @
		"		Gate.setportstate(gate, 8, 0) " @
		"		Gate.setportstate(gate, 9, 0) " @
		"		Gate.setportstate(gate, 10, 0) " @
		"		Gate.setportstate(gate, 11, 0) " @
		"		Gate.setportstate(gate, 12, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 13;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "5 0 0";
	logicPortDir[0] = 5;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "3 0 0";
	logicPortDir[1] = 5;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "1 0 0";
	logicPortDir[2] = 5;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-1 0 0";
	logicPortDir[3] = 5;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-3 0 0";
	logicPortDir[4] = 5;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-5 0 0";
	logicPortDir[5] = 5;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "5 0 0";
	logicPortDir[6] = 4;
	logicPortUIName[6] = "Out0";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "3 0 0";
	logicPortDir[7] = 4;
	logicPortUIName[7] = "Out1";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "1 0 0";
	logicPortDir[8] = 4;
	logicPortUIName[8] = "Out2";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "-1 0 0";
	logicPortDir[9] = 4;
	logicPortUIName[9] = "Out3";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "-3 0 0";
	logicPortDir[10] = 4;
	logicPortUIName[10] = "Out4";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "-5 0 0";
	logicPortDir[11] = 4;
	logicPortUIName[11] = "Out5";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "5 0 0";
	logicPortDir[12] = 2;
	logicPortUIName[12] = "Clock";
	logicPortCauseUpdate[12] = true;
	
};
