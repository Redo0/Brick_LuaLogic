
datablock fxDtsBrickData(LogicGate_GateNor5_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/NOR 5 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/NOR 5 Bit";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "NOR 5 Bit";
	logicUIName = "NOR 5 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "5 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 6, (not ( " @
		"		(Gate.getportstate(gate, 1)~=0) or " @
		"		(Gate.getportstate(gate, 2)~=0) or " @
		"		(Gate.getportstate(gate, 3)~=0) or " @
		"		(Gate.getportstate(gate, 4)~=0) or " @
		"		(Gate.getportstate(gate, 5)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 6;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "4 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "2 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 1;
	logicPortPos[3] = "-2 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	logicPortCauseUpdate[3] = true;
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-4 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	logicPortCauseUpdate[4] = true;
	
	logicPortType[5] = 0;
	logicPortPos[5] = "4 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out";
	
};
