
datablock fxDtsBrickData(LogicWire_1x1x7_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x7.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x7";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x7";
	
	logicBrickSize = "1 1 21";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
