
datablock fxDtsBrickData(LogicWire_1x45f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x45f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x45f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x45f";
	
	logicBrickSize = "1 45 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
