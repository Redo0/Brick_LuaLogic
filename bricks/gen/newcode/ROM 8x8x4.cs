
datablock fxDtsBrickData(LogicGate_Rom8x8x4_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 8x8x4.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 8x8x4";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 8x8x4";
	logicUIName = "ROM 8x8x4";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 8 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 255 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 11;
	
	isLogicRom = true;
	logicRomY = 8;
	logicRomZ = 4;
	logicRomX = 8;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 -7 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 -7 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 -7 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 -7 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "-1 -7 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "-3 -7 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "7 7 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "O0";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "5 7 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "O1";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "3 7 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O2";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "1 7 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O3";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "7 -7 0";
	logicPortDir[10] = 2;
	logicPortUIName[10] = "Clock";
	logicPortCauseUpdate[10] = true;
	
};
