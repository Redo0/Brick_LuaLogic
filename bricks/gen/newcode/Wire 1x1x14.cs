
datablock fxDtsBrickData(LogicWire_1x1x14_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x14.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x14";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x14";
	
	logicBrickSize = "1 1 42";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
