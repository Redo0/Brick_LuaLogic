
datablock fxDtsBrickData(LogicGate_Rom8x2x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 8x2x8.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 8x2x8";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 8x2x8";
	logicUIName = "ROM 8x2x8";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "8 2 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 127 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 13;
	
	isLogicRom = true;
	logicRomY = 2;
	logicRomZ = 8;
	logicRomX = 8;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "7 -1 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "5 -1 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "3 -1 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "1 -1 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "7 1 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "O0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "5 1 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "O1";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "3 1 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "O2";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "1 1 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "O3";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "-1 1 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O4";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "-3 1 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O5";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "-5 1 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O6";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "-7 1 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O7";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "7 -1 0";
	logicPortDir[12] = 2;
	logicPortUIName[12] = "Clock";
	logicPortCauseUpdate[12] = true;
	
};
