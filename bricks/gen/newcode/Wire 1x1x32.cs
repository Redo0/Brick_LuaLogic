
datablock fxDtsBrickData(LogicWire_1x1x32_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x32.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x32";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x32";
	
	logicBrickSize = "1 1 96";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
