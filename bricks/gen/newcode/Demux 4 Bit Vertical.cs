
datablock fxDtsBrickData(LogicGate_Demux4Vertical_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 4 Bit Vertical.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 4 Bit Vertical";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 4 Bit Vertical";
	logicUIName = "Demux 4 Bit Vertical";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "1 1 16";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 5 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 21)~=0 then " @
		"		local idx = 5 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) + " @
		"			(Gate.getportstate(gate, 3) * 4) + " @
		"			(Gate.getportstate(gate, 4) * 8) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 21;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -15";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 -13";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 0 -11";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "Sel2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 0 -9";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "Sel3";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "0 0 -15";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "Out0";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "0 0 -13";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out1";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "0 0 -11";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "Out2";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "0 0 -9";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "Out3";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "0 0 -7";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "Out4";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "0 0 -5";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "Out5";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "0 0 -3";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "Out6";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "0 0 -1";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "Out7";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "0 0 1";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out8";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "0 0 3";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out9";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "0 0 5";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out10";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "0 0 7";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out11";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 0 9";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out12";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "0 0 11";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out13";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "0 0 13";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out14";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "0 0 15";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out15";
	
	logicPortType[20] = 1;
	logicPortPos[20] = "0 0 -15";
	logicPortDir[20] = 5;
	logicPortUIName[20] = "Enable";
	logicPortCauseUpdate[20] = true;
	
};
