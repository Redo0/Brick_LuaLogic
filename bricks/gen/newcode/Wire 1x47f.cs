
datablock fxDtsBrickData(LogicWire_1x47f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x47f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x47f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x47f";
	
	logicBrickSize = "1 47 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
