
datablock fxDtsBrickData(LogicWire_1x1x22_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x22.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x22";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x22";
	
	logicBrickSize = "1 1 66";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
