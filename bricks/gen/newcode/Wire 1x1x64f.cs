
datablock fxDtsBrickData(LogicWire_1x1x64f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x64f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x64f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x64f";
	
	logicBrickSize = "1 1 64";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
