
datablock fxDtsBrickData(LogicGate_DFlipFlop12Bit_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/D FlipFlop 12 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/D FlipFlop 12 Bit";
	
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "D FlipFlop 12 Bit";
	logicUIName = "D FlipFlop 12 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "12 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 25)~=0 then " @
		"		Gate.setportstate(gate, 13, Gate.getportstate(gate, 1)) " @
		"		Gate.setportstate(gate, 14, Gate.getportstate(gate, 2)) " @
		"		Gate.setportstate(gate, 15, Gate.getportstate(gate, 3)) " @
		"		Gate.setportstate(gate, 16, Gate.getportstate(gate, 4)) " @
		"		Gate.setportstate(gate, 17, Gate.getportstate(gate, 5)) " @
		"		Gate.setportstate(gate, 18, Gate.getportstate(gate, 6)) " @
		"		Gate.setportstate(gate, 19, Gate.getportstate(gate, 7)) " @
		"		Gate.setportstate(gate, 20, Gate.getportstate(gate, 8)) " @
		"		Gate.setportstate(gate, 21, Gate.getportstate(gate, 9)) " @
		"		Gate.setportstate(gate, 22, Gate.getportstate(gate, 10)) " @
		"		Gate.setportstate(gate, 23, Gate.getportstate(gate, 11)) " @
		"		Gate.setportstate(gate, 24, Gate.getportstate(gate, 12)) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 25;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "11 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "9 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "7 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "5 0 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "3 0 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "1 0 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "-1 0 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "-3 0 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "-5 0 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "-7 0 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "-9 0 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "-11 0 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "11 0 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "Out0";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "9 0 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "Out1";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "7 0 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "Out2";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "5 0 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "Out3";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "3 0 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "Out4";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "1 0 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "Out5";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "-1 0 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "Out6";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "-3 0 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "Out7";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "-5 0 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "Out8";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "-7 0 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "Out9";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "-9 0 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "Out10";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "-11 0 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "Out11";
	
	logicPortType[24] = 1;
	logicPortPos[24] = "11 0 0";
	logicPortDir[24] = 2;
	logicPortUIName[24] = "Clock";
	logicPortCauseUpdate[24] = true;
	
};
