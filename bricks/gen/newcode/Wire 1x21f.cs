
datablock fxDtsBrickData(LogicWire_1x21f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x21f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x21f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x21f";
	
	logicBrickSize = "1 21 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
