
datablock fxDtsBrickData(LogicWire_1x14f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x14f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x14f";
	
	category = "Logic Bricks";
	subCategory = "Wires Horizontal";
	uiName = "Wire 1x14f";
	
	logicBrickSize = "1 14 1";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
