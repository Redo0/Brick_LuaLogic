
datablock fxDtsBrickData(LogicWire_1x1x4f_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x4f.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x4f";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x4f";
	
	logicBrickSize = "1 1 4";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
