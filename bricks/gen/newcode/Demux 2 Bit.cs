
datablock fxDtsBrickData(LogicGate_Demux2_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Demux 2 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Demux 2 Bit";
	
	category = "Logic Bricks";
	subCategory = "Mux";
	uiName = "Demux 2 Bit";
	logicUIName = "Demux 2 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "4 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	gate.laston = 3 " @
		"end"
	;
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	if Gate.getportstate(gate, 7)~=0 then " @
		"		local idx = 3 + " @
		"			(Gate.getportstate(gate, 1) * 1) + " @
		"			(Gate.getportstate(gate, 2) * 2) " @
		"		Gate.setportstate(gate, idx, 1) " @
		"		if gate.laston~=idx then " @
		"			Gate.setportstate(gate, gate.laston, 0) " @
		"			gate.laston = idx " @
		"		end " @
		"	else " @
		"		Gate.setportstate(gate, gate.laston, 0) " @
		"	end " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 7;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "3 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "Sel0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "1 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "Sel1";
	
	logicPortType[2] = 0;
	logicPortPos[2] = "3 0 0";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "Out0";
	
	logicPortType[3] = 0;
	logicPortPos[3] = "1 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Out1";
	
	logicPortType[4] = 0;
	logicPortPos[4] = "-1 0 0";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "Out2";
	
	logicPortType[5] = 0;
	logicPortPos[5] = "-3 0 0";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "Out3";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "3 0 0";
	logicPortDir[6] = 2;
	logicPortUIName[6] = "Enable";
	logicPortCauseUpdate[6] = true;
	
};
