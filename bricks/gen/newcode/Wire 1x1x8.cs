
datablock fxDtsBrickData(LogicWire_1x1x8_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/Wire 1x1x8.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/Wire 1x1x8";
	
	category = "Logic Bricks";
	subCategory = "Wires Vertical";
	uiName = "Wire 1x1x8";
	
	logicBrickSize = "1 1 24";
	orientationFix = 0;
	
	isLogic = true;
	isLogicWire = true;
	
	
	
	
};
