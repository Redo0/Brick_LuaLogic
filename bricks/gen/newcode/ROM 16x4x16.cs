
datablock fxDtsBrickData(LogicGate_Rom16x4x16_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 16x4x16.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 16x4x16";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 16x4x16";
	logicUIName = "ROM 16x4x16";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "16 4 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 1023 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 23;
	
	isLogicRom = true;
	logicRomY = 4;
	logicRomZ = 16;
	logicRomX = 16;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "15 -3 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "13 -3 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "11 -3 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "9 -3 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "7 -3 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "5 -3 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 0;
	logicPortPos[6] = "15 3 0";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "O0";
	
	logicPortType[7] = 0;
	logicPortPos[7] = "13 3 0";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "O1";
	
	logicPortType[8] = 0;
	logicPortPos[8] = "11 3 0";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "O2";
	
	logicPortType[9] = 0;
	logicPortPos[9] = "9 3 0";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "O3";
	
	logicPortType[10] = 0;
	logicPortPos[10] = "7 3 0";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "O4";
	
	logicPortType[11] = 0;
	logicPortPos[11] = "5 3 0";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "O5";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "3 3 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O6";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "1 3 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O7";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "-1 3 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O8";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "-3 3 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O9";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "-5 3 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O10";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "-7 3 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O11";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "-9 3 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O12";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "-11 3 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O13";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "-13 3 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O14";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "-15 3 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O15";
	
	logicPortType[22] = 1;
	logicPortPos[22] = "15 -3 0";
	logicPortDir[22] = 2;
	logicPortUIName[22] = "Clock";
	logicPortCauseUpdate[22] = true;
	
};
