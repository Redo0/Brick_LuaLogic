
datablock fxDtsBrickData(LogicGate_Rom64x64x64_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/ROM 64x64x64.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/ROM 64x64x64";
	
	category = "Logic Bricks";
	subCategory = "ROM";
	uiName = "ROM 64x64x64";
	logicUIName = "ROM 64x64x64";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "64 64 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = 
		"return function(gate) " @
		"	for i = 0, 262143 do " @
		"		Gate.setcdata(gate, i, 0) " @
		"	end " @
		"end"
	;
	logicInput = 
		"return function(gate, args) " @
		"	local data = args[1] " @
		"	for i = 1, #data do " @
		"		local c = data:sub(i, i) " @
		"		local v = (c==\"1\") and 1 or 0 " @
		"		Gate.setcdata(gate, i-1, v) " @
		"	end " @
		"	Gate.queue(gate, 0) " @
		"end"
	;
	logicUpdate = "return function(gate) end";
	logicGlobal = "";
	
	numLogicPorts = 77;
	
	isLogicRom = true;
	logicRomY = 64;
	logicRomZ = 64;
	logicRomX = 64;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "63 -63 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "A0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "61 -63 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "A1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "59 -63 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "A2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "57 -63 0";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "A3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "55 -63 0";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "A4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "53 -63 0";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "A5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "51 -63 0";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "A6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "49 -63 0";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "A7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "47 -63 0";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "A8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "45 -63 0";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "A9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "43 -63 0";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "A10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "41 -63 0";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "A11";
	
	logicPortType[12] = 0;
	logicPortPos[12] = "63 63 0";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "O0";
	
	logicPortType[13] = 0;
	logicPortPos[13] = "61 63 0";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "O1";
	
	logicPortType[14] = 0;
	logicPortPos[14] = "59 63 0";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "O2";
	
	logicPortType[15] = 0;
	logicPortPos[15] = "57 63 0";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "O3";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "55 63 0";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "O4";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "53 63 0";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "O5";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "51 63 0";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "O6";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "49 63 0";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "O7";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "47 63 0";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "O8";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "45 63 0";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "O9";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "43 63 0";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "O10";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "41 63 0";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "O11";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "39 63 0";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "O12";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "37 63 0";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "O13";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "35 63 0";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "O14";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "33 63 0";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "O15";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "31 63 0";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "O16";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "29 63 0";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "O17";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "27 63 0";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "O18";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "25 63 0";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "O19";
	
	logicPortType[32] = 0;
	logicPortPos[32] = "23 63 0";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "O20";
	
	logicPortType[33] = 0;
	logicPortPos[33] = "21 63 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "O21";
	
	logicPortType[34] = 0;
	logicPortPos[34] = "19 63 0";
	logicPortDir[34] = 1;
	logicPortUIName[34] = "O22";
	
	logicPortType[35] = 0;
	logicPortPos[35] = "17 63 0";
	logicPortDir[35] = 1;
	logicPortUIName[35] = "O23";
	
	logicPortType[36] = 0;
	logicPortPos[36] = "15 63 0";
	logicPortDir[36] = 1;
	logicPortUIName[36] = "O24";
	
	logicPortType[37] = 0;
	logicPortPos[37] = "13 63 0";
	logicPortDir[37] = 1;
	logicPortUIName[37] = "O25";
	
	logicPortType[38] = 0;
	logicPortPos[38] = "11 63 0";
	logicPortDir[38] = 1;
	logicPortUIName[38] = "O26";
	
	logicPortType[39] = 0;
	logicPortPos[39] = "9 63 0";
	logicPortDir[39] = 1;
	logicPortUIName[39] = "O27";
	
	logicPortType[40] = 0;
	logicPortPos[40] = "7 63 0";
	logicPortDir[40] = 1;
	logicPortUIName[40] = "O28";
	
	logicPortType[41] = 0;
	logicPortPos[41] = "5 63 0";
	logicPortDir[41] = 1;
	logicPortUIName[41] = "O29";
	
	logicPortType[42] = 0;
	logicPortPos[42] = "3 63 0";
	logicPortDir[42] = 1;
	logicPortUIName[42] = "O30";
	
	logicPortType[43] = 0;
	logicPortPos[43] = "1 63 0";
	logicPortDir[43] = 1;
	logicPortUIName[43] = "O31";
	
	logicPortType[44] = 0;
	logicPortPos[44] = "-1 63 0";
	logicPortDir[44] = 1;
	logicPortUIName[44] = "O32";
	
	logicPortType[45] = 0;
	logicPortPos[45] = "-3 63 0";
	logicPortDir[45] = 1;
	logicPortUIName[45] = "O33";
	
	logicPortType[46] = 0;
	logicPortPos[46] = "-5 63 0";
	logicPortDir[46] = 1;
	logicPortUIName[46] = "O34";
	
	logicPortType[47] = 0;
	logicPortPos[47] = "-7 63 0";
	logicPortDir[47] = 1;
	logicPortUIName[47] = "O35";
	
	logicPortType[48] = 0;
	logicPortPos[48] = "-9 63 0";
	logicPortDir[48] = 1;
	logicPortUIName[48] = "O36";
	
	logicPortType[49] = 0;
	logicPortPos[49] = "-11 63 0";
	logicPortDir[49] = 1;
	logicPortUIName[49] = "O37";
	
	logicPortType[50] = 0;
	logicPortPos[50] = "-13 63 0";
	logicPortDir[50] = 1;
	logicPortUIName[50] = "O38";
	
	logicPortType[51] = 0;
	logicPortPos[51] = "-15 63 0";
	logicPortDir[51] = 1;
	logicPortUIName[51] = "O39";
	
	logicPortType[52] = 0;
	logicPortPos[52] = "-17 63 0";
	logicPortDir[52] = 1;
	logicPortUIName[52] = "O40";
	
	logicPortType[53] = 0;
	logicPortPos[53] = "-19 63 0";
	logicPortDir[53] = 1;
	logicPortUIName[53] = "O41";
	
	logicPortType[54] = 0;
	logicPortPos[54] = "-21 63 0";
	logicPortDir[54] = 1;
	logicPortUIName[54] = "O42";
	
	logicPortType[55] = 0;
	logicPortPos[55] = "-23 63 0";
	logicPortDir[55] = 1;
	logicPortUIName[55] = "O43";
	
	logicPortType[56] = 0;
	logicPortPos[56] = "-25 63 0";
	logicPortDir[56] = 1;
	logicPortUIName[56] = "O44";
	
	logicPortType[57] = 0;
	logicPortPos[57] = "-27 63 0";
	logicPortDir[57] = 1;
	logicPortUIName[57] = "O45";
	
	logicPortType[58] = 0;
	logicPortPos[58] = "-29 63 0";
	logicPortDir[58] = 1;
	logicPortUIName[58] = "O46";
	
	logicPortType[59] = 0;
	logicPortPos[59] = "-31 63 0";
	logicPortDir[59] = 1;
	logicPortUIName[59] = "O47";
	
	logicPortType[60] = 0;
	logicPortPos[60] = "-33 63 0";
	logicPortDir[60] = 1;
	logicPortUIName[60] = "O48";
	
	logicPortType[61] = 0;
	logicPortPos[61] = "-35 63 0";
	logicPortDir[61] = 1;
	logicPortUIName[61] = "O49";
	
	logicPortType[62] = 0;
	logicPortPos[62] = "-37 63 0";
	logicPortDir[62] = 1;
	logicPortUIName[62] = "O50";
	
	logicPortType[63] = 0;
	logicPortPos[63] = "-39 63 0";
	logicPortDir[63] = 1;
	logicPortUIName[63] = "O51";
	
	logicPortType[64] = 0;
	logicPortPos[64] = "-41 63 0";
	logicPortDir[64] = 1;
	logicPortUIName[64] = "O52";
	
	logicPortType[65] = 0;
	logicPortPos[65] = "-43 63 0";
	logicPortDir[65] = 1;
	logicPortUIName[65] = "O53";
	
	logicPortType[66] = 0;
	logicPortPos[66] = "-45 63 0";
	logicPortDir[66] = 1;
	logicPortUIName[66] = "O54";
	
	logicPortType[67] = 0;
	logicPortPos[67] = "-47 63 0";
	logicPortDir[67] = 1;
	logicPortUIName[67] = "O55";
	
	logicPortType[68] = 0;
	logicPortPos[68] = "-49 63 0";
	logicPortDir[68] = 1;
	logicPortUIName[68] = "O56";
	
	logicPortType[69] = 0;
	logicPortPos[69] = "-51 63 0";
	logicPortDir[69] = 1;
	logicPortUIName[69] = "O57";
	
	logicPortType[70] = 0;
	logicPortPos[70] = "-53 63 0";
	logicPortDir[70] = 1;
	logicPortUIName[70] = "O58";
	
	logicPortType[71] = 0;
	logicPortPos[71] = "-55 63 0";
	logicPortDir[71] = 1;
	logicPortUIName[71] = "O59";
	
	logicPortType[72] = 0;
	logicPortPos[72] = "-57 63 0";
	logicPortDir[72] = 1;
	logicPortUIName[72] = "O60";
	
	logicPortType[73] = 0;
	logicPortPos[73] = "-59 63 0";
	logicPortDir[73] = 1;
	logicPortUIName[73] = "O61";
	
	logicPortType[74] = 0;
	logicPortPos[74] = "-61 63 0";
	logicPortDir[74] = 1;
	logicPortUIName[74] = "O62";
	
	logicPortType[75] = 0;
	logicPortPos[75] = "-63 63 0";
	logicPortDir[75] = 1;
	logicPortUIName[75] = "O63";
	
	logicPortType[76] = 1;
	logicPortPos[76] = "63 -63 0";
	logicPortDir[76] = 2;
	logicPortUIName[76] = "Clock";
	logicPortCauseUpdate[76] = true;
	
};
