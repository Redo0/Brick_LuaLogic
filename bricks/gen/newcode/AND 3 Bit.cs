
datablock fxDtsBrickData(LogicGate_GateAnd3_Data){
	brickFile = $LuaLogic::Path @ "bricks/gen/newbricks/AND 3 Bit.blb";
	iconName = $LuaLogic::Path @ "bricks/gen/newicons/AND 3 Bit";
	
	category = "Logic Bricks";
	subCategory = "Gates";
	uiName = "AND 3 Bit";
	logicUIName = "AND 3 Bit";
	logicUIDesc = "";
	
	hasPrint = 1;
	printAspectRatio = "Logic";
	
	logicBrickSize = "3 1 1";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicInit = "";
	logicInput = "";
	logicUpdate = 
		"return function(gate) " @
		"	Gate.setportstate(gate, 4, (( " @
		"		(Gate.getportstate(gate, 1)~=0) and " @
		"		(Gate.getportstate(gate, 2)~=0) and " @
		"		(Gate.getportstate(gate, 3)~=0) " @
		"	)) and 1 or 0) " @
		"end"
	;
	logicGlobal = "";
	
	numLogicPorts = 4;
	
	
	
	logicPortType[0] = 1;
	logicPortPos[0] = "2 0 0";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "In0";
	logicPortCauseUpdate[0] = true;
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 0 0";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "In1";
	logicPortCauseUpdate[1] = true;
	
	logicPortType[2] = 1;
	logicPortPos[2] = "-2 0 0";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "In2";
	logicPortCauseUpdate[2] = true;
	
	logicPortType[3] = 0;
	logicPortPos[3] = "2 0 0";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "Out";
	
};
