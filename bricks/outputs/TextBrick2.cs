
datablock fxDTSBrickData(LogicGate_TextBrick2_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Text Brick";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicUIName = "Text Brick";
	logicUIDesc = "Takes 9-bit serial input of format 1cxxxxxxxx10. If c==0, x = 8-bit ascii code. If c==1, lower 6 bits of x = color ID, top bit = invert character. MSB first.";
	
	logicInit   = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-init.lua"  );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-update.lua");
	logicGlobal = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-global.lua");
	
	numLogicPorts = 1;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "In";
};
lualogic_registergatedefinition("LogicGate_TextBrick2_Data");

function LogicGate_TextBrick2_Data::LuaLogic_Callback(%data, %brick, %arg) {
	%colorId = getWord(%arg, 0); if(%colorId != -1) %brick.setColor  (%colorId);
	//%colorFx = getWord(%arg, 3); if(%colorFx != -1) %brick.setColorFx(%colorFx);
	%domain = getWord(%arg, 1);
	%print  = getWord(%arg, 2);
	if(%domain !$= "_" && %print !$= "_") %brick.setPrint(lualogic_getprint(%print, %domain));
}


// Pah's half-width prints

datablock fxDtsBrickData(LogicGate_TextBrick2Top2_Data : LogicGate_TextBrick2_Data) {
	brickFile = $LuaLogic::Path @ "bricks/blb/ScreenTop.blb";
	uiName = "Text Brick Right";
	logicGlobal = "";
	logicPortPos[0] = "0 0 0";
};
lualogic_registergatedefinition("LogicGate_TextBrick2Top2_Data");
function LogicGate_TextBrick2Top2_Data::LuaLogic_Callback(%data, %brick, %arg) {
	LogicGate_TextBrick2_Data::LuaLogic_Callback(%data, %brick, %arg);
}

datablock fxDtsBrickData(LogicGate_TextBrick2Bottom2_Data : LogicGate_TextBrick2_Data) {
	brickFile = $LuaLogic::Path @ "bricks/blb/ScreenBottom.blb";
	uiName = "Text Brick Left";
	logicGlobal = "";
	logicPortPos[0] = "0 0 0";
};
lualogic_registergatedefinition("LogicGate_TextBrick2Bottom2_Data");
function LogicGate_TextBrick2Bottom2_Data::LuaLogic_Callback(%data, %brick, %arg) {
	LogicGate_TextBrick2_Data::LuaLogic_Callback(%data, %brick, %arg);
}
