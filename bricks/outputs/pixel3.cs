
datablock fxDTSBrickData(LogicGate_Pixel3_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Logic Pixel";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicUIName = "Pixel";
	logicUIDesc = "Displays either black or its brick color depending on power";
	
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/pixel3-update.lua");
	
	numLogicPorts = 1;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "In";
};
lualogic_registergatedefinition("LogicGate_Pixel3_Data");

function LogicGate_Pixel3_Data::LuaLogic_Callback(%data, %brick, %state){
	if(%state){
		%brick.setPrint(lualogic_getprint("space"));
	}else{
		%brick.setPrint(lualogic_getprint("color000"));
	}
}

datablock fxDtsBrickData(LogicGate_Pixel3Upsidedown_Data : LogicGate_Pixel3_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrickUpsidedown.blb";
	uiName = "Logic Pixel Upside Down";
	
	logicUiName = "Pixel Upside Down";
	
	logicPortPos[0] = "0 0 1";
};
lualogic_registergatedefinition("LogicGate_Pixel3Upsidedown_Data");

function LogicGate_Pixel3Upsidedown_Data::LuaLogic_Callback(%data, %brick, %state){
	LogicGate_Pixel3_Data::LuaLogic_Callback(%data, %brick, %state);
}

$ND::ManualSymmetryZDB["Logic Pixel"            ] = "Logic Pixel Upside Down";
$ND::ManualSymmetryZDB["Logic Pixel Upside Down"] = "Logic Pixel"            ;
