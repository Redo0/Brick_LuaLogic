
return function(gate)
	Gate.initdata(gate)
	local gatedata = Gate.getdata(gate)
	
	gatedata.lastTickChanged = 0
	gatedata.listenState = "wait"
	gatedata.bitsReceived = 0
	gatedata.valReceived = 0
end
