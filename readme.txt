
Brick_LuaLogic
A high-performance digital logic simulation frontend for Blockland
by Eagle517 and Redo

How to Install:
	1. Install this add-on to Blockland.
	2. Install the required add-on Print_Logic_Default - https://gitlab.com/Eagle517/Print_Logic_Default
	3. Download Lua backend - https://gitlab.com/Eagle517/lua-logic
	4. Install LuaPower - https://luapower.com/
		Add luapower-all-master/bin/mingw64 to Path

How to Use:
	1. Start Blockland server
	2. Run Lua backend using StartBackend.bat
	3. Enter llc(); in Blockland server console to connect to the backend.
		If all goes well, the Lua console should show "Connection from 127.0.0.1:25000", and the Blockland console should show "LuaLogic -> tcp connected".

Commands:
	/lt - Toggle the logic updates on or off
	/lst (timeMS) - Set the logic tick time in milliseconds - This is the propagation delay through a single gate. Minimum is 1 - to use higher tick rates, use the multiplier.
	/lsm (multiplier) - Set the logic multiplier. This is how many logic ticks are executed per tick time. For example, for an LST of 5 ms, and an LSM of 30, the effective tick rate will be 6 kHz (1000/5 * 10)
	/ls - Force a logic tick when updates are disabled - useful for debugging circuits and observing timing
	/lfx - Toggle brick FX updates on or off - useful for reducing ghosting when operating large circuits
	/lfxt (timeMS) - Set the logic FX update time in milliseconds - this is how often the logic backend sends FX updates to the game. Increase to reduce ghosting, decrease for better visualization. Setting to 0 not recommended.
	/ltr - Toggle bottom-print display showing the actual logic tick rate
	/li - View information about the number of logic bricks currently on the server


Credits:
	Eagle517 - Logic simulation backend, Add-on communication with backend
	Redo - Logic bricks, Brick generator, Misc backend changes
	Auios - Event gate, Work on predecessor logic mods
	MeltingPlastic - Bug fixes, Work on predecessor logic mods
