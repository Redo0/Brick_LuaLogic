
function mClampFloat(%x, %min, %max){
	return %x<%min ? %min : (%x>%max ? %max : %x);
}

function lualogic_registergatedefinition(%data){
	//lualogic_registergatedefinition_auto(%data);
	
	//handled automatically now
}

function lualogic_registergatedefinition_auto(%data){
	if(!isObject(%data))
		return;
	
	%id = %data.getID();
	
	if((%idx = $LuaLogic::GateDefinitionIDX[%id]) $= "")
	{
		%idx = $LuaLogic::GateDefinitionCount+0;
		$LuaLogic::GateDefinitionIDX[%id] = %idx;
		$LuaLogic::GateDefinitionCount++;
	}
	
	%numports = %data.numLogicPorts;
	
	%def = %id @ ";" @
		lualogic_expandescape(%data.logicUIName) @ ";" @
		lualogic_expandescape(%data.logicUIDesc) @ ";" @
		lualogic_expandescape(%data.logicInit  ) @ ";" @
		lualogic_expandescape(%data.logicUpdate) @ ";" @
		lualogic_expandescape(%data.logicInput ) @ ";" @
		lualogic_expandescape(%data.logicGlobal) @ ";" @
		//(%data.logicCFunction || 0) @ ";" @
		//(%data.logicCDataSize || 0) @ ";" @
		%numports
	;
	
	for(%i = 0; %i < %numports; %i++)
	{
		%def = %def @ ";" @ %data.logicPortType[%i] @ ";" @ %data.logicPortPos[%i] @ ";" @ %data.logicPortDir[%i]
				@ ";" @ (%data.logicPortCauseUpdate[%i] == true) @ ";" @ %data.logicPortUIName[%i];
	}
	
	$LuaLogic::GateDefinition[%idx] = %def;
}

function lualogic_registerAllGateDefinitions(){
	echo("LuaLogic: Registering gate definitions");
	
	for(%dbidx=0; %dbidx<DatablockGroup.getCount(); %dbidx++){
		%db = DatablockGroup.getObject(%dbidx);
		if(%db.isLogic && %db.isLogicGate){
			lualogic_registergatedefinition_auto(%db);
		}
	}
}

function lualogic_print(%text)
{
	echo("LuaLogic -> ", %text);
}

function lualogic_roundpos(%pos)
{
	return mFloor(getWord(%pos, 0)*4)/4 SPC mFloor(getWord(%pos, 1)*4)/4 SPC mFloor(getWord(%pos, 2)*10)/10;
}

function lualogic_roundstudpos(%pos)
{
	return mFloor(getWord(%pos, 0)*2)/2 SPC mFloor(getWord(%pos, 1)*2)/2 SPC mFloor(getWord(%pos, 2)*5)/5;
}

function lualogic_pos(%pos)
{
	%pos = lualogic_roundpos(%pos);
	return getWord(%pos, 0)/0.25 SPC getWord(%pos, 1)/0.25 SPC getWord(%pos, 2)/0.1;
}

function lualogic_studpos(%pos)
{
	%pos = lualogic_roundstudpos(%pos);
	return getWord(%pos, 0)/0.5*2 + 1 SPC getWord(%pos, 1)/0.5*2 + 1 SPC getWord(%pos, 2)/0.2*2;
}

function lualogic_postobrick(%pos)
{
	return getWord(%pos, 0)*0.25 SPC getWord(%pos, 1)*0.25 SPC getWord(%pos, 2)*0.1;
}

function lualogic_connect(%port)
{
	if(isObject(LuaLogicTCP))
		LuaLogicTCP.delete();
	%tcp = new TCPObject(LuaLogicTCP);
	%tcp.connect("127.0.0.1:" @ %port);
}

function lualogic_send(%data)
{
	if(isObject(LuaLogicTCP) && LuaLogicTCP.isConnected)
	{
		//while(strpos(%data, ";;") != -1)
		//	%data = strReplace(%data, ";;", "; ;");
		
		if(strlen(LuaLogicTCP.data) + strlen(%data) >= 1024)
			LuaLogicTCP.sendData();
		
		if(LuaLogicTCP.data $= "")
			LuaLogicTCP.data = %data;
		else
			LuaLogicTCP.data = LuaLogicTCP.data @ ";" @ %data;
	}
}

function lualogic_sendgatedefinitions()
{
	for(%i = 0; %i < $LuaLogic::GateDefinitionCount; %i++)
		lualogic_send("GD;" @ $LuaLogic::GateDefinition[%i]);
}

function lualogic_sendoptions(){
	lualogic_send("OPT;TICK_ENABLED;" @ $Pref::Server::LuaLogic::OPT_TICK_ENABLED);
	lualogic_send("OPT;TICK_TIME;"    @ $Pref::Server::LuaLogic::OPT_TICK_TIME   );
	lualogic_send("OPT;FX_UPDATES;"   @ $Pref::Server::LuaLogic::OPT_FX_UPDATES  );
	lualogic_send("OPT;FX_TIME;"      @ $Pref::Server::LuaLogic::OPT_FX_TIME     );
	lualogic_send("OPT;TICK_MULT;"    @ $Pref::Server::LuaLogic::OPT_TICK_MULT   );
}

function lualogic_savedata(){
	lualogic_send("SAVE");
}

function lualogic_sendinput(%gate, %argc, %arg0, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10, %arg11, %arg12, %arg13, %arg14, %arg15) {
	%args = lualogic_expandescape(%arg0);
	for(%i = 1; %i < %argc; %i++) { %args = %args @ ";" @ lualogic_expandescape(%arg[%i]); }
	lualogic_sendinput_raw(%gate, %argc, %args);
}
function lualogic_sendinput_raw(%gate, %argc, %args) {
	if(%argc > 0) { lualogic_send("IN;" @ %gate.getID() @ ";" @ %argc @ ";" @ %args); }
	else          { lualogic_send("IN;" @ %gate.getID() @ ";0"); }
}

function lualogic_ss(%obj, %state)
{
	lualogic_send("SG;" @ %obj @ ";" @ (%state == true));
}

function lualogic_definecolor(%color, %rgb, %allowTransparency)
{
	%r = getWord(%rgb, 0);
	%g = getWord(%rgb, 1);
	%b = getWord(%rgb, 2);

	%alpha = %allowTransparency ? 0.001 : 1;

	%bestDist = 9e9;

	for(%i = 0; %i < 64; %i++)
	{
		%crgba = getColorIDTable(%i);
		if(getWord(%crgba, 3) >= %alpha)
		{
			%dr = getWord(%crgba, 0) - %r;
			%dg = getWord(%crgba, 1) - %g;
			%db = getWord(%crgba, 2) - %b;
			%dist = %dr*%dr + %dg*%dg + %db*%db;

			if(%dist < %bestDist)
			{
				%bestDist = %dist;
				%bestColor = %i;
			}
		}
	}

	$LuaLogic::Color[%color] = %bestColor;
	return %bestColor;
}

function lualogic_iscolor(%color)
{
	return $LuaLogic::Color[%color] !$= "";
}

function lualogic_getcolor(%color)
{
	if($LuaLogic::Color[%color] !$= "")
		return $LuaLogic::Color[%color];
	return 0;
}

function lualogic_createPrintNameTable() {
	deleteVariables("$LuaLogic::PrintsByFile*");
	%count = getNumPrintTextures();
	for(%i=0; %i<%count; %i++) {
		$LuaLogic::PrintsByFile[getPrintTexture(%i)] = %i;
	}
}

function lualogic_setPrintDomain(%domain) {
	$LuaLogic::Print_Domain = %domain;
}

function lualogic_defineprint(%name, %file) {
	%count = getNumPrintTextures();
	%print = $LuaLogic::PrintsByFile[%file];
	if(%print $= "") { warn("LuaLogic_definePrint: No print named " @ %file @ " (" @ $LuaLogic::Print_Domain @ "." @ %name @ ")"); return; }
	$LuaLogic::Print[$LuaLogic::Print_Domain, %name] = %print;
}

function lualogic_isprint(%print, %domain) {
	return $LuaLogic::Print[%domain, %print] !$= "";
}

function lualogic_getprint(%print, %domain) {
	if(%domain $= "") %domain = "default";
	%print = $LuaLogic::Print[%domain, %print];
	if(%print !$= "") return %print;
	else              return 0;
}

function lualogic_readfile(%filename){
	%filestr="";
	
	%file=new FileObject();
	%success=%file.openForRead(%filename);
	
	if(%success){
		while(!%file.isEOF()){
			%line = %file.readLine();
			%filestr = %filestr @ %line @ "\n";
		}
	}else{
		echo("LuaLogic: Failed to read file \"" @ %filename @ "\"");
	}
	%file.close();
	%file.delete();
	
	return %filestr;
}

$LuaLogic::EscapeCount = 0;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\\"; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "b"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\t"; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "t"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\n"; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "n"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\r"; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "r"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\'"; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "a"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = "\""; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "q"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = ";" ; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "s"; $LuaLogic::EscapeCount++;
$LuaLogic::EscapeIn[$LuaLogic::EscapeCount] = ":" ; $LuaLogic::EscapeOut[$LuaLogic::EscapeCount] = "c"; $LuaLogic::EscapeCount++;

function lualogic_expandescape(%str){
	%ostr = "";
	
	%len = strLen(%str);
	for(%i=0; %i<%len; %i++){
		%ci = getSubStr(%str, %i, 1);
		
		%co = %ci;
		for(%j=0; %j<$LuaLogic::EscapeCount; %j++){
			if(%ci$=$LuaLogic::EscapeIn[%j]){ %co = "\\" @ $LuaLogic::EscapeOut[%j]; }
		}
		
		%ostr = %ostr @ %co;
	}
	
	return %ostr;
}

function lualogic_collapseescape(%str){
	%ostr = "";
	
	%i = 0;
	%len = strLen(%str);
	while(%i<%len){
		%ci = getSubStr(%str, %i, 1);
		
		%co = %ci;
		if(%ci$="\\" && %i<%len-1){
			%i++;
			%ci = getSubStr(%str, %i, 1);
			for(%j=0; %j<$LuaLogic::EscapeCount; %j++){
				if(%ci$=$LuaLogic::EscapeOut[%j]){ %co = $LuaLogic::EscapeIn[%j]; }
			}
		}
		
		%ostr = %ostr @ %co;
		%i++;
	}
	
	return %ostr;
}

function lualogic_loadprintsandcolors() {
	echo("LuaLogic: Loading prints and colors");
	
	deleteVariables("$LuaLogic::Color*");
	lualogic_definecolor("RED"   , "1 0 0 1");
	lualogic_definecolor("GREEN" , "0 1 0 1");
	lualogic_definecolor("YELLOW", "1 1 0 1");
	
	deleteVariables("$LuaLogic::Print*");
	
	lualogic_createPrintNameTable();
	
	lualogic_setprintdomain("default");
	lualogic_defineprint("ARROW"    , "Add-Ons/Print_Logic_Default/prints/arrow.png");
	lualogic_defineprint("UPARROW"  , "Add-Ons/Print_Logic_Default/prints/uparrow.png");
	lualogic_defineprint("DOWNARROW", "Add-Ons/Print_Logic_Default/prints/downarrow.png");
	lualogic_defineprint("ANDGATE"  , "Add-Ons/Print_Logic_Default/prints/AND.png");
	
	for(%i = 0; %i < 8; %i++)
	{
		%a = (%i >> 2) & 1;
		%b = (%i >> 1) & 1;
		%c = (%i >> 0) & 1;
		lualogic_defineprint("COLOR" @ %a @ %b @ %c, "Add-Ons/Print_Logic_Default/prints/color_" @ %a @ %b @ %c @ ".png");
	}
	
	lualogic_loadPrints_default ("default"    );
	lualogic_loadPrints_terminal("terminal"   , ""    );
	lualogic_loadPrints_terminal("terminalInv", "-inv");
	lualogic_loadPrints_jp      ("terminal"   , ""    );
	lualogic_loadPrints_jp      ("terminalInv", "-inv");
	echo("LuaLogic: Loading prints done");
}

function lualogic_loadPrints_default(%domain) {
	lualogic_setprintdomain(%domain);
	
	lualogic_defineprint("space"             , "Add-Ons/Print_Letters_Default/prints/-space.png"           );
	
	lualogic_defineprint("A"                 , "Add-Ons/Print_Letters_Default/prints/A.png"                );
	lualogic_defineprint("B"                 , "Add-Ons/Print_Letters_Default/prints/B.png"                );
	lualogic_defineprint("C"                 , "Add-Ons/Print_Letters_Default/prints/C.png"                );
	lualogic_defineprint("D"                 , "Add-Ons/Print_Letters_Default/prints/D.png"                );
	lualogic_defineprint("E"                 , "Add-Ons/Print_Letters_Default/prints/E.png"                );
	lualogic_defineprint("F"                 , "Add-Ons/Print_Letters_Default/prints/F.png"                );
	lualogic_defineprint("G"                 , "Add-Ons/Print_Letters_Default/prints/G.png"                );
	lualogic_defineprint("H"                 , "Add-Ons/Print_Letters_Default/prints/H.png"                );
	lualogic_defineprint("I"                 , "Add-Ons/Print_Letters_Default/prints/I.png"                );
	lualogic_defineprint("J"                 , "Add-Ons/Print_Letters_Default/prints/J.png"                );
	lualogic_defineprint("K"                 , "Add-Ons/Print_Letters_Default/prints/K.png"                );
	lualogic_defineprint("L"                 , "Add-Ons/Print_Letters_Default/prints/L.png"                );
	lualogic_defineprint("M"                 , "Add-Ons/Print_Letters_Default/prints/M.png"                );
	lualogic_defineprint("N"                 , "Add-Ons/Print_Letters_Default/prints/N.png"                );
	lualogic_defineprint("O"                 , "Add-Ons/Print_Letters_Default/prints/O.png"                );
	lualogic_defineprint("P"                 , "Add-Ons/Print_Letters_Default/prints/P.png"                );
	lualogic_defineprint("Q"                 , "Add-Ons/Print_Letters_Default/prints/Q.png"                );
	lualogic_defineprint("R"                 , "Add-Ons/Print_Letters_Default/prints/R.png"                );
	lualogic_defineprint("S"                 , "Add-Ons/Print_Letters_Default/prints/S.png"                );
	lualogic_defineprint("T"                 , "Add-Ons/Print_Letters_Default/prints/T.png"                );
	lualogic_defineprint("U"                 , "Add-Ons/Print_Letters_Default/prints/U.png"                );
	lualogic_defineprint("V"                 , "Add-Ons/Print_Letters_Default/prints/V.png"                );
	lualogic_defineprint("W"                 , "Add-Ons/Print_Letters_Default/prints/W.png"                );
	lualogic_defineprint("X"                 , "Add-Ons/Print_Letters_Default/prints/X.png"                );
	lualogic_defineprint("Y"                 , "Add-Ons/Print_Letters_Default/prints/Y.png"                );
	lualogic_defineprint("Z"                 , "Add-Ons/Print_Letters_Default/prints/Z.png"                );
	
	lualogic_defineprint("Alcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Alcase.png"         );
	lualogic_defineprint("Blcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Blcase.png"         );
	lualogic_defineprint("Clcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Clcase.png"         );
	lualogic_defineprint("Dlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Dlcase.png"         );
	lualogic_defineprint("Elcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Elcase.png"         );
	lualogic_defineprint("Flcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Flcase.png"         );
	lualogic_defineprint("Glcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Glcase.png"         );
	lualogic_defineprint("Hlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Hlcase.png"         );
	lualogic_defineprint("Ilcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ilcase.png"         );
	lualogic_defineprint("Jlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Jlcase.png"         );
	lualogic_defineprint("Klcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Klcase.png"         );
	lualogic_defineprint("Llcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Llcase.png"         );
	lualogic_defineprint("Mlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Mlcase.png"         );
	lualogic_defineprint("Nlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Nlcase.png"         );
	lualogic_defineprint("Olcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Olcase.png"         );
	lualogic_defineprint("Plcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Plcase.png"         );
	lualogic_defineprint("Qlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Qlcase.png"         );
	lualogic_defineprint("Rlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Rlcase.png"         );
	lualogic_defineprint("Slcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Slcase.png"         );
	lualogic_defineprint("Tlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Tlcase.png"         );
	lualogic_defineprint("Ulcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ulcase.png"         );
	lualogic_defineprint("Vlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Vlcase.png"         );
	lualogic_defineprint("Wlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Wlcase.png"         );
	lualogic_defineprint("Xlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Xlcase.png"         );
	lualogic_defineprint("Ylcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ylcase.png"         );
	lualogic_defineprint("Zlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Zlcase.png"         );
	
	lualogic_defineprint("0"                 , "Add-Ons/Print_Letters_Default/prints/0.png"                );
	lualogic_defineprint("1"                 , "Add-Ons/Print_Letters_Default/prints/1.png"                );
	lualogic_defineprint("2"                 , "Add-Ons/Print_Letters_Default/prints/2.png"                );
	lualogic_defineprint("3"                 , "Add-Ons/Print_Letters_Default/prints/3.png"                );
	lualogic_defineprint("4"                 , "Add-Ons/Print_Letters_Default/prints/4.png"                );
	lualogic_defineprint("5"                 , "Add-Ons/Print_Letters_Default/prints/5.png"                );
	lualogic_defineprint("6"                 , "Add-Ons/Print_Letters_Default/prints/6.png"                );
	lualogic_defineprint("7"                 , "Add-Ons/Print_Letters_Default/prints/7.png"                );
	lualogic_defineprint("8"                 , "Add-Ons/Print_Letters_Default/prints/8.png"                );
	lualogic_defineprint("9"                 , "Add-Ons/Print_Letters_Default/prints/9.png"                );
	
	lualogic_defineprint("bang"              , "Add-Ons/Print_Letters_Default/prints/-bang.png"            );
	lualogic_defineprint("at"                , "Add-Ons/Print_Letters_Default/prints/-at.png"              );
	lualogic_defineprint("pound"             , "Add-Ons/Print_Letters_Default/prints/-pound.png"           );
	lualogic_defineprint("dollar"            , "Add-Ons/Print_Letters_Default/prints/-dollar.png"          );
	lualogic_defineprint("percent"           , "Add-Ons/Print_Letters_Default/prints/-percent.png"         );
	lualogic_defineprint("caret"             , "Add-Ons/Print_Letters_Default/prints/-caret.png"           );
	lualogic_defineprint("and"               , "Add-Ons/Print_Letters_Default/prints/-and.png"             );
	lualogic_defineprint("asterisk"          , "Add-Ons/Print_Letters_Default/prints/-asterisk.png"        );
	lualogic_defineprint("minus"             , "Add-Ons/Print_Letters_Default/prints/-minus.png"           );
	lualogic_defineprint("equals"            , "Add-Ons/Print_Letters_Default/prints/-equals.png"          );
	lualogic_defineprint("plus"              , "Add-Ons/Print_Letters_Default/prints/-plus.png"            );
	lualogic_defineprint("apostrophe"        , "Add-Ons/Print_Letters_Default/prints/-apostrophe.png"      );
	lualogic_defineprint("less_than"         , "Add-Ons/Print_Letters_Default/prints/-less_than.png"       );
	lualogic_defineprint("greater_than"      , "Add-Ons/Print_Letters_Default/prints/-greater_than.png"    );
	lualogic_defineprint("period"            , "Add-Ons/Print_Letters_Default/prints/-period.png"          );
	lualogic_defineprint("qmark"             , "Add-Ons/Print_Letters_Default/prints/-qmark.png"           );
	
	lualogic_defineprint("apostrophe2"       , "Add-Ons/Print_Letters_Extra/prints/-apostrophe2.png"       );
	lualogic_defineprint("colon"             , "Add-Ons/Print_Letters_Extra/prints/-colon.png"             );
	lualogic_defineprint("comma"             , "Add-Ons/Print_Letters_Extra/prints/-comma.png"             );
	lualogic_defineprint("curlybracketleft"  , "Add-Ons/Print_Letters_Extra/prints/-curlybracketleft.png"  );
	lualogic_defineprint("curlybracketright" , "Add-Ons/Print_Letters_Extra/prints/-curlybracketright.png" );
	lualogic_defineprint("roundbracketleft"  , "Add-Ons/Print_Letters_Extra/prints/-roundbracketleft.png"  );
	lualogic_defineprint("roundbracketright" , "Add-Ons/Print_Letters_Extra/prints/-roundbracketright.png" );
	lualogic_defineprint("slashleft"         , "Add-Ons/Print_Letters_Extra/prints/-slashleft.png"         );
	lualogic_defineprint("slashright"        , "Add-Ons/Print_Letters_Extra/prints/-slashright.png"        );
	lualogic_defineprint("squarebracketleft" , "Add-Ons/Print_Letters_Extra/prints/-squarebracketleft.png" );
	lualogic_defineprint("squarebracketright", "Add-Ons/Print_Letters_Extra/prints/-squarebracketright.png");
	lualogic_defineprint("tilde"             , "Add-Ons/Print_Letters_Extra/prints/-tilde.png"             );
	lualogic_defineprint("underscore"        , "Add-Ons/Print_Letters_Extra/prints/-underscore.png"        );
	lualogic_defineprint("verticalbar"       , "Add-Ons/Print_Letters_Extra/prints/-verticalbar.png"       );
	
	lualogic_defineprint("semicolon"         , "Add-Ons/Print_Letters_ExtraExtended/prints/-semicolon.png" );
	lualogic_defineprint("backtick"          , "Add-Ons/Print_Letters_ExtraExtended/prints/-backtick.png"  );
}

function lualogic_loadPrints_terminal(%domain, %p) {
	lualogic_setprintdomain(%domain);
	
	lualogic_defineprint("space"             , "Add-Ons/Print_Terminal_Default/prints/-term-space"              @ %p @ ".png");
	
	lualogic_defineprint("A"                 , "Add-Ons/Print_Terminal_Default/prints/-term-A"                   @ %p @ ".png");
	lualogic_defineprint("B"                 , "Add-Ons/Print_Terminal_Default/prints/-term-B"                   @ %p @ ".png");
	lualogic_defineprint("C"                 , "Add-Ons/Print_Terminal_Default/prints/-term-C"                   @ %p @ ".png");
	lualogic_defineprint("D"                 , "Add-Ons/Print_Terminal_Default/prints/-term-D"                   @ %p @ ".png");
	lualogic_defineprint("E"                 , "Add-Ons/Print_Terminal_Default/prints/-term-E"                   @ %p @ ".png");
	lualogic_defineprint("F"                 , "Add-Ons/Print_Terminal_Default/prints/-term-F"                   @ %p @ ".png");
	lualogic_defineprint("G"                 , "Add-Ons/Print_Terminal_Default/prints/-term-G"                   @ %p @ ".png");
	lualogic_defineprint("H"                 , "Add-Ons/Print_Terminal_Default/prints/-term-H"                   @ %p @ ".png");
	lualogic_defineprint("I"                 , "Add-Ons/Print_Terminal_Default/prints/-term-I"                   @ %p @ ".png");
	lualogic_defineprint("J"                 , "Add-Ons/Print_Terminal_Default/prints/-term-J"                   @ %p @ ".png");
	lualogic_defineprint("K"                 , "Add-Ons/Print_Terminal_Default/prints/-term-K"                   @ %p @ ".png");
	lualogic_defineprint("L"                 , "Add-Ons/Print_Terminal_Default/prints/-term-L"                   @ %p @ ".png");
	lualogic_defineprint("M"                 , "Add-Ons/Print_Terminal_Default/prints/-term-M"                   @ %p @ ".png");
	lualogic_defineprint("N"                 , "Add-Ons/Print_Terminal_Default/prints/-term-N"                   @ %p @ ".png");
	lualogic_defineprint("O"                 , "Add-Ons/Print_Terminal_Default/prints/-term-O"                   @ %p @ ".png");
	lualogic_defineprint("P"                 , "Add-Ons/Print_Terminal_Default/prints/-term-P"                   @ %p @ ".png");
	lualogic_defineprint("Q"                 , "Add-Ons/Print_Terminal_Default/prints/-term-Q"                   @ %p @ ".png");
	lualogic_defineprint("R"                 , "Add-Ons/Print_Terminal_Default/prints/-term-R"                   @ %p @ ".png");
	lualogic_defineprint("S"                 , "Add-Ons/Print_Terminal_Default/prints/-term-S"                   @ %p @ ".png");
	lualogic_defineprint("T"                 , "Add-Ons/Print_Terminal_Default/prints/-term-T"                   @ %p @ ".png");
	lualogic_defineprint("U"                 , "Add-Ons/Print_Terminal_Default/prints/-term-U"                   @ %p @ ".png");
	lualogic_defineprint("V"                 , "Add-Ons/Print_Terminal_Default/prints/-term-V"                   @ %p @ ".png");
	lualogic_defineprint("W"                 , "Add-Ons/Print_Terminal_Default/prints/-term-W"                   @ %p @ ".png");
	lualogic_defineprint("X"                 , "Add-Ons/Print_Terminal_Default/prints/-term-X"                   @ %p @ ".png");
	lualogic_defineprint("Y"                 , "Add-Ons/Print_Terminal_Default/prints/-term-Y"                   @ %p @ ".png");
	lualogic_defineprint("Z"                 , "Add-Ons/Print_Terminal_Default/prints/-term-Z"                   @ %p @ ".png");
	
	lualogic_defineprint("Alcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-A-lwr"               @ %p @ ".png");
	lualogic_defineprint("Blcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-B-lwr"               @ %p @ ".png");
	lualogic_defineprint("Clcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-C-lwr"               @ %p @ ".png");
	lualogic_defineprint("Dlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-D-lwr"               @ %p @ ".png");
	lualogic_defineprint("Elcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-E-lwr"               @ %p @ ".png");
	lualogic_defineprint("Flcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-F-lwr"               @ %p @ ".png");
	lualogic_defineprint("Glcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-G-lwr"               @ %p @ ".png");
	lualogic_defineprint("Hlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-H-lwr"               @ %p @ ".png");
	lualogic_defineprint("Ilcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-I-lwr"               @ %p @ ".png");
	lualogic_defineprint("Jlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-J-lwr"               @ %p @ ".png");
	lualogic_defineprint("Klcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-K-lwr"               @ %p @ ".png");
	lualogic_defineprint("Llcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-L-lwr"               @ %p @ ".png");
	lualogic_defineprint("Mlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-M-lwr"               @ %p @ ".png");
	lualogic_defineprint("Nlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-N-lwr"               @ %p @ ".png");
	lualogic_defineprint("Olcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-O-lwr"               @ %p @ ".png");
	lualogic_defineprint("Plcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-P-lwr"               @ %p @ ".png");
	lualogic_defineprint("Qlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-Q-lwr"               @ %p @ ".png");
	lualogic_defineprint("Rlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-R-lwr"               @ %p @ ".png");
	lualogic_defineprint("Slcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-S-lwr"               @ %p @ ".png");
	lualogic_defineprint("Tlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-T-lwr"               @ %p @ ".png");
	lualogic_defineprint("Ulcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-U-lwr"               @ %p @ ".png");
	lualogic_defineprint("Vlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-V-lwr"               @ %p @ ".png");
	lualogic_defineprint("Wlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-W-lwr"               @ %p @ ".png");
	lualogic_defineprint("Xlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-X-lwr"               @ %p @ ".png");
	lualogic_defineprint("Ylcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-Y-lwr"               @ %p @ ".png");
	lualogic_defineprint("Zlcase"            , "Add-Ons/Print_Terminal_Default/prints/-term-Z-lwr"               @ %p @ ".png");
	
	lualogic_defineprint("0"                 , "Add-Ons/Print_Terminal_Default/prints/-term-0"                   @ %p @ ".png");
	lualogic_defineprint("1"                 , "Add-Ons/Print_Terminal_Default/prints/-term-1"                   @ %p @ ".png");
	lualogic_defineprint("2"                 , "Add-Ons/Print_Terminal_Default/prints/-term-2"                   @ %p @ ".png");
	lualogic_defineprint("3"                 , "Add-Ons/Print_Terminal_Default/prints/-term-3"                   @ %p @ ".png");
	lualogic_defineprint("4"                 , "Add-Ons/Print_Terminal_Default/prints/-term-4"                   @ %p @ ".png");
	lualogic_defineprint("5"                 , "Add-Ons/Print_Terminal_Default/prints/-term-5"                   @ %p @ ".png");
	lualogic_defineprint("6"                 , "Add-Ons/Print_Terminal_Default/prints/-term-6"                   @ %p @ ".png");
	lualogic_defineprint("7"                 , "Add-Ons/Print_Terminal_Default/prints/-term-7"                   @ %p @ ".png");
	lualogic_defineprint("8"                 , "Add-Ons/Print_Terminal_Default/prints/-term-8"                   @ %p @ ".png");
	lualogic_defineprint("9"                 , "Add-Ons/Print_Terminal_Default/prints/-term-9"                   @ %p @ ".png");
	
	lualogic_defineprint("bang"              , "Add-Ons/Print_Terminal_Default/prints/-term-bang"               @ %p @ ".png");
	lualogic_defineprint("at"                , "Add-Ons/Print_Terminal_Default/prints/-term-at"                 @ %p @ ".png");
	lualogic_defineprint("pound"             , "Add-Ons/Print_Terminal_Default/prints/-term-pound"              @ %p @ ".png");
	lualogic_defineprint("dollar"            , "Add-Ons/Print_Terminal_Default/prints/-term-dollar"             @ %p @ ".png");
	lualogic_defineprint("percent"           , "Add-Ons/Print_Terminal_Default/prints/-term-percent"            @ %p @ ".png");
	lualogic_defineprint("caret"             , "Add-Ons/Print_Terminal_Default/prints/-term-caret"              @ %p @ ".png");
	lualogic_defineprint("and"               , "Add-Ons/Print_Terminal_Default/prints/-term-and"                @ %p @ ".png");
	lualogic_defineprint("asterisk"          , "Add-Ons/Print_Terminal_Default/prints/-term-asterisk"           @ %p @ ".png");
	lualogic_defineprint("minus"             , "Add-Ons/Print_Terminal_Default/prints/-term-minus"              @ %p @ ".png");
	lualogic_defineprint("equals"            , "Add-Ons/Print_Terminal_Default/prints/-term-equals"             @ %p @ ".png");
	lualogic_defineprint("plus"              , "Add-Ons/Print_Terminal_Default/prints/-term-plus"               @ %p @ ".png");
	lualogic_defineprint("apostrophe"        , "Add-Ons/Print_Terminal_Default/prints/-term-apostrophe"         @ %p @ ".png");
	lualogic_defineprint("less_than"         , "Add-Ons/Print_Terminal_Default/prints/-term-less_than"          @ %p @ ".png");
	lualogic_defineprint("greater_than"      , "Add-Ons/Print_Terminal_Default/prints/-term-greater_than"       @ %p @ ".png");
	lualogic_defineprint("period"            , "Add-Ons/Print_Terminal_Default/prints/-term-period"             @ %p @ ".png");
	lualogic_defineprint("qmark"             , "Add-Ons/Print_Terminal_Default/prints/-term-qmark"              @ %p @ ".png");
	
	lualogic_defineprint("apostrophe2"       , "Add-Ons/Print_Terminal_Default/prints/-term-double_quote"       @ %p @ ".png");
	lualogic_defineprint("colon"             , "Add-Ons/Print_Terminal_Default/prints/-term-colon"              @ %p @ ".png");
	lualogic_defineprint("comma"             , "Add-Ons/Print_Terminal_Default/prints/-term-comma"              @ %p @ ".png");
	lualogic_defineprint("curlybracketleft"  , "Add-Ons/Print_Terminal_Default/prints/-term-left_brace"         @ %p @ ".png");
	lualogic_defineprint("curlybracketright" , "Add-Ons/Print_Terminal_Default/prints/-term-right_brace"        @ %p @ ".png");
	lualogic_defineprint("roundbracketleft"  , "Add-Ons/Print_Terminal_Default/prints/-term-left_parenthesis"   @ %p @ ".png");
	lualogic_defineprint("roundbracketright" , "Add-Ons/Print_Terminal_Default/prints/-term-right_parenthesis"  @ %p @ ".png");
	lualogic_defineprint("slashleft"         , "Add-Ons/Print_Terminal_Default/prints/-term-forward_slash"      @ %p @ ".png");
	lualogic_defineprint("slashright"        , "Add-Ons/Print_Terminal_Default/prints/-term-backward_slash"     @ %p @ ".png");
	lualogic_defineprint("squarebracketleft" , "Add-Ons/Print_Terminal_Default/prints/-term-left_bracket"       @ %p @ ".png");
	lualogic_defineprint("squarebracketright", "Add-Ons/Print_Terminal_Default/prints/-term-right_bracket"      @ %p @ ".png");
	lualogic_defineprint("tilde"             , "Add-Ons/Print_Terminal_Default/prints/-term-tilde"              @ %p @ ".png");
	lualogic_defineprint("underscore"        , "Add-Ons/Print_Terminal_Default/prints/-term-underscore"         @ %p @ ".png");
	lualogic_defineprint("verticalbar"       , "Add-Ons/Print_Terminal_Default/prints/-term-vertical_bar"       @ %p @ ".png");
	
	lualogic_defineprint("semicolon"         , "Add-Ons/Print_Terminal_Default/prints/-term-semicolon"          @ %p @ ".png");
	lualogic_defineprint("backtick"          , "Add-Ons/Print_Terminal_Default/prints/-term-grave"              @ %p @ ".png");
}

function lualogic_loadPrints_jp(%domain, %p) {
	lualogic_setprintdomain(%domain);
	
	lualogic_defineprint("jp-yen"          , "Add-Ons/Print_Terminal_JP/prints/-jp-yen"           @ %p @ ".png");
	lualogic_defineprint("jp-period"       , "Add-Ons/Print_Terminal_JP/prints/-jp-period"        @ %p @ ".png");
	lualogic_defineprint("jp-left-bracket" , "Add-Ons/Print_Terminal_JP/prints/-jp-left-bracket"  @ %p @ ".png");
	lualogic_defineprint("jp-right-bracket", "Add-Ons/Print_Terminal_JP/prints/-jp-right-bracket" @ %p @ ".png");
	lualogic_defineprint("jp-comma"        , "Add-Ons/Print_Terminal_JP/prints/-jp-comma"         @ %p @ ".png");
	lualogic_defineprint("jp-dot"          , "Add-Ons/Print_Terminal_JP/prints/-jp-dot"           @ %p @ ".png");
	lualogic_defineprint("jp-wo"           , "Add-Ons/Print_Terminal_JP/prints/-jp-wo"            @ %p @ ".png");
	lualogic_defineprint("jp-small-a"      , "Add-Ons/Print_Terminal_JP/prints/-jp-small-a"       @ %p @ ".png");
	lualogic_defineprint("jp-small-i"      , "Add-Ons/Print_Terminal_JP/prints/-jp-small-i"       @ %p @ ".png");
	lualogic_defineprint("jp-small-u"      , "Add-Ons/Print_Terminal_JP/prints/-jp-small-u"       @ %p @ ".png");
	lualogic_defineprint("jp-small-e"      , "Add-Ons/Print_Terminal_JP/prints/-jp-small-e"       @ %p @ ".png");
	lualogic_defineprint("jp-small-o"      , "Add-Ons/Print_Terminal_JP/prints/-jp-small-o"       @ %p @ ".png");
	lualogic_defineprint("jp-small-ya"     , "Add-Ons/Print_Terminal_JP/prints/-jp-small-ya"      @ %p @ ".png");
	lualogic_defineprint("jp-small-yu"     , "Add-Ons/Print_Terminal_JP/prints/-jp-small-yu"      @ %p @ ".png");
	lualogic_defineprint("jp-small-yo"     , "Add-Ons/Print_Terminal_JP/prints/-jp-small-yo"      @ %p @ ".png");
	lualogic_defineprint("jp-small-tsu"    , "Add-Ons/Print_Terminal_JP/prints/-jp-small-tsu"     @ %p @ ".png");
	lualogic_defineprint("jp-dash"         , "Add-Ons/Print_Terminal_JP/prints/-jp-dash"          @ %p @ ".png");
	lualogic_defineprint("jp-a"            , "Add-Ons/Print_Terminal_JP/prints/-jp-a"             @ %p @ ".png");
	lualogic_defineprint("jp-i"            , "Add-Ons/Print_Terminal_JP/prints/-jp-i"             @ %p @ ".png");
	lualogic_defineprint("jp-u"            , "Add-Ons/Print_Terminal_JP/prints/-jp-u"             @ %p @ ".png");
	lualogic_defineprint("jp-e"            , "Add-Ons/Print_Terminal_JP/prints/-jp-e"             @ %p @ ".png");
	lualogic_defineprint("jp-o"            , "Add-Ons/Print_Terminal_JP/prints/-jp-o"             @ %p @ ".png");
	lualogic_defineprint("jp-ka"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ka"            @ %p @ ".png");
	lualogic_defineprint("jp-ki"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ki"            @ %p @ ".png");
	lualogic_defineprint("jp-ku"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ku"            @ %p @ ".png");
	lualogic_defineprint("jp-ke"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ke"            @ %p @ ".png");
	lualogic_defineprint("jp-ko"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ko"            @ %p @ ".png");
	lualogic_defineprint("jp-sa"           , "Add-Ons/Print_Terminal_JP/prints/-jp-sa"            @ %p @ ".png");
	lualogic_defineprint("jp-shi"          , "Add-Ons/Print_Terminal_JP/prints/-jp-shi"           @ %p @ ".png");
	lualogic_defineprint("jp-su"           , "Add-Ons/Print_Terminal_JP/prints/-jp-su"            @ %p @ ".png");
	lualogic_defineprint("jp-se"           , "Add-Ons/Print_Terminal_JP/prints/-jp-se"            @ %p @ ".png");
	lualogic_defineprint("jp-so"           , "Add-Ons/Print_Terminal_JP/prints/-jp-so"            @ %p @ ".png");
	lualogic_defineprint("jp-ta"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ta"            @ %p @ ".png");
	lualogic_defineprint("jp-chi"          , "Add-Ons/Print_Terminal_JP/prints/-jp-chi"           @ %p @ ".png");
	lualogic_defineprint("jp-tsu"          , "Add-Ons/Print_Terminal_JP/prints/-jp-tsu"           @ %p @ ".png");
	lualogic_defineprint("jp-te"           , "Add-Ons/Print_Terminal_JP/prints/-jp-te"            @ %p @ ".png");
	lualogic_defineprint("jp-to"           , "Add-Ons/Print_Terminal_JP/prints/-jp-to"            @ %p @ ".png");
	lualogic_defineprint("jp-na"           , "Add-Ons/Print_Terminal_JP/prints/-jp-na"            @ %p @ ".png");
	lualogic_defineprint("jp-ni"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ni"            @ %p @ ".png");
	lualogic_defineprint("jp-nu"           , "Add-Ons/Print_Terminal_JP/prints/-jp-nu"            @ %p @ ".png");
	lualogic_defineprint("jp-ne"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ne"            @ %p @ ".png");
	lualogic_defineprint("jp-no"           , "Add-Ons/Print_Terminal_JP/prints/-jp-no"            @ %p @ ".png");
	lualogic_defineprint("jp-ha"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ha"            @ %p @ ".png");
	lualogic_defineprint("jp-hi"           , "Add-Ons/Print_Terminal_JP/prints/-jp-hi"            @ %p @ ".png");
	lualogic_defineprint("jp-fu"           , "Add-Ons/Print_Terminal_JP/prints/-jp-fu"            @ %p @ ".png");
	lualogic_defineprint("jp-he"           , "Add-Ons/Print_Terminal_JP/prints/-jp-he"            @ %p @ ".png");
	lualogic_defineprint("jp-ho"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ho"            @ %p @ ".png");
	lualogic_defineprint("jp-ma"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ma"            @ %p @ ".png");
	lualogic_defineprint("jp-mi"           , "Add-Ons/Print_Terminal_JP/prints/-jp-mi"            @ %p @ ".png");
	lualogic_defineprint("jp-mu"           , "Add-Ons/Print_Terminal_JP/prints/-jp-mu"            @ %p @ ".png");
	lualogic_defineprint("jp-me"           , "Add-Ons/Print_Terminal_JP/prints/-jp-me"            @ %p @ ".png");
	lualogic_defineprint("jp-mo"           , "Add-Ons/Print_Terminal_JP/prints/-jp-mo"            @ %p @ ".png");
	lualogic_defineprint("jp-ya"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ya"            @ %p @ ".png");
	lualogic_defineprint("jp-yu"           , "Add-Ons/Print_Terminal_JP/prints/-jp-yu"            @ %p @ ".png");
	lualogic_defineprint("jp-yo"           , "Add-Ons/Print_Terminal_JP/prints/-jp-yo"            @ %p @ ".png");
	lualogic_defineprint("jp-ra"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ra"            @ %p @ ".png");
	lualogic_defineprint("jp-ri"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ri"            @ %p @ ".png");
	lualogic_defineprint("jp-ru"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ru"            @ %p @ ".png");
	lualogic_defineprint("jp-re"           , "Add-Ons/Print_Terminal_JP/prints/-jp-re"            @ %p @ ".png");
	lualogic_defineprint("jp-ro"           , "Add-Ons/Print_Terminal_JP/prints/-jp-ro"            @ %p @ ".png");
	lualogic_defineprint("jp-wa"           , "Add-Ons/Print_Terminal_JP/prints/-jp-wa"            @ %p @ ".png");
	lualogic_defineprint("jp-n"            , "Add-Ons/Print_Terminal_JP/prints/-jp-n"             @ %p @ ".png");
	lualogic_defineprint("jp-dakuten"      , "Add-Ons/Print_Terminal_JP/prints/-jp-dakuten"       @ %p @ ".png");
	lualogic_defineprint("jp-handakuten"   , "Add-Ons/Print_Terminal_JP/prints/-jp-handakuten"    @ %p @ ".png");
}
